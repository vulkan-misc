#ifndef NYANVK_SYMS_GLOBAL_H
#define NYANVK_SYMS_GLOBAL_H
/*
 * this is public domain without any warranties of any kind
 * Sylvain BERTRAND
 */
/* XXX: define VK_GLOBAL_SYMS and cherry pick what you actually use */
#define VK_GLOBAL_SYMS_FULL \
	static void *(*dl_vk_get_instance_proc_addr)(void *instance, u8 *name);\
	static void *(*dl_vk_get_dev_proc_addr)(void *dev, u8 *name);\
\
	static s32 (*dl_vk_enumerate_instance_version)(u32 *version);\
	static s32 (*dl_vk_enumerate_instance_layer_props)(\
				u32 *props_n,\
				struct vk_layer_props_t *props);\
	static s32 (*dl_vk_enumerate_instance_ext_props)(\
				u8 *layer_name,\
				u32 *props_n,\
				struct vk_ext_props_t *props);\
	static s32 (*dl_vk_create_instance)(\
				struct vk_instance_create_info_t *info,\
				void *allocator,\
				void **instance);\
	static s32 (*dl_vk_enumerate_phydevs)(\
				void *instance,\
				u32 *phydevs_n,\
				void **phydevs);\
	static s32 (*dl_vk_enumerate_dev_ext_props)(\
				void *phydev,\
				u8 *layer_name,\
				u32 *props_n,\
				struct vk_ext_props_t *props);\
	static void (*dl_vk_get_phydev_props)(\
				void *phydev,\
				struct vk_phydev_props_t *props);\
	static s32 (*dl_vk_create_dev)(\
			void *phydev,\
			struct vk_dev_create_info_t *create_info,\
			void *allocator,\
			void **dev);\
	static void (*dl_vk_get_phydev_q_fam_props)(\
			void *phydev,\
			u32 *q_fam_props_n,\
			struct vk_q_fam_props_t *props);\
	static s32 (*dl_vk_create_xcb_surf)(\
				void *instance,\
				struct vk_xcb_surf_create_info_t *info,\
				void *allocator,\
				void **surf);\
	static s32 (*dl_vk_get_phydev_surf_support)(\
					void *phydev,\
					u32 q_fam,\
					void *surf,\
					u32 *supported);\
	static s32  (*dl_vk_get_phydev_surf_texel_mem_blk_confs)(\
				void *phydev,\
				struct vk_phydev_surf_info_t *info,\
				u32 *confs_n,\
				struct vk_surf_texel_mem_blk_conf_t *confs);\
	static void (*dl_vk_get_phydev_mem_props)(\
			void *phydev,\
			struct vk_phydev_mem_props_t *props);\
	static s32 (*dl_vk_get_phydev_surf_caps)(\
					void *phydev,\
					struct vk_phydev_surf_info_t *info,\
					struct vk_surf_caps_t *caps);\
	static s32 (*dl_vk_get_phydev_surf_present_modes)(\
					void *phydev,\
					void *surf,\
					u32 *modes_n,\
					u32 *modes);
#endif
