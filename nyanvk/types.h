#ifndef NYANVK_TYPES_H
#define NYANVK_TYPES_H
/*
 * this is public domain without any warranties of any kind
 * Sylvain BERTRAND
 */
/* XXX: KEEP AN EYE ON ABBREVIATIONS, ALWAYS */
/*----------------------------------------------------------------------------*/
#define vk_true 1
#define vk_false 0
#define vk_whole_sz 0xffffffffffffffff
#define vk_q_fam_ignored 0xffffffff
/*----------------------------------------------------------------------------*/
#define VK_VERSION_MAJOR(x) (x >> 22)
#define VK_VERSION_MINOR(x) ((x >> 12) &  0x3ff)
#define VK_VERSION_PATCH(x) (x & 0xfff)
/*----------------------------------------------------------------------------*/
struct vk_offset_2d_t {
	s32 x;
	s32 y;
};
struct vk_offset_3d_t {
	s32 x;
	s32 y;
	s32 z;
};
struct vk_extent_2d_t {
	u32 width;
	u32 height;
};
struct vk_extent_3d_t {
	u32 width;
	u32 height;
	u32 depth;
};
struct vk_rect_2d_t {
	struct vk_offset_2d_t offset;
	struct vk_extent_2d_t extent;
};
struct vk_instance_create_info_t {
	u32 type;
	void *next;
	u32 flags;
	void *app_info; /* allow easy hidden driver optimizations: no! */
	u32 enabled_layers_n;
	u8 **enabled_layer_names;
	u32 enabled_exts_n;
	u8 **enabled_ext_names;
};
#define VK_MAX_EXT_NAME_SZ 256
struct vk_ext_props_t {
	u8 name[VK_MAX_EXT_NAME_SZ];
	u32 spec_version;
};
#define VK_MAX_DESC_SZ 256
struct vk_layer_props_t {
	u8 name[VK_MAX_EXT_NAME_SZ];
	u32 spec_version;
	u32 implementation_version;
	u8 desc[VK_MAX_DESC_SZ];
};
struct vk_phydev_limits_t {
	u32	not_used_00[11];
	u64	not_used_01[2];
	u32	not_used_02[51];
	float	not_used_03[2];
	u32	not_used_04[3];
	float	not_used_05[2];
	u32 	not_used_06;
	size_t	not_used_07;
	u64	not_used_08[3];
	u32	not_used_09[4];
	float	not_used_10[2];
	u32	not_used_11[16];
	float	not_used_12;
	u32	not_used_13[4];
	float	not_used_14[6];
	u32	not_used_15[2];
	u64	not_used_16[3];
};
struct vk_phydev_sparse_props_t {
	u32 not_used[5];
};
#define VK_MAX_PHYDEV_NAME_SZ 256
#define VK_UUID_SZ 16
struct vk_phydev_props_core_t {
	u32 api_version;
	u32 driver_version;
	u32 vendor_id;
	u32 dev_id;
	u32 dev_type;
	u8 name[VK_MAX_PHYDEV_NAME_SZ];
	u8 pl_cache_uuid[VK_UUID_SZ];
	struct vk_phydev_limits_t limits;
	struct vk_phydev_sparse_props_t sparse_props;
};
/* the vulkan 1.1 version */
struct vk_phydev_props_t {
	u32 type;
	void *next;
	struct vk_phydev_props_core_t core;
};
struct vk_q_fam_props_core_t {
	u32 flags;
	u32 qs_n;
	u32 timestamp_valid_bits;
	struct vk_extent_3d_t min_img_transfer_granularity;
};
struct vk_q_fam_props_t {
	u32 type;
	void *next;
	struct vk_q_fam_props_core_t core;
};
struct vk_phydev_features_t {
	u32 not_used[55];
};
struct vk_dev_q_create_info_t {
	u32 type;
	void *next;
	u32 flags;
	u32 q_fam;
	u32 qs_n;
	float *q_prios;
};
struct vk_dev_create_info_t {
	u32 type;
	void *next;
	u32 flags;
	u32 q_create_infos_n;
	struct vk_dev_q_create_info_t *q_create_infos;
	u32 do_not_use_0;
	void *do_not_use_1;
	u32 enabled_exts_n;
	u8 **enabled_ext_names;
	void *do_not_use_2;
};
struct vk_cp_create_info_t {
	u32 type;
	void *next;
	u32 flags;
	u32 q_fam;
};
struct vk_xcb_surf_create_info_t {
	u32 type;
	void *next;
	u32 flags;
    	xcb_connection_t *c;
	xcb_window_t win;
};
struct vk_phydev_surf_info_t {
	u32 type;
	void *next;
	void *surf;
};
struct vk_surf_texel_mem_blk_conf_core_t {
	u32 fmt;
	u32 color_space;
};
struct vk_surf_texel_mem_blk_conf_t {
	u32 type;
	void *next;
	struct vk_surf_texel_mem_blk_conf_core_t core;
};
struct vk_mem_type_t {
	u32 prop_flags;
	u32 heap;
};
struct vk_mem_heap_t {
	u64 sz;
	u32 flags;
};
#define VK_MEM_TYPES_N_MAX               32
#define VK_MEM_HEAPS_N_MAX               16
struct vk_phydev_mem_props_core_t {
	u32 mem_types_n;
	struct  vk_mem_type_t mem_types[VK_MEM_TYPES_N_MAX];
	u32 mem_heaps_n;
	struct vk_mem_heap_t mem_heaps[VK_MEM_HEAPS_N_MAX];
};
struct vk_phydev_mem_props_t {
	u32 type;
	void *next;
    	struct vk_phydev_mem_props_core_t core;
};
struct vk_surf_caps_core_t {
	u32 imgs_n_min;
	u32 imgs_n_max;
	struct vk_extent_2d_t current_extent;
	struct vk_extent_2d_t img_extent_min; 
	struct vk_extent_2d_t img_extent_max;
	u32 img_array_layers_n_max;
	u32 supported_transforms;
	u32 current_transform;
	u32 supported_composite_alpha;
	u32 supported_img_usage_flags;
};
struct vk_surf_caps_t {
	u32 type;
	void *next;
	struct vk_surf_caps_core_t core;
};
struct vk_swpchn_create_info_t {
	u32 type;
	void *next;
	u32 flags;
	void *surf;
	u32 imgs_n_min;
	u32 img_texel_mem_blk_fmt;
	u32 img_color_space;
	struct vk_extent_2d_t img_extent;
	u32 img_layers_n;
	u32 img_usage;
	u32 img_sharing_mode;
	u32 q_fams_n;
	u32 *q_fams;
	u32 pre_transform;
	u32 composite_alpha;
	u32 present_mode;
	u32 clipped;
	void *old_swpchn;
};
struct vk_img_create_info_t {
	u32 type;
	void *next;
	u32 flags;
	u32 img_type;
	u32 texel_mem_blk_fmt;
	struct vk_extent_3d_t extent; 
	u32 mip_lvls_n;
	u32 array_layers_n;
	u32 samples_n; /* flags */
	u32 img_tiling;
	u32 usage;
	u32 sharing_mode;
	u32 q_fams_n;
	u32 *q_fams;
	u32 initial_layout;
};
struct vk_img_mem_rqmts_info_t {
	u32 type;
	void *next;
	void *img;
};
struct vk_mem_rqmts_core_t {
	u64 sz;
	u64 alignment;
	/* idxs of bits are idxs in mem types of vk_phydev_mem_props_core_t */
	u32 mem_type_bits;
};
struct vk_mem_rqmts_t {
	u32 type;
	void *next;
	struct vk_mem_rqmts_core_t core;
};
struct vk_mem_alloc_info_t {
	u32 type;
	void *next;
	u64 sz;
	u32 mem_type_idx; /* in the physical device array of memory types */
};
struct vk_bind_img_mem_info_t {
	u32 type;
	void *next;
	void *img;
	void *mem;
	u64 offset;
};
struct vk_img_subrsrc_range_t {
	u32 aspect;
	u32 base_mip_lvl;
	u32 lvls_n;
	u32 base_array_layer;
	u32 array_layers_n;
};
struct vk_img_mem_barrier_t {
	u32 type;
	void *next;
	u32 src_access;
	u32 dst_access;
	u32 old_layout;
	u32 new_layout;
	u32 src_q_fam;
	u32 dst_q_fam;
	void *img;
	struct vk_img_subrsrc_range_t subrsrc_range;
};
struct vk_cb_alloc_info_t {
	u32 type;
	void *next;
	void *cp;
	u32 lvl;
	u32 cbs_n;
};
struct vk_cb_begin_info_t {
	u32 type;
	void *next;
	u32 flags;
	void *do_not_use;
};
struct vk_submit_info_t {
	u32 type;
	void *next;
	u32 wait_sems_n;
	void **wait_sems;
	u32* wait_dst_stages;
	u32 cbs_n;
	void **cbs;
	u32 signal_sems_n;
	void **signal_sems;
};
struct vk_img_subrsrc_t {
	u32 aspect;
	u32 mip_lvl;
	u32 array_layer;
};
struct vk_subrsrc_layout_t {
	u64 offset;
	u64 sz;
	u64 row_pitch;
	u64 array_pitch;
	u64 depth_pitch;
};
struct vk_img_subrsrc_layers_t {
	u32 aspect;
	u32 mip_lvl;
	u32 base_array_layer;
	u32 array_layers_n;
};
struct vk_acquire_next_img_info_t {
	u32 type;
	void *next;
	void *swpchn;
	u64 timeout;
	void *sem;
	void *fence;
	u32 devs;
};
struct vk_img_blit_t {
	struct vk_img_subrsrc_layers_t src_subrsrc;
	struct vk_offset_3d_t src_offsets[2];
	struct vk_img_subrsrc_layers_t dst_subrsrc;
	struct vk_offset_3d_t dst_offsets[2];
};
struct vk_present_info_t {
	u32 type;
	void *next;
	u32 wait_sems_n;
	void **wait_sems;
	u32 swpchns_n;
	void **swpchns;
	u32 *idxs;
	s32 *results;
};
struct vk_sem_create_info_t {
	u32 type;
	void *next;
	u32 flags;
};
struct vk_comp_map_t {
	s32 r;
	s32 g;
	s32 b;
	s32 a;
};
struct vk_imgview_create_info_t {
	u32 type;
	void *next;
	u32 flags;
	void *img;
	s32 view_type;
	s32 fmt;
	struct vk_comp_map_t comps;
	struct vk_img_subrsrc_range_t subrsrc_range;
};
struct vk_at_desc_t {
	u32 type;
	void *next;
	u32 flags;
	u32 fmt;
	u32 samples_n;
	u32 load_op;
	u32 store_op;
	u32 stencil_load_op;
	u32 stencil_store_op;
	u32 initial_layout;
	u32 final_layout;
};
struct vk_at_ref_t {
	u32 type;
	void *next;
	u32 at;
	u32 layout;
};
struct vk_sp_desc_t {
	u32 type;
	void *next;
	u32 flags;
	u32 pl_bind_point;
	u32 viewmask;
	u32 input_ats_n;
	struct vk_at_ref_t *input_ats;
	u32 color_ats_n;
	struct vk_at_ref_t *color_ats;
	struct vk_at_ref_t *resolve_ats;
	struct vk_at_ref_t *depth_stencil_ats;
	u32 preserve_ats_n;
	u32 *preserve_ats;
};
struct vk_sp_dep_t {
	u32 src_sp;
	u32 dst_sp;
	u32 src_stage_mask;
	u32 dst_stage_mask;
	u32 src_access_mask;
	u32 dst_access_mask;
	u32 dep;
};
struct vk_rp_create_info_t {
	u32 type;
	void *next;
	u32 flags;
	u32 ats_n;
	struct vk_at_desc_t *ats;
	u32 sps_n;
	struct vk_sp_desc_t *sps;
	u32 deps_n;
	struct vk_sp_dep_t *deps;
	u32 correlated_viewmasks_n;
	u32 *correlated_viewmasks;
};
struct vk_sp_begin_info_t {
	u32 type;
	void *next;
	u32 contents;
};
struct vk_sp_end_info_t {
	u32 type;
	void *next;
};
struct vk_fb_create_info_t {
	u32 type;
	void *next;
	u32 flags;
	void *rp;
	u32 ats_n;
	void *ats; /* imgviews!!! */
	u32 width;
	u32 height;
	u32 layers_n;
};
struct vk_shmod_create_info_t {
	u32 type;
	void *next;
	u32 flags;
	size_t code_sz; /* bytes_n */
	u32 *code;
};
struct vk_pl_sh_stage_create_info_t {
	u32 type;
	void *next;
	u32 flags;
	u32 stage;
	void *shmod;
	u8 *name;
	void *specialization_info; /* later */
};
struct vk_pl_vtx_input_state_create_info_t {
	u32 type;
	void *next;
	u32 flags;
	u32 not_used_0;
	void *not_used_1;
	u32 not_used_2;
	void *not_used_3;
};
struct vk_pl_input_assembly_state_create_info_t {
	u32 type;
	void *next;
	u32 flags;
	u32 topology;
	u32 prim_restart_ena;
};
/* XXX: this does define a _transformation_ from "normalized" coords ! */
struct vk_viewport_t {
	f32 x;
	f32 y;
	f32 width;
	f32 height;
	f32 depth_min;
	f32 depth_max;
};
struct vk_pl_viewport_state_create_info_t {
	u32 type;
	void *next;
	u32 flags;
	u32 viewports_n;
	struct vk_viewport_t *viewports;
	u32 scissors_n;
	struct vk_rect_2d_t *scissors;
};
struct vk_pl_raster_state_create_info_t {
	u32 type;
	void *next;
	u32 flags;
	u32 depth_clamp_ena;
	u32 raster_discard_ena;
	u32 polygon_mode;
	u32 cull_mode;
	u32 front_face;
	u32 depth_bias_ena;
	f32 depth_bias_constant_factor;
	f32 depth_bias_clamp;
	f32 depth_bias_slope_factor;
	f32 line_width;
};
struct vk_pl_multisample_state_create_info_t {
	u32 type;
	void *next;
	u32 flags;
	u32 raster_samples_n; /* flag */
	u32 sample_shading_ena;
	f32 sample_shading_min;
	u32 *sample_mask;
	u32 alpha_to_coverage_ena;
	u32 alpha_to_one_ena;
};
struct vk_pl_color_blend_at_state_t {
	u32 blend_ena;

	u32 src_color_blend_factor;
	u32 dst_color_blend_factor;
	/* normalized integer */
	u32 color_blend_op;

	u32 src_alpha_blend_factor;
	u32 dst_alpha_blend_factor;
	u32 alpha_blend_op;

	/* normalized integer */ 
	/* XXX: must always be set, blending enabled or disabled */
	u32 color_write_mask;
};
struct vk_pl_color_blend_state_create_info_t {
	u32 type;
	void *next;
	u32 flags;
	u32 logic_op_ena;
	/* floats */
	u32 logic_op;
	u32 ats_n;
	struct vk_pl_color_blend_at_state_t *ats;
	f32 blend_consts[4];
};
struct vk_pushconst_range_t {
	u32 shader_stages;
	u32 offset;
	u32 size;
};
struct vk_pl_layout_create_info_t {
	u32 type;
	void *next;
	u32 flags;
	u32 layouts_n;
	void **layouts;
	u32 pushconst_ranges_n;
	struct vk_pushconst_range_t *ranges;
};
struct vk_gfx_pl_create_info_t {
	u32 type;
	void *next;
	u32 flags;
	u32 sh_stages_n;
	struct vk_pl_sh_stage_create_info_t *sh_stages;
	struct vk_pl_vtx_input_state_create_info_t *vtx_input_state;
	struct vk_pl_input_assembly_state_create_info_t *input_assembly_state;
	void *dont_use_0;
	struct vk_pl_viewport_state_create_info_t *viewport_state;
	struct vk_pl_raster_state_create_info_t *raster_state;
	struct vk_pl_multisample_state_create_info_t *multisample_state;
	void *not_used_0;
	struct vk_pl_color_blend_state_create_info_t *color_blend_state;
	void *not_used_1;
	void *layout;
	void *rp;
	u32 sp;
	void *base_pl;
	u32 base_pl_idx;
};
union vk_clr_color_val_t {
	f32 f32s[4];
	u32 u32s[4];
	s32 s32s[4];
};
struct vk_clr_depth_stencil_val_t {
	f32 depth;
	u32 stencil;
};
union vk_clr_val_t {
	union vk_clr_color_val_t		color;
	struct vk_clr_depth_stencil_val_t	depth_stencil;
};
struct vk_rp_begin_info_t {
	u32 type;
	void *next;
	void *rp;
	void *fb;
	struct vk_rect_2d_t render_area;
	u32 clr_vals_n;
	union vk_clr_val_t *clr_vals;
};
#endif
