#ifndef NYANVK_CONSTS_H
#define NYANVK_CONSTS_H
/*
 * this is public domain without any warranties of any kind
 * Sylvain BERTRAND
 */
/* XXX: KEEP AN EYE ON ABBREVIATIONS, ALWAYS */
/*
 * 64 bits platforms: enums do default to 32 bits, but can go up to 64 bits
 * based on the range of vals they hold. this is important for
 * vulkan ABI which we will fix.
 * _individually_, each val is defaulted to 32bits, if possible, and signed
 * or not.
 * XXX: All vulkan enums use 32 bits storage
 */
enum {
        vk_err_out_of_host_mem		= -1,
	/*--------------------------------------------------------------------*/
	vk_success			= 0,
	vk_incomplete			= 5,
	vk_r_enum_max			= 0x7fffffff
};
enum {
	vk_struct_type_instance_create_info			= 1,
	vk_struct_type_dev_q_create_info			= 2,
	vk_struct_type_dev_create_info				= 3,
	vk_struct_type_submit_info				= 4,
	vk_struct_type_mem_alloc_info				= 5,
	vk_struct_type_sem_create_info				= 9,
	vk_struct_type_img_create_info				= 14,
	vk_struct_type_imgview_create_info			= 15,
	vk_struct_type_shmod_create_info			= 16,
	vk_struct_type_pl_sh_stage_create_info			= 18,
	vk_struct_type_pl_vtx_input_state_create_info		= 19,
	vk_struct_type_pl_input_assembly_state_create_info	= 20,
	vk_struct_type_pl_viewport_state_create_info		= 22,
	vk_struct_type_pl_raster_state_create_info		= 23,
	vk_struct_type_pl_multisample_state_create_info		= 24,
	vk_struct_type_pl_color_blend_state_create_info		= 26,
	vk_struct_type_gfx_pl_create_info			= 28,
	vk_struct_type_pl_layout_create_info			= 30,
	vk_struct_type_fb_create_info				= 37,
	vk_struct_type_cp_create_info				= 39,
	vk_struct_type_cb_alloc_info				= 40,
	vk_struct_type_cb_begin_info				= 42,
	vk_struct_type_rp_begin_info				= 43,
	vk_struct_type_img_mem_barrier				= 45,
	/* extension number 2 or index 1, offset 0 */ 
	vk_struct_type_swpchn_create_info	= 1000000000 + 1000 + 0,
	/* extension number 2 or index 1, offset 1 */ 
	vk_struct_type_present_info		= 1000000000 + 1000 + 1,
	/* extension number 6 or index 5, offset 0 */ 
	vk_struct_type_xcb_surf_create_info	= 1000000000 + 5000 + 0,
	/* extension number 60 or index 59, offset 1 */
	vk_struct_type_phydev_props		= 1000000000 + 59000 + 1,
	/* extension number 60 or index 59, offset 5 */
	vk_struct_type_q_fam_props		= 1000000000 + 59000 + 5,
	/* extension number 60 or index 59, offset 6 */
	vk_struct_type_phydev_mem_props		= 1000000000 + 59000 + 6,
	/* extension number 60 or index 59, offset 10 */
	vk_struct_type_acquire_next_img_info	= 1000000000 + 59000 + 10,
	/* extension number 110 or index 109, offset 0 */
    	vk_struct_type_at_desc			= 1000000000 + 109000 + 0,
	/* extension number 110 or index 109, offset 1 */
    	vk_struct_type_at_ref			= 1000000000 + 109000 + 1,
	/* extension number 110 or index 109, offset 2 */
    	vk_struct_type_sp_desc			= 1000000000 + 109000 + 2,
	/* extension number 110 or index 109, offset 4 */
    	vk_struct_type_rp_create_info		= 1000000000 + 109000 + 4,
	/* extension number 110 or index 109, offset 5 */
    	vk_struct_type_sp_begin_info		= 1000000000 + 109000 + 5,
	/* extension number 110 or index 109, offset 6 */
    	vk_struct_type_sp_end_info		= 1000000000 + 109000 + 6,
	/* extension number 120 or index 119, offset 0 */
    	vk_struct_type_phydev_surf_info		= 1000000000 + 119000 + 0,
	/* extension number 120 or index 119, offset 1 */
    	vk_struct_type_surf_caps		= 1000000000 + 119000 + 1,
	/* extension number 120 or index 119, offset 2 */
	vk_struct_type_surf_texel_mem_blk_conf	= 1000000000 + 119000 + 2,
	/* extension number 147 or index 146, offset 1 */
	vk_struct_type_img_mem_rqmts_info	= 1000000000 + 146000 + 1,
	/* extension number 147 or index 146, offset 3 */
	vk_struct_type_mem_rqmts		= 1000000000 + 146000 + 3,
	/* extension number 158 or index 157, offset 1 */
	vk_struct_type_bind_img_mem_info	= 1000000000 + 157000 + 1,
	vk_struct_type_enum_max = 0x7fffffff
};
enum {
	vk_phydev_type_other		= 0,
	vk_phydev_type_integrated_gpu	= 1,
	vk_phydev_type_discrete_gpu	= 2,
	vk_phydev_type_virtual_gpu	= 3,
	vk_phydev_type_cpu		= 4,
	vk_phydev_type_enum_max		= 0x7fffffff
};
enum {
	vk_q_gfx_bit			= 0x00000001,
	vk_q_compute_bit		= 0x00000002,
	vk_q_transfer_bit		= 0x00000004,
	vk_q_sparse_binding_bit		= 0x00000008,
	vk_q_protected_bit		= 0x00000010,
	vk_q_flag_bits_enum_max		= 0x7fffffff
};
enum {
	vk_cp_create_transient_bit		= 0x00000001,
	vk_cp_create_reset_cb_bit		= 0x00000002,
	vk_cp_create_flag_bits_enum_max		= 0x7fffffff
};
enum {
	vk_texel_mem_blk_fmt_undefined		= 0,
	vk_texel_mem_blk_fmt_b8g8r8a8_unorm	= 44,
	vk_texel_mem_blk_fmt_b8g8r8a8_srgb	= 50,
	vk_texel_mem_blk_fmt_enum_max		= 0x7fffffff
};
enum {
	vk_color_space_srgb_nonlinear	= 0,
	vk_color_space_enum_max		= 0x7fffffff
};
enum {
	vk_mem_prop_dev_local_bit	= 0x00000001,
	vk_mem_prop_host_visible_bit	= 0x00000002,
	vk_mem_prop_host_cached_bit	= 0x00000008,
	vk_mem_prop_bits_enum_max	= 0x7fffffff
};
enum {
	vk_mem_heap_dev_local_bit	= 0x00000001,
	vk_mem_heap_multi_instance_bit	= 0x00000002,
	vk_mem_heap_bits_enum_max	= 0x7fffffff
};
enum {
	vk_surf_transform_identity_bit		= 0x00000001,
	vk_surf_transform_bits_enum_max		= 0x7fffffff
};
enum {
	vk_composite_alpha_opaque_bit		= 0x00000001,
	vk_composite_alpha_bits_enum_max	= 0x7fffffff
};
enum {
	vk_img_usage_transfer_src_bit		= 0x00000001,
	vk_img_usage_transfer_dst_bit		= 0x00000002,
	vk_img_usage_color_attachment_bit	= 0x00000010,
	vk_img_usage_bits_enum_max		= 0x7fffffff
};
enum {
	vk_sharing_mode_exclusive	= 0,
	vk_sharing_mode_enum_max	= 0x7fffffff
};
enum {
	vk_present_mode_immediate	= 0,
	vk_present_mode_mailbox		= 1,
	vk_present_mode_fifo		= 2,
	vk_present_mode_fifo_relaxed	= 3,
	vk_present_mode_enum_max	= 0x7fffffff
};
enum {
	vk_img_type_2d		= 1,
	vk_img_type_enum_max	= 0x7fffffff
};
enum {
	vk_samples_n_1_bit		= 0x00000001,
	vk_samples_n_bits_enum_max	= 0x7fffffff
};
enum {
	vk_img_tiling_optimal	= 0,
	vk_img_tiling_linear	= 1,
	vk_img_tiling_enum_max	= 0x7fffffff
};
enum {
	vk_img_create_flag_2d_array_compatible_bit	= 0x00000002,
	vk_img_create_flag_enum_max			= 0x7fffffff
};
enum {
	vk_img_layout_undefined		= 0,
	vk_img_layout_general		= 1,
	vk_img_layout_color_at_optimal	= 2,
	/* extension number 2 or index 1, offset 2 */ 
	vk_img_layout_present		= 1000000000 + 1000 + 2,
	vk_img_layout_enum_n_max	= 0x7fffffff
};
enum {
	/* more */
	vk_pl_stage_top_of_pipe_bit	= (1 << 0),
	vk_pl_stage_bottom_of_pipe_bit	= (1 << 13),
	vk_pl_stage_bits_enum_max	= 0x7fffffff
};
enum {
	vk_img_aspect_color_bit		= 1,
	vk_img_aspect_bits_enum_max	= 0x7fffffff
};
enum {
	vk_cb_lvl_primary	= 0,
	vk_cb_lvl_enum_max	= 0x7fffffff
};
enum {
	vk_cb_usage_one_time_submit_bit	= 0x00000001,
	vk_cb_usage_enum_max		= 0x7fffffff
};
enum {
	vk_comp_swizzle_identity	= 0,
	vk_comp_swizzle_enum_max	= 0x7fffffff
};
enum {
	vk_imgview_type_2d		= 1,
	vk_imgview_type_enum_max	= 0x7fffffff
};
enum {
	vk_at_load_op_load = 0,
	vk_at_load_op_clr = 1,
	vk_at_load_op_dont_care = 2,
	vk_at_load_op_enum_max = 0x7fffffff
};
enum {
	vk_at_store_op_store = 0,
	vk_at_store_op_dont_care = 1,
	vk_at_store_op_enum_max = 0x7fffffff
};
enum {
	vk_pl_bind_point_gfx = 0,
	vk_pl_bind_point_compute = 1,
	vk_pl_bind_point_enum_max = 0x7fffffff
};
enum {
	vk_sh_stage_vtx_bit		= 0x00000001,
	vk_sh_stage_frag_bit		= 0x00000010,
	/* meh */
	vk_sh_stage_compute_bit		= 0x00000020,
	vk_sh_stage_bits_enum_max	= 0x7fffffff
};
enum {
	vk_prim_topology_triangle_list = 3,
	/* more */
	vk_prim_topology_enum_max = 0x7fffffff
};
enum {
	vk_logic_op_copy	= 3,
	vk_logic_op_enum_max	= 0x7fffffff
};
enum {
	vk_color_comp_r_bit		= 0x00000001,
	vk_color_comp_g_bit		= 0x00000002,
	vk_color_comp_b_bit		= 0x00000004,
	vk_color_comp_a_bit		= 0x00000008,
	vk_color_comp_bits_enum_max	= 0x7fffffff
};
enum {
	vk_blend_factor_zero			= 0,
	vk_blend_factor_one			= 1,
	vk_blend_factor_src_alpha		= 6,
	vk_blend_factor_one_minus_src_alpha	= 7,
	vk_blend_factor_enum_max		= 0x7fffffff
};
enum {
	vk_blend_op_add			= 0,
	vk_blend_op_substract		= 1,
	vk_blend_op_reverse_substract	= 2,
	vk_blend_op_min			= 3,
	vk_blend_op_max			= 4,
	vk_blend_op_enum_max		= 0x7fffffff
};
enum {
	vk_sp_contents_inline	= 0,
	/* more? */
	vk_sp_contents_enum_max	= 0x7fffffff
};
enum {
	vk_polygon_mode_fill		= 0,
	/* more */
	vk_polygon_mode_enum_max	= 0x7fffffff
};
enum {
	vk_cull_mode_none		= 0,
	vk_cull_mode_front_bit		= 0x00000001,
	vk_cull_mode_back_bit		= 0x00000002,
	vk_cull_mode_bits_enum_max	= 0x7fffffff
};
enum {
	vk_front_face_counter_clockwise	= 0,
	vk_front_face_clockwise		= 1,
	vk_front_face_enum_max		= 0x7fffffff
};
#endif
