#ifndef NYANVK_SYMS_DEV_H
#define NYANVK_SYMS_DEV_H
/*
 * this is public domain without any warranties of any kind
 * Sylvain BERTRAND
 */
/* XXX: define VK_DEV_SYMS and cherry pick what you actually use */
#define VK_DEV_SYMS_FULL \
	void (*dl_vk_get_dev_q)(void *dev, u32 fam, u32 q_idx, void **q); \
	s32 (*dl_vk_create_cp)( \
				void *dev, \
				struct vk_cp_create_info_t *create_info, \
				void *allocator, \
				void **vk_cp); \
	s32 (*dl_vk_create_swpchn)( \
			void *dev, \
			struct vk_swpchn_create_info_t *info, \
			void *allocator, \
			void **swpchn); \
	s32 (*dl_vk_get_swpchn_imgs)( \
				void *dev, \
				void *swpchn, \
				u32 *imgs_n, \
				void **imgs); \
	s32 (*dl_vk_create_img)( \
			void *dev, \
			struct vk_img_create_info_t *info, \
			void *allocator, \
			void **img); \
	s32 (*dl_vk_get_img_mem_rqmts)( \
			void *dev, \
			struct vk_img_mem_rqmts_info_t *info, \
			struct vk_mem_rqmts_t *mem_rqmts); \
	s32 (*dl_vk_alloc_mem)( \
			void *dev, \
			struct vk_mem_alloc_info_t *info, \
			void *allocator, \
			void **mem); \
	s32 (*dl_vk_bind_img_mem)( \
			void *dev, \
			u32 infos_n, \
			struct vk_bind_img_mem_info_t *infos); \
	s32 (*dl_vk_map_mem)( \
			void *dev, \
			void *mem, \
			u64 offset, \
			u64 sz, \
			u32 flags, \
			void **data); \
	s32 (*dl_vk_alloc_cbs)( \
			void *dev, \
			struct vk_cb_alloc_info_t *info, \
			void **cbs); \
	s32 (*dl_vk_begin_cb)( \
			void *cb, \
			struct vk_cb_begin_info_t *info); \
	s32 (*dl_vk_end_cb)(void *cb); \
	void (*dl_vk_cmd_pl_barrier)( \
				void *cb, \
				u32 src_stage, \
				u32 dst_stage, \
				u32 dep_flags, \
				u32 mem_barriers_n, \
				void *mem_barriers, \
				u32 buf_mem_barriers_n, \
				void *buf_mem_barriers, \
				u32 img_mem_barriers_n, \
				struct vk_img_mem_barrier_t *img_mem_barriers); \
	s32 (*dl_vk_q_submit)( \
			void *q, \
			u32 submits_n, \
			struct vk_submit_info_t *submits, \
			void *fence); \
	s32 (*dl_vk_q_wait_idle)(void *q); \
	s32 (*dl_vk_acquire_next_img)( \
				void *dev, \
				struct vk_acquire_next_img_info_t *info, \
				u32 *img_idx); \
	s32 (*dl_vk_q_present)( \
			void *q, \
			struct vk_present_info_t *info); \
	s32 (*dl_vk_create_sem)( \
			void *dev, \
			struct vk_sem_create_info_t *info, \
			void *allocator, \
			void **sem); \
	s32 (*dl_vk_create_imgview)( \
			void *dev, \
			struct vk_imgview_create_info_t *info, \
			void *allocator, \
			void **imgview); \
	s32 (*dl_vk_create_rp)( \
			void *dev, \
			struct vk_rp_create_info_t *info, \
			void *allocator, \
			void **rp); \
	s32 (*dl_vk_create_fb)( \
			void *dev, \
			struct vk_fb_create_info_t *info, \
			void *allocator, \
			void **fb); \
	s32 (*dl_vk_create_shmod)( \
			void *dev, \
			struct vk_shmod_create_info_t *info, \
			void *allocator, \
			void **shmod); \
	s32 (*dl_vk_reset_cb)( \
			void *cb, \
			u32 flags); \
	void (*dl_vk_destroy_shmod)( \
			void *dev, \
			void *shmod, \
			void *allocator); \
	s32 (*dl_vk_create_pl_layout)( \
			void *dev, \
			struct vk_pl_layout_create_info_t *info, \
			void *allocator, \
			void **pl_layout); \
	s32 (*dl_vk_create_gfx_pls)( \
			void *dev, \
			void *pl_cache, \
			u32 create_infos_n, \
			struct vk_gfx_pl_create_info_t *infos, \
			void *allocator, \
			void **pl); \
	void (*dl_vk_get_img_subrsrc_layout)( \
					void *dev, \
					void *img, \
					struct vk_img_subrsrc_t *subrsrc, \
					struct vk_subrsrc_layout_t *layout); \
	void (*dl_vk_cmd_begin_rp)( \
			void *cb, \
			struct vk_rp_begin_info_t *rp_info, \
			struct vk_sp_begin_info_t *sp_info); \
	void (*dl_vk_cmd_end_rp)( \
				void *cb, \
				struct vk_sp_end_info_t *info); \
	void (*dl_vk_cmd_bind_pl)( \
			void *cb, \
			u32 pl_bind_point, \
			void *pl); \
	void (*dl_vk_cmd_blit_img)( \
			void *cb, \
			void *src_img, \
			u32 src_img_layout, \
			void *dst_img, \
			u32 dst_img_layout, \
			u32 regions_n, \
			struct vk_img_blit_t *regions, \
			u32 filter); \
	void (*dl_vk_cmd_draw)( \
			void *cb, \
			u32 vtxs_n, \
			u32 instances_n, \
			u32 first_vtx, \
			u32 first_instance);
#endif
