#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
/* see LICENSE sylvain.bertrand@legeek.net */
/* 
 * FAT WARNING: DO NOT TRY TO GENERALIZE/FACTOR THE CODE TOO EARLY
 *              AND EXCESSIVE GENERALIZATION/FACTORIZATION IS NOT WELCOME
 */
/*
 * ABBREVIATIONS:
 * c : Char
 * b : Byte
 * e : End (usualy the byte past the last valid byte)
 * eq : EQual
 * eol : End Of Line
 * hdr : HeaDeR
 * itf : InTerFace
 * lit : LITeral
 * op : OPcode/OPeration
 * opd : OPerandD
 * s : Start
 * str : STRing
 * strlit : STRing LITeral (the spir-v str storage way)
 * val : VALue
 * var : VARiable
 * ws : White Space
 */
#define u8 uint8_t
#define u16 uint16_t
#define u32 uint32_t
#define f32 float
#define loop for(;;)
/* don't forget the 0 terminating char */
#define U32_STR_BYTES_N_MAX (sizeof("4294967295") + 1)
/* totally arbitrary don't forget the 0 terminating char */
#define F32_STR_BYTES_N_MAX BUFSIZ

#define err(fmt,...) fprintf(stderr, fmt, ##__VA_ARGS__)

/* some function return values */
#define OK		0
#define LAST_LINE	1
#define EOL		2
#define ERR		3

#define SPIRV_LIMITS_ID_MAX 0xffff
/* the extra opds some op can have, used to book some memory */
#define VAR_OPDS_N_MAX 1024

#define LINE_SZ_MAX (1 << 13) /* 2^13 = 8192 */
static u8 line[LINE_SZ_MAX]; /* must have a terminating \n */
static u32 line_num;
static u8 *b; /* byte current line position in processing */
static u8 *eol; /* pointer on the byte right after the last valid byte */

/*
 * spirv binary WITHOUT THE HEADER, because we need the upper id bound
 * value which will know at the end of the assembly
 */
static u32 *binary;
static u32 binary_words_n;

/* 1 bits for each defined id */
static u32 ids[(SPIRV_LIMITS_ID_MAX + 1) / 32];

static void emit(u32 word)
{
	++binary_words_n;

	binary = realloc(binary, binary_words_n * sizeof(word));

	binary[binary_words_n - 1] = word;
}


static u8 id_use(u32 id, u8 **err_str)
{
	static u8 err_buf[BUFSIZ];
	u32 word;
	u32 word_bit;

	*err_str = err_buf;

	if (id > SPIRV_LIMITS_ID_MAX) {
		snprintf(err_buf, BUFSIZ, "%%%u is out of spirv universal limits %u", id, SPIRV_LIMITS_ID_MAX);
		return ERR;
	}
	word = id / 32;
	word_bit = id % 32;

	if ((ids[word] & (1 << word_bit)) != 0) {
		snprintf(err_buf, BUFSIZ, "%%%u is already in use", id);
		return ERR;
	}

	ids[word] |= (1 << word_bit);
	return OK;
}

static u32 ids_bound(void)
{
	u32 word;
	u8 bit;

	/* top idx of the bit array of ids */
	word = (SPIRV_LIMITS_ID_MAX + 1) / 32 - 1;
	loop {
		if (ids[word] != 0) {
			bit = 31;
			loop { /* we are sure to get a set bit */
				if (((1 << bit) & ids[word]) != 0)
					break;
				bit--;
			}
			return (32 * word + bit) + 1;
		}
		if (word == 0)
			break;
		word--;
	}
	return 0; /* no ids... */
}

static void out_hdr(void)
{
	u32 word;
	word = 0x07230203; /* magic number */
	fwrite(&word, sizeof(word), 1, stdout);

	word = 0x00010000; /* version 0xxx1.xxx0 */
	fwrite(&word, sizeof(word), 1, stdout);

	word = 0xdeadbeef; /* generator magic number, namely ours */
	fwrite(&word, sizeof(word), 1, stdout);

	word = ids_bound();
	fwrite(&word, sizeof(word), 1, stdout); /* the spirv id upper bound */

	word = 0;
	fwrite(&word, sizeof(word), 1, stdout); /* 0 */
};


/*
 * no, we don't use libc "getlines".
 * does shield the rest of the program against reading hazards.
 * return OK, LAST_LINE and ERR.
 */
static u8 read_line(void)
{
	u16 total_read_bytes_n;

	total_read_bytes_n = 0;
	b = line;
	loop {	
		size_t read_bytes_n;

		read_bytes_n = fread(b, 1, 1, stdin);
		if (read_bytes_n != 1)	{
			if (feof(stdin) != 0) {
				eol = b;
				return LAST_LINE;
			}
			if (ferror(stdin) != 0)
				return ERR;
		}

		if (*b == '\n') {
			/* TODO: should be done by the preprocesor */
			/* check for line continuation */
			if ((b != line) && (b[-1] == '\\')) {
				/* rewind "\\n" and keep reading */
				total_read_bytes_n -= 2;
				b -= 2;
				continue;
			}
			eol = b;
			return OK;
		}

		++total_read_bytes_n;

		/* no more room for the terminating \n */
		if (total_read_bytes_n == LINE_SZ_MAX)
			return ERR;

		++b;
	}
}

static bool is_ws(u8 c)
{
	/* we don't support all utf-8 encoded white spaces */
	if ( c == ' ' || c == '\t')
		return true;
	return false;
}

/* return OK or EOL */
static u8 ws_skip(void)
{
	loop {
		if (b == eol)
			return EOL;
		if (!is_ws(*b))
			return OK;
		++b;
	}
}

static void ws_or_eol_or_eq_reach(void)
{
	loop {
		if (b == eol || *b == '=' || is_ws(*b))	
			return;
		++b;
	}
}

static void ws_or_eol_or_bar_reach(void)
{
	loop {
		if (b == eol || *b == '|' || is_ws(*b))	
			return;
		++b;
	}
}

static void ws_or_eol_reach(void)
{
	loop {
		if (b == eol || is_ws(*b))	
			return;
		++b;
	}
}

static bool str_slice_cmp(u8 *str, u8 *s, u8 *e)
{
	loop {
		if (s == e && *str == 0)
			return true;

		if (s == e && *str != 0)
			return false;

		if (s != e && *str == 0)
			return false;

		if (*str != *s)
			return false;

		++str;
		++s;
	}
}

/* expect a valid char pointed by s */
static u8 id_get(u32 *id, u8 *s, u8 *e, u8 **err_str)
{
	u32 u32_str[U32_STR_BYTES_N_MAX]; /* include the 0 terminating char */
	char *end_ptr;
	unsigned long val;

	*err_str = "no error";


	if (*s != '%') { /* an id start with '%' */
		*err_str = "missing '%' at the beginning of the id";
		return ERR;
	}

	++s;

	if (s == e) {/* empty id */
		*err_str = "the id is empty";
		return ERR;
	}

	/* too big for a u32 */
	if ((e - s) > U32_STR_BYTES_N_MAX) {
		*err_str = "the id overflows its maximum storage size, a non signed 32 bits integer";
		return ERR;
	}

	/* extract the id str to please system strtoul */
	memset(u32_str, 0, sizeof(u32_str));
	memcpy(u32_str, s, e - s);

	val = strtoul((void*)u32_str, &end_ptr, 10);
	if (*end_ptr != 0) {
		*err_str = "system string to number conversion failed";
		return ERR; /* got an invalid char */
	}

	*id = (u32)val;
	return OK;
}


/* the global b variable will point on the first valid char of the opd val */
static u8 opd_val_reach(u8 *op_str, u8 *opd_str)
{
	u8 r;

	r = ws_skip();
	if (r != OK) {
		err("ERROR:%u:%s:%s:missing\n", line_num, op_str, opd_str);
		exit(1);
	}

	if (*b != '=') {
		err("ERROR:%u:%s:%s:missing value assignment equal char\n", line_num, op_str, opd_str);
		exit(1);
	}
	
	++b;

	if (b == eol) {
		err("ERROR:%u:%s:%s:missing value\n", line_num, op_str, opd_str);
		exit(1);
	}

	r = ws_skip();
	if (r != OK) {
		err("ERROR:%u:%s:%s:missing value\n", line_num, op_str, opd_str);
		exit(1);
	}
}

static u32 opd_id(u8 *op, u8 *name, u8 *desc)
{
	u8 r;
	u8 *opd_s;
	u8 *opd_e;
	u8 *err_str;
	u32 id;

	if (name != 0)
		opd_val_reach(op, name);

	opd_s = b;
	ws_or_eol_reach();
	opd_e = b;

	r = id_get(&id, opd_s, opd_e, &err_str);
	if (r != OK) {
		err("ERROR:%u:%s:something is wrong with the %s format:%s\n", line_num, op, desc, err_str);
		exit(1);
	}
	return id;
}

static void op_capability(void)
{
	u8 r;
	u8 *cap_s;
	u8 *cap_e;
	u32 cap;
	u32 op;

	r = ws_skip();
	if (r == EOL) {
		err("ERROR:%u:capability:missing capability\n", line_num);
		exit(1);
	}

	cap_s = b;
	ws_or_eol_reach();
	cap_e = b;

	if      (str_slice_cmp("matrix", cap_s, cap_e))
			cap = 0;
	else if (str_slice_cmp("shader", cap_s, cap_e))
			cap = 1;
	/* TODO: MORE */
	else {
		err("ERROR:%u:capability:unknown capability \"%.*s\"\n", line_num, cap_e - cap_s, cap_s);
		exit(1);
	}

	op = 17;
	op |= (2 << 16);
	emit(op);
	emit(cap);
}

static u32 opd_addressing_model(u8 *op)
{
	u8 *opd_s;
	u8 *opd_e;
			
	opd_val_reach(op, "addressing_model");

	opd_s = b;
	ws_or_eol_reach();
	opd_e = b;
			
	if (str_slice_cmp("logical", opd_s, opd_e))
		return 0;
	else if (str_slice_cmp("physical32", opd_s, opd_e))
		return 1;
	else if (str_slice_cmp("physical64", opd_s, opd_e))
		return 2;
	/* TODO: more */

	err("ERROR:%u:%s:addressing_model operand:unknown value \"%.*s\"\n", line_num, op, opd_e - opd_s, opd_s);
	exit(1);
}

static u32 opd_memory_model(u8 *op)
{
	u8 *opd_s;
	u8 *opd_e;
			
	opd_val_reach(op, "memory_model");

	opd_s = b;
	ws_or_eol_reach();
	opd_e = b;
			
	if (str_slice_cmp("simple", opd_s, opd_e))
		return 0;
	else if (str_slice_cmp("glsl450", opd_s, opd_e))
		return 1;
	else if (str_slice_cmp("opencl", opd_s, opd_e))
		return 2;
	else if (str_slice_cmp("vulkan", opd_s, opd_e))
		return 3;

	err("ERROR:%u:%s:memory_model operand:unknown value \"%.*s\"\n", line_num, op, opd_e - opd_s, opd_s);
	exit(1);
}

static void op_memory_model(void)
{
	bool have_addressing_model;
	u32 addressing_model;
	bool have_memory_model;
	u32 memory_model;
	u32 op;

	have_addressing_model = false;
	have_memory_model = false;

	loop {
		u8 r;
		u8 *opd_s;
		u8 *opd_e;

		r = ws_skip();
		if (r == EOL) {
			err("ERROR:%u:memory_model:missing operands\n", line_num);
			exit(1);
		}

		opd_s = b;
		ws_or_eol_or_eq_reach();
		opd_e = b;

		if (str_slice_cmp("addressing_model", opd_s, opd_e)) {
			addressing_model = opd_addressing_model("memory_model");
			have_addressing_model = true;
		} else if (str_slice_cmp("memory_model", opd_s, opd_e)) {
			memory_model = opd_memory_model("memory_model");
			have_memory_model = true;
		} else {
			err("ERROR:%u:memory_model:unknown operand \"%.*s\"\n", line_num, opd_e - opd_s, opd_s);
			exit(1);
		}

		if (have_addressing_model && have_memory_model)
			break;
	}

	op = 14;
	op |= (3 << 16);
	emit(op);
	emit(addressing_model);
	emit(memory_model);
}

static u32 opd_execution_model(u8 *op)
{
	u8 *opd_s;
	u8 *opd_e;
			
	opd_val_reach(op, "execution_model");

	opd_s = b;
	ws_or_eol_reach();
	opd_e = b;
			
	if (str_slice_cmp("vertex", opd_s, opd_e))
		return 0;
	else if (str_slice_cmp("tessellation_control", opd_s, opd_e))
		return 1;
	else if (str_slice_cmp("tessellation_evaluation", opd_s, opd_e))
		return 2;
	else if (str_slice_cmp("geometry", opd_s, opd_e))
		return 3;
	else if (str_slice_cmp("fragment", opd_s, opd_e))
		return 4;
	else if (str_slice_cmp("glcompute", opd_s, opd_e))
		return 5;
	else if (str_slice_cmp("kernel", opd_s, opd_e))
		return 6;
	/* TODO: more */

	err("ERROR:%u:%s:execution_model operand:unknown value \"%.*s\"\n", line_num, op, opd_e - opd_s, opd_s);
	exit(1);
}

static u32 opd_entry_point(u8 *op)
{
	u8 *opd_s;
	u8 *opd_e;
	u32 id;
	u8 r;
	u8 *err_str;
			
	opd_val_reach(op, "entry_point");

	opd_s = b;
	ws_or_eol_reach();
	opd_e = b;

	r = id_get(&id, opd_s, opd_e, &err_str);
	if (r != OK) {
		err("ERROR:%u:%s:entry_point operand:invalid id \"%.*s\":%s\n", line_num, op, opd_e - opd_s, opd_s, err_str);
		exit(1);
	}
	return id;
}

/* will update the variable b to point right after the closing '"' */
#define STRLIT_WORDS_N_MAX (LINE_SZ_MAX >> 2) /* good approx :) */
static u8 strlit_get(u32 **strlit, u32 *strlit_words_n, u8 **err)
{
	static u32 buf[STRLIT_WORDS_N_MAX];
	u8 *s;
	u32 bytes_n;

	memset(buf, 0, STRLIT_WORDS_N_MAX << 2);
	*strlit = (u32*)buf;
	*strlit_words_n = 0;

	if (*b != '"') {
		*err = "string literal does not start with a '\"'";
		return ERR;
	}

	s = b;
	++b; /* consume the starting '"' */

	loop {
		if (b == eol) {
			*err = "string literal is missing the closing '\"'";
			return ERR;
		}

		if (*b == '"')
			break;

		++b;
	}

	/* here, b does point on the closing '"' */
	bytes_n = b - (s + 1);
	memcpy(buf, s + 1, bytes_n); /* don't include the '"'s */

	/* don't forget to add the terminating 0 */
	bytes_n += 1;
	*strlit_words_n = bytes_n / 4 + ((bytes_n % 4) != 0 ? 1 : 0);

	++b; /* consume the closing '"' */
	return OK;
}
#undef STRLIT_WORDS_N_MAX

static void opd_name(u8 *op, u32 **strlit, u32 *strlit_words_n)
{
	u8 r;
	u8 *err_str;

	opd_val_reach(op, "name");

	r = strlit_get(strlit, strlit_words_n, &err_str);
	if (r != OK) {
		err("ERROR:%u:%s:name operand:invalid string literal:%s\n", line_num, op, err_str);
		exit(1);
	}
}

/* expect b to point on a valid char, will consume the line */
static u8 ids_collect(u32 *ids, u32 *ids_n, u8 **err_str)
{
	static u8 err_str_buf[BUFSIZ];

	err_str_buf[0] = 0;
	*err_str = err_str_buf;
	*ids_n = 0;

	loop {
		u8 r;
		u8 *s;
		u8 *e;
		u8 *id_get_err_str;

		s = b;
		ws_or_eol_reach();
		e = b;

		r = id_get(ids, s, e, &id_get_err_str);
		if (r != OK) {
			snprintf(err_str_buf, BUFSIZ, "id[%u]:%s", *ids_n, id_get_err_str);
			return ERR;
		}
		++ids;
		++(*ids_n);

		r = ws_skip();
		if (r != OK)
			return OK; /* got all ids */
	}
}

static void op_entry_point(void)
{
	u8 r;
	bool have_execution_model;
	u32 execution_model;
	bool have_entry_point;
	u32 entry_point;
	bool have_name;
	u32 *name;
	u32 name_words_n;
	u8 *err_str;

	u32 itf_ids[VAR_OPDS_N_MAX];
	u32 itf_ids_n;

	u32 word;

	have_execution_model = false;
	have_entry_point = false;
	have_name = false;
	itf_ids_n = 0;

	loop {
		u8 *opd_s;
		u8 *opd_e;

		r = ws_skip();
		if (r == EOL) {
			err("ERROR:%u:entry_point:missing operands\n", line_num);
			exit(1);
		}

		opd_s = b;
		ws_or_eol_or_eq_reach();
		opd_e = b;
	
		if (str_slice_cmp("execution_model", opd_s, opd_e)) {
			execution_model = opd_execution_model("entry_point");
			have_execution_model = true;
		} else if (str_slice_cmp("entry_point", opd_s, opd_e)) {
			entry_point = opd_id("entry_point", "entry_point",
							"entry point id");
			have_entry_point = true;
		} else if (str_slice_cmp("name", opd_s, opd_e)) {
			opd_name("entry_point", &name, &name_words_n);
			have_name = true;
		} else {
			err("ERROR:%u:entry_point:unknown operand \"%.*s\"\n", line_num, opd_e - opd_s, opd_s);
			exit(1);
		}

		if (have_execution_model && have_entry_point && have_name)
			break;
	}

	r = ws_skip();
	if (r == OK) {
		/* have id(s) of interface var(s) */
		r = ids_collect(itf_ids, &itf_ids_n, &err_str);
		if (r != OK) {
			err("ERROR:%u:entry_point:collecting identifiers of interface variables:%s\n",  line_num, err_str);
			exit(1);
		}
	}

	word = 15;
	word |= (3 + name_words_n + itf_ids_n) << 16;
	emit(word);

	emit(execution_model);
	emit(entry_point);

	word = 0;
	loop {
		if (word == name_words_n)
			break;
		emit(name[word]);
		++word;
	}

	word = 0;
	loop {
		if (word == itf_ids_n)
			break;
		emit(itf_ids[word]);
		++word;
	}
}

/* for the 2 abstract types void and bool */
static void op_type_x(u8 *name, u32 op)
{
	u8 r;
	u8 *id_s;
	u8 *id_e;
	u32 id;
	u8 *err_str;

	r = ws_skip();
	if (r == EOL) {
		err("ERROR:%u:type_%s:missing identifier\n", line_num, name);
		exit(1);
	}

	id_s = b;
	ws_or_eol_reach();
	id_e = b;

	r = id_get(&id, id_s, id_e, &err_str);
	if (r != OK) {
		err("ERROR:%u:type_%s:unable to get the id:%s\n", line_num, name, err_str);
		exit(1);
	}

	op |= (2 << 16);
	emit(op);
	emit(id);

	r = id_use(id, &err_str);
	if (r != OK) {
		err("ERROR:%u:type_%s:something is wrong with the id:%s\n", line_num, name, err_str);
		exit(1);
	}
}

static bool has_radix(u8 *s, u8 *e)
{
	loop {
		if (s == e)
			break;	
		if (*s == '.')
			return true;
		++s;
	}
	return false;
}

static u8 f32_decode(f32 *f_32, u8 *s, u8 *e, u8 **err_str)
{
	char *end_ptr;
	/* include the 0 terminating char */
	u8 f32_str[F32_STR_BYTES_N_MAX];
	float f;

	/* extract the u32 str to please system strtof */
	memset(f32_str, 0, sizeof(f32_str));
	memcpy(f32_str, s, e - s);

	f = strtof(f32_str, &end_ptr); /* banzai */
	if (*end_ptr != 0) {
		*err_str = "system string to float number conversion failed";
		return ERR;
	}
	*f_32 =  (f32)f; /* banzai */
	return OK;
}

static u8 u32_decode(u32 *u_32, u8 *s, u8 *e, u8 **err_str)
{
	char *end_ptr;
	u8 u32_str[U32_STR_BYTES_N_MAX]; /* include the 0 terminating char */
	unsigned long u_l;
	int base;

	/* if '0x' or '0X', the base is hexadecimal, else decimal */
	if (((e - s) > 3) && (s[0] == '0') && ((s[1] == 'x') || s[1] == 'X'))
		base = 16;
	else
		base = 10;

	/* extract the u32 str to please system strtoul */
	memset(u32_str, 0, sizeof(u32_str));
	memcpy(u32_str, s, e - s);

	u_l = strtoul((void*)u32_str, &end_ptr, base); /* banzai */
	if (*end_ptr != 0) {
		*err_str = "system string to integer number conversion failed";
		return ERR; /* got an invalid char */
	}
	*u_32 = (u32)u_l; /* banzai */
	return OK;
}

static u8 u32_get(u32 *val_u32, u8 *op, u8 *opd, u8 **err_str)
{
	u8 r;
	u8 *s;
	u8 *e;
	f32 f;
	f32 *pf;

	pf = &f;

	opd_val_reach(op, opd);

	s = b;
	ws_or_eol_reach();
	e = b;

	if (has_radix(s, e)) {
		r = f32_decode(pf, s, e, err_str);
		*val_u32 = *(u32*)pf;
	} else {
		r = u32_decode(val_u32, s, e, err_str);
	}
	return r;
}

/* expect b to point on the first valid char of the first val */
static u8 u32s_collect(u32 *vals, u32 *vals_n, u8 **err_str)
{
	u8 r;
	u8 *s;
	u8 *e;
	f32 f;
	f32 *pf;

	pf = &f;

	*vals_n = 0;

	loop {
		s = b;
		ws_or_eol_reach();
		e = b;

		if (has_radix(s, e)) {
			r = f32_decode(pf, s, e, err_str);
			vals[*vals_n] = *(u32*)pf;
		} else {
			r = u32_decode(&vals[*vals_n], s, e, err_str);
		}

		if (r != OK)
			return r;

		++(*vals_n);

		r = ws_skip();
		if (r != OK) /* no more vals */
			break;
	}
	return OK;
}

static void op_type_float(void)
{
	bool have_id;
	u32 id;
	bool have_width;
	u32 width; 
	u32 word;

	have_id = false;
	have_width = false;

	loop {
		u8 r;
		u8 *opd_s;
		u8 *opd_e;
		u8 *err_str;

		r = ws_skip();
		if (r == EOL) {
			err("ERROR:%u:type_float:missing operands\n", line_num);
			exit(1);
		}

		opd_s = b;
		ws_or_eol_or_eq_reach();
		opd_e = b;
	
		if (str_slice_cmp("id", opd_s, opd_e)) {
			id = opd_id("type_function", "id", "id");

			r = id_use(id, &err_str);
			if (r != OK) {
				err("ERROR:%u:type_float:something is wrong with the id:%s\n", line_num, err_str);
				exit(1);
			}

			have_id = true;
		} else if (str_slice_cmp("width", opd_s, opd_e)) {
			r = u32_get(&width, "type_float", "width", &err_str);
			if (r != OK) {
				err("ERROR:%u:type_float:something is wrong with the width:%s\n", line_num, err_str);
				exit(1);
			}
			have_width = true;
		} else {
			err("ERROR:%u:type_float:unknown operand \"%.*s\"\n", line_num, opd_e - opd_s, opd_s);
			exit(1);
		}

		if (have_id && have_width)
			break;
	}

	word = 22;
	word |= 3 << 16;
	emit(word);

	emit(id);
	emit(width);
}

static void op_type_vector(void)
{
	bool have_id;
	u32 id;
	bool have_component_type;
	u32 component_type;
	bool have_components_n;
	u32 components_n;
	u32 word;

	have_id = false;
	have_components_n = false;

	loop {
		u8 r;
		u8 *opd_s;
		u8 *opd_e;
		u8 *err_str;

		r = ws_skip();
		if (r == EOL) {
			err("ERROR:%u:type_vector:missing operands\n", line_num);
			exit(1);
		}

		opd_s = b;
		ws_or_eol_or_eq_reach();
		opd_e = b;
	
		if (str_slice_cmp("id", opd_s, opd_e)) {
			id = opd_id("type_vector", "id", "id");

			r = id_use(id, &err_str);
			if (r != OK) {
				err("ERROR:%u:type_vector:something is wrong with the id:%s\n", line_num, err_str);
				exit(1);
			}

			have_id = true;
		} else if (str_slice_cmp("component_type", opd_s, opd_e)) {
			component_type = opd_id("type_vector", "component_type",
							"component type id");
			have_component_type = true;
		} else if (str_slice_cmp("components_n", opd_s, opd_e)) {
			r = u32_get(&components_n, "type_vector",
						"components_n", &err_str);
			if (r != OK) {
				err("ERROR:%u:type_vector:something is wrong with the count of components:%s\n", line_num, err_str);
				exit(1);
			}
			have_components_n = true;
		} else {
			err("ERROR:%u:type_vector:unknown operand \"%.*s\"\n", line_num, opd_e - opd_s, opd_s);
			exit(1);
		}

		if (have_id && have_component_type && have_components_n)
			break;
	}

	word = 23;
	word |= 4 << 16;
	emit(word);

	emit(id);
	emit(component_type);
	emit(components_n);

}

static void op_type_function(void)
{
	u8 r;
	bool have_id;
	u32 id;
	bool have_return_type;
	u32 return_type;
	u8 *err_str;

	u32 params[VAR_OPDS_N_MAX];
	u32 params_n;

	u32 word;

	have_id = false;
	have_return_type = false;
	params_n = 0;

	loop {
		u8 *opd_s;
		u8 *opd_e;

		r = ws_skip();
		if (r == EOL) {
			err("ERROR:%u:type_function:missing operands\n", line_num);
			exit(1);
		}

		opd_s = b;
		ws_or_eol_or_eq_reach();
		opd_e = b;
	
		if (str_slice_cmp("id", opd_s, opd_e)) {
			id = opd_id("type_function", "id", "id");

			r = id_use(id, &err_str);
			if (r != OK) {
				err("ERROR:%u:type_function:something is wrong with the id:%s\n", line_num, err_str);
				exit(1);
			}

			have_id = true;
		} else if (str_slice_cmp("return_type", opd_s, opd_e)) {
			return_type = opd_id("type_function", "return_type",
							"return type id");
			have_return_type = true;
		} else {
			err("ERROR:%u:type_function:unknown operand \"%.*s\"\n", line_num, opd_e - opd_s, opd_s);
			exit(1);
		}

		if (have_id && have_return_type)
			break;
	}

	r = ws_skip();
	if (r == OK) {
		/* have param(s) */
		r = ids_collect(params, &params_n, &err_str);
		if (r != OK) {
			err("ERROR:%u:type_function:collecting parameter(s) of function type:%s\n",  line_num, err_str);
			exit(1);
		}
	}

	word = 33;
	word |= (3 + params_n) << 16;
	emit(word);

	emit(id);
	emit(return_type);

	word = 0;
	loop {
		if (word == params_n)
			break;
		emit(params[word]);
		++word;
	}
}

static u32 opd_signedness(u8 *op)
{
	u8 *opd_s;
	u8 *opd_e;

	opd_val_reach(op, "signedness");

	opd_s = b;
	ws_or_eol_reach();
	opd_e = b;
			
	if (str_slice_cmp("unsigned", opd_s, opd_e))
		return 0;
	else if (str_slice_cmp("signed", opd_s, opd_e))
		return 1;

	err("ERROR:%u:%s:signedness operand:unknown value \"%.*s\"\n", line_num, op, opd_e - opd_s, opd_s);
	exit(1);
}

static void op_type_int(void)
{
	bool have_id;
	u32 id;
	bool have_width;
	u32 width; 
	bool have_signedness;
	u32 signedness;
	u32 word;

	have_id = false;
	have_width = false;
	have_signedness = false;

	loop {
		u8 r;
		u8 *opd_s;
		u8 *opd_e;
		u8 *err_str;

		r = ws_skip();
		if (r == EOL) {
			err("ERROR:%u:type_int:missing operands\n", line_num);
			exit(1);
		}

		opd_s = b;
		ws_or_eol_or_eq_reach();
		opd_e = b;
	
		if (str_slice_cmp("id", opd_s, opd_e)) {
			id = opd_id("type_int", "id", "id");

			r = id_use(id, &err_str);
			if (r != OK) {
				err("ERROR:%u:type_int:something is wrong with the id:%s\n", line_num, err_str);
				exit(1);
			}
			have_id = true;
		} else if (str_slice_cmp("width", opd_s, opd_e)) {
			r = u32_get(&width, "type_int", "width", &err_str);
			if (r != OK) {
				err("ERROR:%u:type_int:something is wrong with the width:%s\n", line_num, err_str);
				exit(1);
			}
			have_width = true;
		} else if (str_slice_cmp("signedness", opd_s, opd_e)) {
			signedness = opd_signedness("type_int");
			have_signedness = true;
		} else {
			err("ERROR:%u:type_signedness:unknown operand \"%.*s\"\n", line_num, opd_e - opd_s, opd_s);
			exit(1);
		}

		if (have_id && have_width && have_signedness)
			break;
	}

	word = 21;
	word |= 4 << 16;
	emit(word);

	emit(id);
	emit(width);
	emit(signedness);
}

static u32 opd_storage_class(u8 *op)
{
	u8 *opd_s;
	u8 *opd_e;
			
	opd_val_reach(op, "storage_class");

	opd_s = b;
	ws_or_eol_reach();
	opd_e = b;
			
	if (str_slice_cmp("uniform_constant", opd_s, opd_e))
		return 0;
	else if (str_slice_cmp("input", opd_s, opd_e))
		return 1;
	else if (str_slice_cmp("uniform", opd_s, opd_e))
		return 2;
	else if (str_slice_cmp("output", opd_s, opd_e))
		return 3;
	else if (str_slice_cmp("workgroup", opd_s, opd_e))
		return 4;
	else if (str_slice_cmp("cross_workgroup", opd_s, opd_e))
		return 5;
	else if (str_slice_cmp("private", opd_s, opd_e))
		return 6;
	else if (str_slice_cmp("function", opd_s, opd_e))
		return 7;
	else if (str_slice_cmp("generic", opd_s, opd_e))
		return 8;
	else if (str_slice_cmp("push_constant", opd_s, opd_e))
		return 9;
	else if (str_slice_cmp("atomic_counter", opd_s, opd_e))
		return 10;
	else if (str_slice_cmp("image", opd_s, opd_e))
		return 11;
	else if (str_slice_cmp("storage_buffer", opd_s, opd_e))
		return 12;
	/* TODO: more */

	err("ERROR:%u:%s:storage class operand:unknown value \"%.*s\"\n", line_num, op, opd_e - opd_s, opd_s);
	exit(1);
}

static void op_type_pointer(void)
{
	bool have_id;
	u32 id;
	bool have_storage_class;
	u32 storage_class;
	bool have_type;
	u32 type;
	u32 word;

	have_id = false;
	have_storage_class = false;
	have_type = false;

	loop {
		u8 r;
		u8 *opd_s;
		u8 *opd_e;
		u8 *err_str;

		r = ws_skip();
		if (r == EOL) {
			err("ERROR:%u:type_pointer:missing operands\n", line_num);
			exit(1);
		}

		opd_s = b;
		ws_or_eol_or_eq_reach();
		opd_e = b;
	
		if (str_slice_cmp("id", opd_s, opd_e)) {
			id = opd_id("type_pointer", "id", "id");

			r = id_use(id, &err_str);
			if (r != OK) {
				err("ERROR:%u:type_pointer:something is wrong with the id:%s\n", line_num, err_str);
				exit(1);
			}
			have_id = true;
		} else if (str_slice_cmp("storage_class", opd_s, opd_e)) {
			storage_class = opd_storage_class("type_pointer");
			have_storage_class = true;
		} else if (str_slice_cmp("type", opd_s, opd_e)) {
			/* forward reference is not allowed, may check */
			type = opd_id("type_pointer", "type", "type id");
			have_type = true;
		} else {
			err("ERROR:%u:type_pointer:unknown operand \"%.*s\"\n", line_num, opd_e - opd_s, opd_s);
			exit(1);
		}

		if (have_id && have_type && have_storage_class)
			break;
	}

	word = 32;
	word |= 4 << 16;
	emit(word);

	emit(id);
	emit(storage_class);
	emit(type);

}

/* a spirv "variable" is a pointer to some allocated storage */
static void op_variable(void)
{
	u8 r;
	bool have_type;
	u32 type;
	bool have_pointer_id;
	u32 pointer_id;
	bool have_storage_class;
	u32 storage_class;
	u8 *err_str;

	u32 initializers[VAR_OPDS_N_MAX];
	u32 initializers_n;

	u32 word;

	have_type = false;
	have_pointer_id = false;
	have_storage_class = false;
	initializers_n = 0;

	loop {
		u8 *opd_s;
		u8 *opd_e;

		r = ws_skip();
		if (r == EOL) {
			err("ERROR:%u:variable:missing operands\n", line_num);
			exit(1);
		}

		opd_s = b;
		ws_or_eol_or_eq_reach();
		opd_e = b;
	
		if (str_slice_cmp("type", opd_s, opd_e)) {
			type = opd_id("variable", "type", "type id");
			have_type = true;
		} else if (str_slice_cmp("pointer_id", opd_s, opd_e)) {
			pointer_id = opd_id("variable", "pointer_id", "pointer id");

			r = id_use(pointer_id, &err_str);
			if (r != OK) {
				err("ERROR:%u:variable:something is wrong with the pointer id:%s\n", line_num, err_str);
				exit(1);
			}
			have_pointer_id = true;
		} else if (str_slice_cmp("storage_class", opd_s, opd_e)) {
			storage_class = opd_storage_class("variable");
			have_storage_class = true;
		} else {
			err("ERROR:%u:variable:unknown operand \"%.*s\"\n", line_num, opd_e - opd_s, opd_s);
			exit(1);
		}

		if (have_type && have_pointer_id && have_storage_class)
			break;
	}

	r = ws_skip();
	if (r == OK) {
		/* have initializer(s) */
		r = ids_collect(initializers, &initializers_n, &err_str);
		if (r != OK) {
			err("ERROR:%u:variable:collecting type(s) of initializer(s):%s\n",  line_num, err_str);
			exit(1);
		}
	}

	word = 59;
	word |= (4 + initializers_n) << 16;
	emit(word);

	emit(type);
	emit(pointer_id);
	emit(storage_class);

	word = 0;
	loop {
		if (word == initializers_n)
			break;
		emit(initializers[word]);
		++word;
	}
}

static void decoration_location_opds_consume(u8 *op, u32 *opds, u32 *opds_n)
{
	u8 r;
	u8 *opd_s;
	u8 *opd_e;
	u8 *err_str;

	r = ws_skip();
	if (r == EOL) {
		err("ERROR:%u:%s:location decoration is missing its value\n", line_num, op);
		exit(1);
	}

	opd_s = b;
	ws_or_eol_reach();
	opd_e = b;

	r = u32_decode(opds + *opds_n, opd_s, opd_e, &err_str);
	if (r != OK) {
		err("ERROR:%u:%s:unable to decade location decoration value:%s\n", line_num, op, err_str);
		exit(1);
	}
	++(*opds_n);
}

static void decoration_builtin_opds_consume(u8 *op, u32 *opds, u32 *opds_n)
{
	u8 r;
	u8 *opd_s;
	u8 *opd_e;

	r = ws_skip();
	if (r == EOL) {
		err("ERROR:%u:%s:builtin decoration is missing its operand\n", line_num, op);
		exit(1);
	}

	opd_s = b;
	ws_or_eol_reach();
	opd_e = b;

	if      (str_slice_cmp("position", opd_s, opd_e)) {
		opds[*opds_n] = 0;
		++(*opds_n);
	} else if (str_slice_cmp("point_size", opd_s, opd_e)) {
		opds[*opds_n] = 1;
		++(*opds_n);
	} else if (str_slice_cmp("clip_distance", opd_s, opd_e)) {
		opds[*opds_n] = 3;
		++(*opds_n);
	} else if (str_slice_cmp("cull_distance", opd_s, opd_e)) {
		opds[*opds_n] = 4;
		++(*opds_n);
	} else if (str_slice_cmp("vertex_id", opd_s, opd_e)) {
		opds[*opds_n] = 5;
		++(*opds_n);
	} else if (str_slice_cmp("instance_id", opd_s, opd_e)) {
		opds[*opds_n] = 6;
		++(*opds_n);
	} else if (str_slice_cmp("primitive_id", opd_s, opd_e)) {
		opds[*opds_n] = 7;
		++(*opds_n);
	} else if (str_slice_cmp("invocation_id", opd_s, opd_e)) {
		opds[*opds_n] = 8;
		++(*opds_n);
	} else if (str_slice_cmp("layer", opd_s, opd_e)) {
		opds[*opds_n] = 9;
		++(*opds_n);
	} else if (str_slice_cmp("viewport_index", opd_s, opd_e)) {
		opds[*opds_n] = 10;
		++(*opds_n);
	} else if (str_slice_cmp("tess_level_outer", opd_s, opd_e)) {
		opds[*opds_n] = 11;
		++(*opds_n);
	} else if (str_slice_cmp("tess_level_inner", opd_s, opd_e)) {
		opds[*opds_n] = 12;
		++(*opds_n);
	} else if (str_slice_cmp("tess_coord", opd_s, opd_e)) {
		opds[*opds_n] = 13;
		++(*opds_n);
	} else if (str_slice_cmp("patch_vertices", opd_s, opd_e)) {
		opds[*opds_n] = 14;
		++(*opds_n);
	} else if (str_slice_cmp("frag_coord", opd_s, opd_e)) {
		opds[*opds_n] = 15;
		++(*opds_n);
	} else if (str_slice_cmp("point_coord", opd_s, opd_e)) {
		opds[*opds_n] = 16;
		++(*opds_n);
	} else if (str_slice_cmp("front_facing", opd_s, opd_e)) {
		opds[*opds_n] = 17;
		++(*opds_n);
	} else if (str_slice_cmp("sample_id", opd_s, opd_e)) {
		opds[*opds_n] = 18;
		++(*opds_n);
	} else if (str_slice_cmp("sample_pos", opd_s, opd_e)) {
		opds[*opds_n] = 19;
		++(*opds_n);
	} else if (str_slice_cmp("sample_mask", opd_s, opd_e)) {
		opds[*opds_n] = 20;
		++(*opds_n);
	} else if (str_slice_cmp("frag_depth", opd_s, opd_e)) {
		opds[*opds_n] = 22;
		++(*opds_n);
	} else if (str_slice_cmp("helper_invocation", opd_s, opd_e)) {
		opds[*opds_n] = 23;
		++(*opds_n);
	} else if (str_slice_cmp("num_workgroups", opd_s, opd_e)) {
		opds[*opds_n] = 24;
		++(*opds_n);
	} else if (str_slice_cmp("workgroup_size", opd_s, opd_e)) {
		opds[*opds_n] = 25;
		++(*opds_n);
	} else if (str_slice_cmp("workgroup_id", opd_s, opd_e)) {
		opds[*opds_n] = 26;
		++(*opds_n);
	} else if (str_slice_cmp("local_invocation_id", opd_s, opd_e)) {
		opds[*opds_n] = 27;
		++(*opds_n);
	} else if (str_slice_cmp("global_invocation_id", opd_s, opd_e)) {
		opds[*opds_n] = 28;
		++(*opds_n);
	} else if (str_slice_cmp("local_invocation_index", opd_s, opd_e)) {
		opds[*opds_n] = 29;
		++(*opds_n);
	} else if (str_slice_cmp("work_dim", opd_s, opd_e)) {
		opds[*opds_n] = 30;
		++(*opds_n);
	} else if (str_slice_cmp("global_size", opd_s, opd_e)) {
		opds[*opds_n] = 31;
		++(*opds_n);
	} else if (str_slice_cmp("enqueued_workgroup_size", opd_s, opd_e)) {
		opds[*opds_n] = 32;
		++(*opds_n);
	} else if (str_slice_cmp("global_offset", opd_s, opd_e)) {
		opds[*opds_n] = 33;
		++(*opds_n);
	} else if (str_slice_cmp("global_linear_id", opd_s, opd_e)) {
		opds[*opds_n] = 34;
		++(*opds_n);
	} else if (str_slice_cmp("subgroup_size", opd_s, opd_e)) {
		opds[*opds_n] = 36;
		++(*opds_n);
	} else if (str_slice_cmp("subgroup_max_size", opd_s, opd_e)) {
		opds[*opds_n] = 37;
		++(*opds_n);
	} else if (str_slice_cmp("subgroups_n", opd_s, opd_e)) {
		opds[*opds_n] = 38;
		++(*opds_n);
	} else if (str_slice_cmp("enqueued_subgroups_n", opd_s, opd_e)) {
		opds[*opds_n] = 39;
		++(*opds_n);
	} else if (str_slice_cmp("subgroup_id", opd_s, opd_e)) {
		opds[*opds_n] = 40;
		++(*opds_n);
	} else if (str_slice_cmp("subgroup_local_invocation_id", opd_s, opd_e)) {
		opds[*opds_n] = 41;
		++(*opds_n);
	} else if (str_slice_cmp("subgroup_local_invocation_id", opd_s, opd_e)) {
		opds[*opds_n] = 41;
		++(*opds_n);
	} else if (str_slice_cmp("vertex_index", opd_s, opd_e)) {
		opds[*opds_n] = 42;
		++(*opds_n);
	} else if (str_slice_cmp("instance_index", opd_s, opd_e)) {
		opds[*opds_n] = 43;
		++(*opds_n);
	/* TODO: more */
	} else {
		err("ERROR:%u:%s:unknown builtin decoration \"%.*s\"\n", line_num, op, opd_e - opd_s, opd_s);
		exit(1);
	}
}

/* will consume the current instruction extra opds */
static void decoration_opds_consume(u8 * op, u32 decoration, u32 *opds,
								u32 *opds_n)
{
	switch (decoration) {
	case 11:
		decoration_builtin_opds_consume(op, opds, opds_n);
		break;
	case 30:
		decoration_location_opds_consume(op, opds, opds_n);
		break;
	}
}

static u32 opd_decoration(u8 *op)
{
	u8 *opd_s;
	u8 *opd_e;
			
	opd_val_reach(op, "decoration");

	opd_s = b;
	ws_or_eol_reach();
	opd_e = b;
			
	if (str_slice_cmp("relaxed_precision", opd_s, opd_e))
		return 0;
	else if (str_slice_cmp("scep_id", opd_s, opd_e))
		return 1;
	else if (str_slice_cmp("block", opd_s, opd_e))
		return 2;
	else if (str_slice_cmp("buffer_block", opd_s, opd_e))
		return 3;
	else if (str_slice_cmp("row_major", opd_s, opd_e))
		return 4;
	else if (str_slice_cmp("col_major", opd_s, opd_e))
		return 5;
	else if (str_slice_cmp("array_stride", opd_s, opd_e))
		return 6;
	else if (str_slice_cmp("matrix_stride", opd_s, opd_e))
		return 7;
	else if (str_slice_cmp("glsl_shared", opd_s, opd_e))
		return 8;
	else if (str_slice_cmp("glsl_packed", opd_s, opd_e))
		return 9;
	else if (str_slice_cmp("c_packed", opd_s, opd_e))
		return 10;
	else if (str_slice_cmp("builtin", opd_s, opd_e))
		return 11;
	else if (str_slice_cmp("location", opd_s, opd_e))
		return 30;
	/* TODO: more */

	err("ERROR:%u:%s:decoration operand:unknown value \"%.*s\"\n", line_num, op, opd_e - opd_s, opd_s);
	exit(1);
}

static void op_decorate(void)
{
	u8 r;
	bool have_target;
	u32 target;
	bool have_decoration;
	u32 decoration;
	u8 *err_str;

	u32 decoration_opds[VAR_OPDS_N_MAX];
	u32 decoration_opds_n;

	u32 word;

	have_target = false;
	have_decoration = false;
	decoration_opds_n = 0;

	loop {
		u8 *opd_s;
		u8 *opd_e;

		r = ws_skip();
		if (r == EOL) {
			err("ERROR:%u:decorate:missing operands\n", line_num);
			exit(1);
		}

		opd_s = b;
		ws_or_eol_or_eq_reach();
		opd_e = b;
	
		if (str_slice_cmp("target", opd_s, opd_e)) {
			target = opd_id("decorate", "target", "target id");
			have_target = true;
		} else if (str_slice_cmp("decoration", opd_s, opd_e)) {
			decoration = opd_decoration("decorate");
			have_decoration = true;
		} else {
			err("ERROR:%u:decorate:unknown operand \"%.*s\"\n", line_num, opd_e - opd_s, opd_s);
			exit(1);
		}

		if (have_target && have_decoration)
			break;
	}

	decoration_opds_consume("decorate", decoration, decoration_opds,
							&decoration_opds_n);
	r = ws_skip();
	if (r != EOL) {
		err("ERROR:%u:decorate:too many decoration specific operands\n",  line_num);
		exit(1);
	}

	word = 71;
	word |= (3 + decoration_opds_n) << 16;
	emit(word);

	emit(target);
	emit(decoration);

	word = 0;
	loop {
		if (word == decoration_opds_n)
			break;
		emit(decoration_opds[word]);
		++word;
	}
}

static void op_constant(void)
{
	u8 r;
	bool have_type;
	u32 type;
	bool have_id;
	u32 id;
	u8 *err_str;

	u32 vals[VAR_OPDS_N_MAX];
	u32 vals_n;

	u32 word;

	have_type = false;
	have_id = false;
	vals_n = 0;

	loop {
		u8 *opd_s;
		u8 *opd_e;

		r = ws_skip();
		if (r == EOL) {
			err("ERROR:%u:constant:missing operands\n", line_num);
			exit(1);
		}

		opd_s = b;
		ws_or_eol_or_eq_reach();
		opd_e = b;
	
		if (str_slice_cmp("type", opd_s, opd_e)) {
			type = opd_id("constant", "type", "type id");
			have_type = true;
		} else if (str_slice_cmp("id", opd_s, opd_e)) {
			id = opd_id("type", "id", "id");

			r = id_use(id, &err_str);
			if (r != OK) {
				err("ERROR:%u:constant:something is wrong with the id:%s\n", line_num, err_str);
				exit(1);
			}
			have_id = true;
		} else {
			err("ERROR:%u:constant:unknown operand \"%.*s\"\n", line_num, opd_e - opd_s, opd_s);
			exit(1);
		}

		if (have_type && have_id)
			break;
	}

	r = ws_skip();
	if (r != OK) {
		err("ERROR:%u:constant:missing value(s)\n",  line_num);
		exit(1);
	}

	r = u32s_collect(vals, &vals_n, &err_str);
	if (r != OK) {
		err("ERROR:%u:constant:unable to convert value(s):%s\n",  line_num, err_str);
		exit(1);
	}

	word = 43;
	word |= (3 + vals_n) << 16;
	emit(word);

	emit(type);
	emit(id);

	word = 0;
	loop {
		if (word == vals_n)
			break;
		emit(vals[word]);
		++word;
	}
}

static void op_constant_composite(void)
{
	u8 r;
	bool have_type;
	u32 type;
	bool have_id;
	u32 id;
	u8 *err_str;

	/* constituents (ids) */
	u32 cs[VAR_OPDS_N_MAX];
	u32 cs_n;

	u32 word;

	have_type = false;
	have_id = false;
	cs_n = 0;

	loop {
		u8 *opd_s;
		u8 *opd_e;

		r = ws_skip();
		if (r == EOL) {
			err("ERROR:%u:constant_composite:missing operands\n", line_num);
			exit(1);
		}

		opd_s = b;
		ws_or_eol_or_eq_reach();
		opd_e = b;
	
		if (str_slice_cmp("type", opd_s, opd_e)) {
			type = opd_id("constant_composite", "type", "type id");
			have_type = true;
		} else if (str_slice_cmp("id", opd_s, opd_e)) {
			id = opd_id("type", "id", "id");

			r = id_use(id, &err_str);
			if (r != OK) {
				err("ERROR:%u:constant_composite:something is wrong with the id:%s\n", line_num, err_str);
				exit(1);
			}
			have_id = true;
		} else {
			err("ERROR:%u:constant_composite:unknown operand \"%.*s\"\n", line_num, opd_e - opd_s, opd_s);
			exit(1);
		}

		if (have_type && have_id)
			break;
	}

	r = ws_skip();
	if (r != OK) {
		err("ERROR:%u:constant_composite:missing constituent(s)\n",  line_num);
		exit(1);
	}

	r = ids_collect(cs, &cs_n, &err_str);
	if (r != OK) {
		err("ERROR:%u:constant_composite:unable to get constituent(s):%s\n",  line_num, err_str);
		exit(1);
	}

	word = 44;
	word |= (3 + cs_n) << 16;
	emit(word);

	emit(type);
	emit(id);

	word = 0;
	loop {
		if (word ==cs_n)
			break;
		emit(cs[word]);
		++word;
	}
}

/* flag0|flag1|flag2... */
static u32 opd_function_control(u8 *op)
{
	u8 *opd_s;
	u8 *opd_e;
	u32 function_control;
			
	opd_val_reach(op, "control"); /* meh */

	function_control = 0;
	loop {
		opd_s = b;
		ws_or_eol_or_bar_reach();
		opd_e = b;
		
		if (str_slice_cmp("none", opd_s, opd_e)) {
			function_control = 0;
			ws_or_eol_reach(); /* consume it all */
			break;
		} else if (str_slice_cmp("inline", opd_s, opd_e))
			function_control |= 0x1;
		else if (str_slice_cmp("dont_inline", opd_s, opd_e))
			function_control |= 0x2;
		else if (str_slice_cmp("pure", opd_s, opd_e))
			function_control |= 0x4;
		else if (str_slice_cmp("const", opd_s, opd_e))
			function_control |= 0x8;
		else {
			err("ERROR:%u:%s:function control operand:unknown flag \"%.*s\"\n", line_num, op, opd_e - opd_s, opd_s);
			exit(1);
		}

		if (b == eol || *b != '|')
			break;

		++b;
	}
	return function_control;
}

static void op_function(void)
{
	u8 r;
	bool have_return_type;
	u32 return_type;
	bool have_type;
	u32 type;
	bool have_function_control;
	u32 function_control;
	bool have_id;
	u32 id;
	u8 *err_str;

	u32 word;

	have_return_type = false;
	have_type = false;
	have_id = false;
	have_function_control = false;

	loop {
		u8 *opd_s;
		u8 *opd_e;

		r = ws_skip();
		if (r == EOL) {
			err("ERROR:%u:function:missing operands\n", line_num);
			exit(1);
		}

		opd_s = b;
		ws_or_eol_or_eq_reach();
		opd_e = b;
	
		if (str_slice_cmp("type", opd_s, opd_e)) {
			type = opd_id("function", "type", "type id");
			have_type = true;
		} else if (str_slice_cmp("return_type", opd_s, opd_e)) {
			return_type = opd_id("function", "return_type",
							"return type id");
			have_return_type = true;
		} else if (str_slice_cmp("control", opd_s, opd_e)) {
			function_control = opd_function_control("function");
			have_function_control= true;
		} else if (str_slice_cmp("id", opd_s, opd_e)) {
			id = opd_id("type", "id", "id");

			r = id_use(id, &err_str);
			if (r != OK) {
				err("ERROR:%u:function:something is wrong with the id:%s\n", line_num, err_str);
				exit(1);
			}
			have_id = true;
		} else {
			err("ERROR:%u:function:unknown operand \"%.*s\"\n", line_num, opd_e - opd_s, opd_s);
			exit(1);
		}

		if (have_type && have_id)
			break;
	}

	word = 54;
	word |= 5 << 16;
	emit(word);

	emit(return_type);
	emit(id);
	emit(function_control);
	emit(type);
}

static void op_return(void)
{
	u32 op;

	op = 253;
	op |= (1 << 16);
	emit(op);
}

static void op_function_end(void)
{
	u32 op;

	op = 56;
	op |= (1 << 16);
	emit(op);
}

static void op_label(void)
{
	u8 r;
	u8 *opd_s;
	u8 *opd_e;
	u32 id;
	u8 *err_str;
	u32 op;

	r = ws_skip();
	if (r == EOL) {
		err("ERROR:%u:label:missing identifier\n", line_num);
		exit(1);
	}

	opd_s = b;
	ws_or_eol_or_eq_reach();
	opd_e = b;

	if (str_slice_cmp("id", opd_s, opd_e)) {
		id = opd_id("label", "id", "label id");
	} else {
		err("ERROR:%u:label:unknown operand \"%.*s\"\n", line_num, opd_e - opd_s, opd_s);
		exit(1);
	}
	
	r = id_use(id, &err_str);
	if (r != OK) {
		err("ERROR:%u:label:something is wrong with the identifier:%s\n", line_num, err_str);
		exit(1);
	}

	op = 248;
	op |= (2 << 16);
	emit(op);
	emit(id);
}

/* XXX: may factor it with op_binop_x */
static void op_iequal(void)
{
	u8 r;
	bool have_type;
	u32 type;
	bool have_id;
	u32 id;
	bool have_opd0;
	u32 opd0;
	bool have_opd1;
	u32 opd1;
	u8 *err_str;

	u32 word;

	have_type = false;
	have_id = false;
	loop {
		u8 *opd_s;
		u8 *opd_e;

		r = ws_skip();
		if (r == EOL) {
			err("ERROR:%u:iequal:missing operands\n", line_num);
			exit(1);
		}

		opd_s = b;
		ws_or_eol_or_eq_reach();
		opd_e = b;
	
		if (str_slice_cmp("type", opd_s, opd_e)) {
			type = opd_id("iequal", "type", "result type id");
			have_type = true;
		} else if (str_slice_cmp("id", opd_s, opd_e)) {
			id = opd_id("type", "id", "id");

			r = id_use(id, &err_str);
			if (r != OK) {
				err("ERROR:%u:iequal:something is wrong with the id:%s\n", line_num, err_str);
				exit(1);
			}
			have_id = true;
		} else {
			err("ERROR:%u:iequal:unknown operand \"%.*s\"\n", line_num, opd_e - opd_s, opd_s);
			exit(1);
		}

		if (have_type && have_id)
			break;
	}

	/* now the operation operands */
	have_opd0 = false;
	have_opd1 = false;
	loop {
		r = ws_skip();
		if (r == EOL) {
			err("ERROR:%u:iequal:missing comparison operands\n", line_num);
			exit(1);
		}

		if (!have_opd0) {
			opd0 = opd_id("iequal", 0, "comparison first operand");
			have_opd0 = true;
		} else if (!have_opd1) {
			opd1 = opd_id("iequal", 0, "comparison second operand");
			have_opd1 = true;
		}

		if (have_opd0 && have_opd1)
			break;
	}

	word = 170;
	word |= 5 << 16;
	emit(word);

	emit(type);
	emit(id);
	emit(opd0);
	emit(opd1);
}

static u32 opd_selection_control(u8 *op)
{
	u8 *opd_s;
	u8 *opd_e;
			
	opd_val_reach(op, "selection_control");

	opd_s = b;
	ws_or_eol_reach();
	opd_e = b;
			
	if (str_slice_cmp("none", opd_s, opd_e))
		return 0;
	else if (str_slice_cmp("flatten", opd_s, opd_e))
		return 1;
	else if (str_slice_cmp("dont_flatten", opd_s, opd_e))
		return 2;
	err("ERROR:%u:%s:selection_control operand:unknown value \"%.*s\"\n", line_num, op, opd_e - opd_s, opd_s);
	exit(1);
}

static void op_selection_merge(void)
{
	u8 r;
	bool have_block;
	u32 block;
	bool have_selection_control;
	u32 selection_control;
	u8 *err_str;

	u32 word;

	have_block = false;
	have_selection_control = false;
	loop {
		u8 *opd_s;
		u8 *opd_e;

		r = ws_skip();
		if (r == EOL) {
			err("ERROR:%u:selection_merge:missing operands\n", line_num);
			exit(1);
		}

		opd_s = b;
		ws_or_eol_or_eq_reach();
		opd_e = b;
	
		if (str_slice_cmp("block", opd_s, opd_e)) {
			block = opd_id("selection_merge", "block", "block label id");
			have_block = true;
		} else if (str_slice_cmp("selection_control", opd_s, opd_e)) {
			selection_control = opd_selection_control("selection_merge");
			have_selection_control = true;
		} else {
			err("ERROR:%u:selection_merge:unknown operand \"%.*s\"\n", line_num, opd_e - opd_s, opd_s);
			exit(1);
		}

		if (have_block && have_selection_control)
			break;
	}

	word = 247;
	word |= 3 << 16;
	emit(word);

	emit(block);
	emit(selection_control);
}

/* expect b to point on the first valid char */
static u8 targets_collect(u32 *opds, u32 *opds_n, u8 **err_str)
{
	static u8 err_str_buf[BUFSIZ];

	*err_str = err_str_buf;
	loop {
		u8 r;
		u8 *u32_s;
		u8 *u32_e;
		u8 *id_s;
		u8 *id_e;
		u8 *err_str_next;

		if ((*opds_n + 2) > VAR_OPDS_N_MAX) {
			snprintf(err_str_buf, BUFSIZ, "target[%u]:no room for target in operand storage", *opds_n / 2);
			return ERR;
		}

		u32_s = b;
		ws_or_eol_reach();
		u32_e = b;

		r = u32_decode(opds + *opds_n, u32_s, u32_e, &err_str_next);
		if (r != OK) {
			snprintf(err_str_buf, BUFSIZ, "target[%u]:literal number:%s", *opds_n / 2, err_str_next);
			return r;
		}

		r = ws_skip();
		if (r == EOL) {
			snprintf(err_str_buf, BUFSIZ, "target[%u]:missing label id", *opds_n / 2);
			return ERR;
		}

		id_s = b;
		ws_or_eol_reach();
		id_e = b;

		r = id_get(opds + *opds_n + 1, id_s, id_e, &err_str_next);
		if (r != OK) {
			snprintf(err_str_buf, BUFSIZ, "target[%u]:label id:%s", *opds_n / 2, err_str_next);
			return r;
		}

		(*opds_n) += 2;

		r = ws_skip();
		if (r == EOL)
			return OK;
	}
}

static void op_switch(void)
{
	u8 r;
	bool have_selector;
	u32 selector;
	bool have_default_id;
	u32 default_id;
	u8 *err_str;

	u32 targets_opds[VAR_OPDS_N_MAX];
	u32 targets_opds_n;

	u32 word;

	have_selector = false;
	have_default_id = false;
	targets_opds_n = 0;
	loop {
		u8 *opd_s;
		u8 *opd_e;

		r = ws_skip();
		if (r == EOL) {
			err("ERROR:%u:switch:missing operands\n", line_num);
			exit(1);
		}

		opd_s = b;
		ws_or_eol_or_eq_reach();
		opd_e = b;
	
		if (str_slice_cmp("selector", opd_s, opd_e)) {
			selector = opd_id("switch", "selector", "selector id");
			have_selector = true;
		} else if (str_slice_cmp("default", opd_s, opd_e)) {
			default_id = opd_id("switch", "default", "default id");
			have_default_id = true;
		} else {
			err("ERROR:%u:switch:unknown operand \"%.*s\"\n", line_num, opd_e - opd_s, opd_s);
			exit(1);
		}

		if (have_selector && have_default_id)
			break;
	}

	r = ws_skip();
	if (r == OK) {
		/* have targets */
		targets_collect(targets_opds, &targets_opds_n, &err_str);
		if (r != OK) {
			err("ERROR:%u:switch:collecting targets:%s\n",  line_num, err_str);
			exit(1);
		}
	}

	word = 251;
	word |= (3 + targets_opds_n) << 16;
	emit(word);

	emit(selector);
	emit(default_id);

	word = 0;
	loop {
		if (word == targets_opds_n)
			break;
		emit(targets_opds[word]);
		++word;
	}
}

static void memory_opds_alignment_get(u8 *op, u32 *alignment)
{
	u8 r;
	u8 *s;
	u8 *e;
	u8 *err_str;

	r = ws_skip();
	if (r != OK) {
		err("ERROR:%u:%s:memory operands:missing alignment literal value\n", line_num, op);
		exit(1);
	}

	s = b;
	ws_or_eol_reach();
	e = b;

	r = u32_decode(alignment, s, e, &err_str);
	if (r != OK) {
		err("ERROR:%u:%s:memory operands:unable to decode alignment literal value:%s\n", line_num, op, err_str);
		exit(1);
	}
	
}

static void memory_opds_x_scope_get(u8 *op, u8 *opd, u32 *scope)
{
	u8 r;
	u8 *s;
	u8 *e;

	r = ws_skip();
	if (r != OK) {
		err("ERROR:%u:%s:memory operands:missing %s scope\n", line_num, op, opd);
		exit(1);
	}

	s = b;
	ws_or_eol_reach();
	e = b;

	/* XXX: will probably be factor out */
	if (str_slice_cmp("cross_device", s, e))
		*scope = 0;
	else if (str_slice_cmp("device", s, e))
		*scope = 1;
	else if (str_slice_cmp("workgroup", s, e))
		*scope = 2;
	else if (str_slice_cmp("subgroup", s, e))
		*scope = 3;
	else if (str_slice_cmp("invocation", s, e))
		*scope = 4;
	else if (str_slice_cmp("queue_family", s, e))
		*scope = 5;
	else {
		err("ERROR:%u:%s:memory operands:unable to decode %s scope\n", line_num, op, opd);
		exit(1);
	}
}

/* flag0|flag1|flag2... opds_flag_X opds_flag_Y... with X < Y*/
static void memory_opds_collect(u8 *op, u32 *memory_opds, u32 *memory_opds_n)
{
	u8 *opd_s;
	u8 *opd_e;
	u32 additional_opd_idx;

	memory_opds[0] = 0x0;

	loop {
		opd_s = b;
		ws_or_eol_or_bar_reach();
		opd_e = b;
		
		if (str_slice_cmp("none", opd_s, opd_e)) {
			memory_opds[0] = 0x0;
			ws_or_eol_reach(); /* consume it all, useless */
			break;
		} else if (str_slice_cmp("volatile", opd_s, opd_e))
			memory_opds[0] |= 0x1;
		else if (str_slice_cmp("aligned", opd_s, opd_e))
			memory_opds[0] |= 0x2;
		else if (str_slice_cmp("non_temporal", opd_s, opd_e))
			memory_opds[0] |= 0x4;
		else if (str_slice_cmp("make_pointer_available", opd_s, opd_e))
			memory_opds[0] |= 0x8;
		else if (str_slice_cmp("make_pointer_visible", opd_s, opd_e))
			memory_opds[0] |= 0x10;
		else if (str_slice_cmp("non_private_pointer", opd_s, opd_e))
			memory_opds[0] |= 0x20;
		else {
			err("ERROR:%u:%s:memory operands:unknown operand \"%.*s\"\n", line_num, op, opd_e - opd_s, opd_s);
			exit(1);
		}
		if (b == eol || *b != '|')
			break;
		++b;
	}

	*memory_opds_n = 1;
	additional_opd_idx = 1; /* skip the flags word */

	if ((memory_opds[0] & 0x2) != 0) {
		memory_opds_alignment_get(op, memory_opds + additional_opd_idx);
		++(*memory_opds_n);
		++additional_opd_idx;
	}
	if ((memory_opds[0] & 0x8) != 0) {
		memory_opds_x_scope_get(op, "make_pointer_available",
					memory_opds +  additional_opd_idx);
		++(*memory_opds_n);
		++additional_opd_idx;
	}
	if ((memory_opds[0] & 0x10) != 0) {
		memory_opds_x_scope_get(op, "make_pointer_visible",
					memory_opds + additional_opd_idx);
		++(*memory_opds_n);
		++additional_opd_idx;
	}
}

static void op_store(void)
{
	u8 r;
	bool have_pointer;
	u32 pointer;
	bool have_object;
	u32 object;
	u8 *err_str;

	u32 memory_opds[VAR_OPDS_N_MAX];
	u32 memory_opds_n;

	u32 word;

	have_pointer = false;
	have_object = false;
	memory_opds_n = 0;

	loop {
		u8 *opd_s;
		u8 *opd_e;

		r = ws_skip();
		if (r == EOL) {
			err("ERROR:%u:type_function:missing operands\n", line_num);
			exit(1);
		}

		opd_s = b;
		ws_or_eol_or_eq_reach();
		opd_e = b;
	
		if (str_slice_cmp("pointer", opd_s, opd_e)) {
			pointer = opd_id("store", "pointer", "pointer id");
			have_pointer = true;
		} else if (str_slice_cmp("object", opd_s, opd_e)) {
			object = opd_id("store", "object", "object id");
			have_object = true;
		} else {
			err("ERROR:%u:store:unknown operand \"%.*s\"\n", line_num, opd_e - opd_s, opd_s);
			exit(1);
		}

		if (have_pointer && have_object)
			break;
	}

	r = ws_skip();
	if (r == OK)  /* have memory opd(s) */
		memory_opds_collect("store", memory_opds, &memory_opds_n);

	word = 62;
	word |= (3 + memory_opds_n) << 16;
	emit(word);

	emit(pointer);
	emit(object);

	word = 0;
	loop {
		if (word == memory_opds_n)
			break;
		emit(memory_opds[word]);
		++word;
	}
}

static void op_load(void)
{
	u8 r;
	bool have_type;
	u32 type;
	bool have_id;
	u32 id;
	bool have_pointer;
	u32 pointer;
	u8 *err_str;

	u32 memory_opds[VAR_OPDS_N_MAX];
	u32 memory_opds_n;

	u32 word;

	have_type = false;
	have_pointer = false;
	have_id = false;
	memory_opds_n = 0;

	loop {
		u8 *opd_s;
		u8 *opd_e;

		r = ws_skip();
		if (r == EOL) {
			err("ERROR:%u:type_function:missing operands\n", line_num);
			exit(1);
		}

		opd_s = b;
		ws_or_eol_or_eq_reach();
		opd_e = b;
	
		if (str_slice_cmp("type", opd_s, opd_e)) {
			type = opd_id("load", "type", "type id");
			have_type= true;
		} else if (str_slice_cmp("id", opd_s, opd_e)) {
			id = opd_id("load", "id", "id");

			r = id_use(id, &err_str);
			if (r != OK) {
				err("ERROR:%u:load:something is wrong with the id:%s\n", line_num, err_str);
				exit(1);
			}
			have_id = true;
		} else if (str_slice_cmp("pointer", opd_s, opd_e)) {
			pointer = opd_id("load", "pointer", "pointer id");
			have_pointer = true;
		} else {
			err("ERROR:%u:load:unknown operand \"%.*s\"\n", line_num, opd_e - opd_s, opd_s);
			exit(1);
		}

		if (have_type && have_id && have_pointer)
			break;
	}

	r = ws_skip();
	if (r == OK)  /* have memory opd(s) */
		memory_opds_collect("load", memory_opds, &memory_opds_n);

	word = 61;
	word |= (4 + memory_opds_n) << 16;
	emit(word);

	emit(type);
	emit(id);
	emit(pointer);

	word = 0;
	loop {
		if (word == memory_opds_n)
			break;
		emit(memory_opds[word]);
		++word;
	}
}

static void op_dispatch(void)
{
	u8 *op_s;
	u8 *op_e;

	op_s = b;
	ws_or_eol_reach();
	op_e = b;

	if      (str_slice_cmp("capability", op_s, op_e))
			op_capability();
	else if (str_slice_cmp("memory_model", op_s, op_e))
			op_memory_model();
	else if (str_slice_cmp("entry_point", op_s, op_e))
			op_entry_point();
	else if (str_slice_cmp("type_void", op_s, op_e))
			op_type_x("void", 19);
	else if (str_slice_cmp("type_bool", op_s, op_e))
			op_type_x("bool", 20);
	else if (str_slice_cmp("type_function", op_s, op_e))
			op_type_function();
	else if (str_slice_cmp("type_float", op_s, op_e))
			op_type_float();
	else if (str_slice_cmp("type_vector", op_s, op_e))
			op_type_vector();
	else if (str_slice_cmp("type_int", op_s, op_e))
			op_type_int();
	else if (str_slice_cmp("type_pointer", op_s, op_e))
			op_type_pointer();
	else if (str_slice_cmp("variable", op_s, op_e))
			op_variable();
	else if (str_slice_cmp("decorate", op_s, op_e))
			op_decorate();
	else if (str_slice_cmp("constant", op_s, op_e))
			op_constant();
	else if (str_slice_cmp("constant_composite", op_s, op_e))
			op_constant_composite();
	else if (str_slice_cmp("function", op_s, op_e))
			op_function();
	else if (str_slice_cmp("return", op_s, op_e))
			op_return();
	else if (str_slice_cmp("function_end", op_s, op_e))
			op_function_end();
	else if (str_slice_cmp("label", op_s, op_e))
			op_label();
	else if (str_slice_cmp("iequal", op_s, op_e))
			op_iequal();
	else if (str_slice_cmp("selection_merge", op_s, op_e))
			op_selection_merge();
	else if (str_slice_cmp("switch", op_s, op_e))
			op_switch();
	else if (str_slice_cmp("store", op_s, op_e))
			op_store();
	else if (str_slice_cmp("load", op_s, op_e))
			op_load();
	/* TODO: MORE */
	else {
		err("ERROR:%u:unknown instruction \"%.*s\"\n", line_num, op_e - op_s, op_s);
		exit(1);
	}
}

static void line_process(void)
{
	u8 r;

	b = line;

	if (*b == '#') /* C preprocessor line, skip line */
		return;

	r = ws_skip();
	if (r == EOL)
		return;

	op_dispatch();
}

int main(void)
{
	clearerr(stdin);
	binary = 0;
	line_num = 1;
	memset(ids, 0, sizeof(ids));

	loop {
		u8 r;

		r = read_line();
		if (r == ERR) {
			err("ERROR:%u:an error occured while reading the line\n", line_num);
			exit(1);
		}

		line_process();

		if (r == LAST_LINE)
			break;

		++line_num;
	}

	out_hdr();
	fwrite(binary, sizeof(*binary), binary_words_n, stdout); 
	exit(0);
}
