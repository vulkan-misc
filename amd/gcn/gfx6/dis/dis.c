/* amd gpu gfx6 */
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
/*
 * ABBREVIATIONS:
 *
 * opd(s) : OPeranD)S)
 * s = Scalar
 * src : SouRCe
 * v(s) : Vector(S)
 * w(s) : Word(S)
 */
/*
 * we could not care less about memory management
 */ 
#define u8 uint8_t
#define u16 uint16_t
#define u32 uint32_t
#define loop for(;;)
#define out(fmt,...) fprintf(stdout,fmt, ##__VA_ARGS__)
#define STR_SZ_MAX 256
#define pf(fmt,...) snprintf(str, STR_SZ_MAX, fmt, ##__VA_ARGS__)
#define CASE_STR(a,b) \
	case a:\
		strcpy(str, b);\
		break

static u32 w;

#define OK 0
#define END 1
#define ERR 2
static u8 read_w(u32 *w)
{
	size_t read_bytes_n;
	read_bytes_n = fread(w, sizeof(*w), 1, stdin);
	if (read_bytes_n != sizeof(*w))	{
		if (feof(stdin) != 0)
			return END;
		if (ferror(stdin) != 0)
			return ERR;
	}
}

/* op must be in range */
static u8 *v_op_blk_0_to_str(u8 op)
{
	static u8 str[STR_SZ_MAX];

	switch (op) {
	CASE_STR( 0, "v_cndmask_b32");
	CASE_STR( 1, "v_readlane_b32");
	CASE_STR( 2, "v_writelane_b32");
	CASE_STR( 3, "v_add_f32");
	CASE_STR( 4, "v_sub_f32");
	CASE_STR( 5, "v_subrev_f32");
	CASE_STR( 6, "v_mac_legacy_f32");
	CASE_STR( 7, "v_mul_legacy_f32");
	CASE_STR( 8, "v_mul_f32");
	CASE_STR( 9, "v_mul_i32_i24");
	CASE_STR(10, "v_mul_hi_i32_i24");
	CASE_STR(11, "v_mul_u32_u24");
	CASE_STR(12, "v_mul_hi_u32_u24");
	CASE_STR(13, "v_min_legacy_f32");
	CASE_STR(14, "v_max_legacy_f32");
	CASE_STR(15, "v_min_f32");
	CASE_STR(16, "v_max_f32");
	CASE_STR(17, "v_min_i32");
	CASE_STR(18, "v_max_i32");
	CASE_STR(19, "v_min_u32");
	CASE_STR(20, "v_max_u32");
	CASE_STR(21, "v_lshr_b32");
	CASE_STR(22, "v_lshrrev_b32");
	CASE_STR(23, "v_ashr_i32");
	CASE_STR(24, "v_ashrrev_i32");
	CASE_STR(25, "v_lshl_b32");
	CASE_STR(26, "v_lshlrev_b32");
	CASE_STR(27, "v_and_b32");
	CASE_STR(28, "v_or_b32");
	CASE_STR(29, "v_xor_b32");
	CASE_STR(30, "v_bfm_b32");
	CASE_STR(31, "v_mac_b32");
	CASE_STR(32, "v_madmk_f32");
	CASE_STR(33, "v_madak_f32");
	CASE_STR(34, "v_bcnt_u32_b32");
	CASE_STR(35, "v_mbcnt_lo_u32_b32");
	CASE_STR(36, "v_mbcnt_hi_u32_b32");
	default:
		out("ERROR:vector opcode blk 0 unknown code %u\n", op);
		exit(1);
		break;
	}
}

/* op must be in range */
static u8 *v_op_blk_1_to_str(u8 op)
{
	static u8 str[STR_SZ_MAX];

	switch (op) {
	CASE_STR(0, "v_add_i32" );
	CASE_STR(1, "v_sub_i32");
	CASE_STR(2, "v_subrev_i32");
	CASE_STR(3, "v_addc_u32");
	CASE_STR(4, "v_subb_u32");
	CASE_STR(5, "v_subbrev_u32");
	default:
		out("ERROR:vector opcode blk 1 unknown code %u\n", op);
		exit(1);
		break;
	}
}

/* op must be in range */
static u8 *v_op_blk_2_to_str(u8 op)
{
	static u8 str[STR_SZ_MAX];

	switch (op) {
	CASE_STR(0, "v_ldexp_f32");
	CASE_STR(1, "v_cvt_pkaccum_u8_f32");
	CASE_STR(2, "v_cvt_pknorm_i16_f32");
	CASE_STR(3, "v_cvt_pknorm_u16_f32");
	CASE_STR(4, "v_cvt_pkrtz_f16_f32");
	CASE_STR(5, "v_cvt_pk_u16_u32");
	CASE_STR(6, "v_cvt_pk_i16_i32");
	default:
		out("ERROR:vector opcode blk 2 unknown code %u\n", op);
		exit(1);
		break;
	}
}

static u8 *s_opd_to_str(u8 opd, bool *literal_was_read, u32 *literal)
{
	u8 r;
	u8 *str;

	str = malloc(STR_SZ_MAX);

	if (opd <= 103) {
		pf("sgpr%u", opd);
		goto exit;
	}
	if (112 <= opd && opd <= 123) {
		pf("ttmp%u", opd);
		goto exit;
	}
	if (128 <= opd && opd <= 192) {
		pf("%u", opd - 128);
		goto exit;
	}
	if (193 <= opd && opd <= 208) {
		pf("-%u", opd - 193 + 1);
		goto exit;
	}
	switch (opd) {	
	CASE_STR(106, "vcc_lo");
	CASE_STR(107, "vcc_hi");
	CASE_STR(108, "tba_lo");
	CASE_STR(109, "tba_hi");
	CASE_STR(110, "tma_lo");
	CASE_STR(111, "tma_hi");
	CASE_STR(124, "m0");
	CASE_STR(126, "exec_lo");
	CASE_STR(127, "exec_hi");
	CASE_STR(240, "0.5");
	CASE_STR(241, "-0.5");
	CASE_STR(242, "1.0");
	CASE_STR(243, "-1.0");
	CASE_STR(244, "2.0");
	CASE_STR(245, "-2.0");
	CASE_STR(246, "4.0");
	CASE_STR(247, "-4.0");
	CASE_STR(251, "vccz");
	CASE_STR(252, "execz");
	CASE_STR(253, "scc");
	case 255:
		if (!*literal_was_read) {
			r = read_w(literal);
			if (r != OK) {
				out("ERROR:missing literal\n");
				exit(1);
			}
			*literal_was_read = true;
		}
		pf("literal(=0x%08x)", *literal);
		break;
	default:
		pf("unknown_operand(=0x%02x)", opd);
		break;
	}
exit:
	return str;
}

static u8 *v_opd_to_str(u16 v_opd, bool *literal_was_read, u32 *literal)
{
	u8 *str;

	if (256 <= v_opd && v_opd <= 511) {
		str = malloc(STR_SZ_MAX);
		pf("vgpr%u", v_opd - 256);
		goto exit;
	}

	str = s_opd_to_str((u8)v_opd, literal_was_read, literal);
exit:
	return str;
}

static u8 *sop1_op_to_str(u8 op)
{
	static u8 str[STR_SZ_MAX];

	switch (op) {
	CASE_STR(  3, "s_mov_b32");
	CASE_STR(  4, "s_mov_b64");
	CASE_STR(  5, "s_cmov_b32");
	CASE_STR(  6, "s_cmov_b64");
	CASE_STR(  7, "s_not_b32");
	CASE_STR(  8, "s_not_b64");
	CASE_STR(  9, "s_wqm_b32");
	CASE_STR( 10, "s_wqm_b64");
	CASE_STR( 11, "s_brev_b32");
	CASE_STR( 12, "s_brev_b64");
	CASE_STR( 13, "s_bctn0_i32_b32");
	CASE_STR( 14, "s_bctn0_i32_b64");
	CASE_STR( 15, "s_bctn1_i32_b32");
	CASE_STR( 16, "s_bctn1_i32_b64");
	CASE_STR( 17, "s_ff0_i32_b32");
	CASE_STR( 18, "s_ff0_i32_b64");
	CASE_STR( 19, "s_ff1_i32_b32");
	CASE_STR( 20, "s_ff1_i32_b64");
	CASE_STR( 21, "s_flbit_i32_b32");
	CASE_STR( 22, "s_flbit_i32_b64");
	CASE_STR( 23, "s_flbit_i32");
	CASE_STR( 24, "s_flbit_i32_i64");
	CASE_STR( 25, "s_sext_i32_i8");
	CASE_STR( 26, "s_sext_i32_i16");
	CASE_STR( 27, "s_bitset0_b32");
	CASE_STR( 28, "s_bitset0_b64");
	CASE_STR( 29, "s_bitset1_b32");
	CASE_STR( 30, "s_bitset1_b64");
	CASE_STR( 31, "s_getpc_b64");
	CASE_STR( 32, "s_setpc_b64");
	CASE_STR( 33, "s_swappc_b64");
	CASE_STR( 34, "s_rfe_b64");
	CASE_STR( 36, "s_and_saveexec_b64");
	CASE_STR( 37, "s_or_saveexec_b64");
	CASE_STR( 38, "s_xor_saveexec_b64");
	CASE_STR( 39, "s_andn2_saveexec_b64");
	CASE_STR( 40, "s_orn2_saveexec_b64");
	CASE_STR( 41, "s_nand_saveexec_b64");
	CASE_STR( 42, "s_nor_saveexec_b64");
	CASE_STR( 43, "s_xnor_saveexec_b64");
	CASE_STR( 44, "s_quadmask_b32");
	CASE_STR( 45, "s_quadmask_b64");
	CASE_STR( 46, "s_movrels_b32");
	CASE_STR( 47, "s_movrels_b64");
	CASE_STR( 48, "s_movreld_b32");
	CASE_STR( 49, "s_movreld_b64");
	CASE_STR( 50, "s_cbranch_join");
	CASE_STR( 52, "s_abs_i32");
	CASE_STR( 53, "s_mov_fed_b32");
	default:
		pf("unknown_sop1_op(%u)", op);
		break;
	}
	return str;
}

static void sop1(void)
{
	u8 op;
	u8 *op_str;

	u32 literal;
	bool literal_was_read;

	u8 ssrc0;
	u8 *ssrc0_str;

	u8 sdst;
	u8 *sdst_str;

	literal_was_read = false;

	op = (u8)(w >> 8);
	ssrc0 = (u8)w; /* 8 bits */
	sdst = ((u8)(w >> 16)) & 0x7f; /* 7 bits */

	op_str = sop1_op_to_str(op);
	ssrc0_str = s_opd_to_str(ssrc0, &literal_was_read, &literal);
	sdst_str = s_opd_to_str(sdst, &literal_was_read, &literal);

	out("%s ssrc0=%s sdst=%s\n", op_str, ssrc0_str, sdst_str);
}

static u8 *sopc_op_to_str(u8 op)
{
	static u8 str[STR_SZ_MAX];

	switch (op) {
	CASE_STR( 0, "s_cmp_eq_i32");
	CASE_STR( 1, "s_cmp_lg_i32");
	CASE_STR( 2, "s_cmp_gt_i32");
	CASE_STR( 3, "s_cmp_ge_i32");
	CASE_STR( 4, "s_cmp_lt_i32");
	CASE_STR( 5, "s_cmp_le_i32");
	CASE_STR( 6, "s_cmp_eq_u32");
	CASE_STR( 7, "s_cmp_lg_u32");
	CASE_STR( 8, "s_cmp_gt_u32");
	CASE_STR( 9, "s_cmp_ge_u32");
	CASE_STR(10, "s_cmp_lt_u32");
	CASE_STR(11, "s_cmp_le_u32");
	CASE_STR(12, "s_bitcmp0_b32");
	CASE_STR(13, "s_bitcmp1_b32");
	CASE_STR(14, "s_bitcmp0_b64");
	CASE_STR(15, "s_bitcmp1_b64");
	CASE_STR(16, "s_setvskip");
	default:
		pf("unknown_sopc_op(%u)", op);
		break;
	}
	return str;
}

static void sopc(void)
{
	u8 op;
	u8 *op_str;

	u32 literal;
	bool literal_was_read;

	u8 ssrc0;
	u8 *ssrc0_str;

	u8 ssrc1;
	u8 *ssrc1_str;

	literal_was_read = false;

	op = ((u8)(w >> 16)) & 0x7f; /* 7 bits */
	ssrc0 = (u8)w; /* 8 bits */
	ssrc1 = (u8)(w >> 8); /* 8 bits */

	op_str = sopc_op_to_str(op);
	ssrc0_str = s_opd_to_str(ssrc0, &literal_was_read, &literal);
	ssrc1_str = s_opd_to_str(ssrc1, &literal_was_read, &literal);

	out("%s ssrc0=%s ssrc1=%s\n", op_str, ssrc0_str, ssrc1_str);
}

static u8 *vop1_op_to_str(u8 op)
{
	static u8 str[STR_SZ_MAX];

	switch (op) {
	CASE_STR(  0, "v_nop");
	CASE_STR(  1, "v_mov_b32");
	CASE_STR(  2, "v_readfirstlane_b32");
	CASE_STR(  3, "v_cvt_i32_f64");
	CASE_STR(  4, "v_cvt_f64_i32");
	CASE_STR(  5, "v_cvt_f32_i32");
	CASE_STR(  6, "v_cvt_f32_u32");
	CASE_STR(  7, "v_cvt_u32_f32");
	CASE_STR(  8, "v_cvt_i32_f32");
	CASE_STR(  9, "v_mov_fed_b32");
	CASE_STR( 10, "v_cvt_f16_f32");
	CASE_STR( 11, "v_cvt_f32_f16");
	CASE_STR( 12, "v_cvt_rpi_i32_f32");
	CASE_STR( 13, "v_cvt_flr_i32_f32");
	CASE_STR( 14, "v_cvt_off_f32_i4");
	CASE_STR( 15, "v_cvt_f32_f64");
	CASE_STR( 16, "v_cvt_f64_f32");
	CASE_STR( 17, "v_cvt_f32_ubyte0");
	CASE_STR( 18, "v_cvt_f32_ubyte1");
	CASE_STR( 19, "v_cvt_f32_ubyte2");
	CASE_STR( 20, "v_cvt_f32_ubyte3");
	CASE_STR( 21, "v_cvt_u32_f64");
	CASE_STR( 22, "v_cvt_f64_u32");
	CASE_STR( 32, "v_fract_f32");
	CASE_STR( 33, "v_trunc_f32");
	CASE_STR( 34, "v_ceil_f32");
	CASE_STR( 35, "v_rndne_f32");
	CASE_STR( 36, "v_floor_f32");
	CASE_STR( 37, "v_exp_f32");
	CASE_STR( 38, "v_log_clamp_f32");
	CASE_STR( 39, "v_log_f32");
	CASE_STR( 40, "v_rcp_clamp_f32");
	CASE_STR( 41, "v_rcp_legacy_f32");
	CASE_STR( 42, "v_rcp_f32");
	CASE_STR( 43, "v_rcp_iflag_f32");
	CASE_STR( 44, "v_rsq_clamp_f32");
	CASE_STR( 45, "v_rsq_legacy_f32");
	CASE_STR( 46, "v_rsq_f32");
	CASE_STR( 47, "v_rsp_f64");
	CASE_STR( 48, "v_rsp_clamp_f64");
	CASE_STR( 49, "v_rsq_f64");
	CASE_STR( 50, "v_rsq_clamp_f64");
	CASE_STR( 51, "v_sqrt_f32");
	CASE_STR( 52, "v_sqrt_f64");
	CASE_STR( 53, "v_sin_f64");
	CASE_STR( 54, "v_cos_f64");
	CASE_STR( 55, "v_not_b32");
	CASE_STR( 56, "v_bfrev_b32");
	CASE_STR( 57, "v_ffbh_u32");
	CASE_STR( 58, "v_ffbl_b32");
	CASE_STR( 59, "v_ffbh_i32");
	CASE_STR( 60, "v_frexp_exp_i32_f64");
	CASE_STR( 61, "v_frexp_mant_f64");
	CASE_STR( 62, "v_frac_f64");
	CASE_STR( 63, "v_frexp_exp_i32_f32");
	CASE_STR( 64, "v_frexp_mant_f32");
	CASE_STR( 65, "v_clrexcp");
	CASE_STR( 66, "v_movreld_b32");
	CASE_STR( 67, "v_movrels_b32");
	CASE_STR( 68, "v_movrelsd_b32");
	default:
		pf("unknown_vop1_op(%u)", op);
		break;
	}
	return str;
}

static void vop1(void)
{
	u8 op;
	u8 *op_str;

	u32 literal;
	bool literal_was_read;

	u16 src0;
	u8 *src0_str;

	u8 vdst;

	literal_was_read = false;

	op = (u8)(w >> 9); /* 8 bits */
	src0 = (u16)(w) & 0x1ff; /* 9 bits */
	vdst = (u8)(w >> 17); /* 8 bits */

	op_str = vop1_op_to_str(op);
	src0_str = v_opd_to_str(op, &literal_was_read, &literal);

	out("%s src0=%s vdst=vgpr%u\n", op_str, src0_str, vdst);
}

static u8 *cmp_op_16_to_str(u8 offset)
{
	static u8 str[STR_SZ_MAX];

	switch (offset) {
	case 0:
		strcpy(str, "f");
		break;
	case 1:
		strcpy(str,"lt");
		break;
	case 2:
		strcpy(str, "eq");
		break;
	case 3:
		strcpy(str, "le");
		break;
	case 4:
		strcpy(str, "gt");
		break;
	case 5:
		strcpy(str, "lg");
		break;
	case 6:
		strcpy(str, "ge");
		break;
	case 7:
		strcpy(str, "o");
		break;
	case 8:
		strcpy(str, "u");
		break;
	case 9:
		strcpy(str, "nge");
		break;
	case 10:
		strcpy(str, "nlg");
		break;
	case 11:
		strcpy(str, "ngt");
		break;
	case 12:
		strcpy(str, "nle");
		break;
	case 13:
		strcpy(str, "neq");
		break;
	case 14:
		strcpy(str, "nlt");
		break;
	case 15:
		strcpy(str, "tru");
		break;
	default:
		pf("unknown_cmp_op_16(%u)", offset);
		break;
	}
	return str;
}

static u8 *cmp_op_8_to_str(u8 offset)
{
	static u8 str[STR_SZ_MAX];

	switch (offset) {
	case 0:
		strcpy(str, "f");
		break;
	case 1:
		strcpy(str, "lt");
		break;
	case 2:
		strcpy(str, "eq");
		break;
	case 3:
		strcpy(str, "le");
		break;
	case 4:
		strcpy(str, "gt");
		break;
	case 5:
		strcpy(str, "lg");
		break;
	case 6:
		strcpy(str, "ge");
		break;
	case 7:
		strcpy(str, "tru");
		break;
	default:
		pf("unknown_cmp_op_8(%u)", offset);
		break;
	}
	return str;
}

static u8 *vopc_op_to_str(u8 op)
{
	static u8 str[STR_SZ_MAX];

	if (0x00 <= op && op <= 0x0f) {
		pf("v_cmp_%s_f32", cmp_op_16_to_str(op));
		goto exit;
	}

	if (0x10 <= op && op <= 0x1f) {
		pf("v_cmpx_%s_f32", cmp_op_16_to_str(op));
		goto exit;
	}

	if (0x20 <= op && op <= 0x2f) {
		pf("v_cmp_%s_f64", cmp_op_16_to_str(op));
		goto exit;
	}

	if (0x30 <= op && op <= 0x3f) {
		pf("v_cmpx_%s_f64", cmp_op_16_to_str(op));
		goto exit;
	}

	if (0x40 <= op && op <= 0x4f) {
		pf("v_cmps_%s_f32", cmp_op_16_to_str(op));
		goto exit;
	}

	if (0x50 <= op && op <= 0x5f) {
		pf("v_cmpsx_%s_f32", cmp_op_16_to_str(op));
		goto exit;
	}

	if (0x60 <= op && op <= 0x6f) {
		pf("v_cmps_%s_f64", cmp_op_16_to_str(op));
		goto exit;
	}

	if (0x70 <= op && op <= 0x7f) {
		pf("v_cmpsx_%s_f64", cmp_op_16_to_str(op));
		goto exit;
	}

	if (0x80 <= op && op <= 0x87) {
		pf("v_cmp_%s_i32", cmp_op_8_to_str(op));
		goto exit;
	}

	if (0x90 <= op && op <= 0x97) {
		pf("v_cmpx_%s_i32", cmp_op_8_to_str(op));
		goto exit;
	}

	if (0xa0 <= op && op <= 0xa7) {
		pf("v_cmp_%s_i64", cmp_op_8_to_str(op));
		goto exit;
	}

	if (0xb0 <= op && op <= 0xb7) {
		pf("v_cmpx_%s_i64", cmp_op_8_to_str(op));
		goto exit;
	}

	if (0xc0 <= op && op <= 0xc7) {
		pf("v_cmp_%s_u64", cmp_op_8_to_str(op));
		goto exit;
	}

	if (0xd0 <= op && op <= 0xd7) {
		pf("v_cmpx_%s_u32", cmp_op_8_to_str(op));
		goto exit;
	}

	if (0xe0 <= op && op <= 0xe7) {
		pf("v_cmp_%s_u64", cmp_op_8_to_str(op));
		goto exit;
	}

	if (0xf0 <= op && op <= 0xf7) {
		pf("v_cmpx_%s_u64", cmp_op_8_to_str(op));
		goto exit;
	}
	switch (op) {
	case 0x88:
		pf("v_cmp_class_f32");
		break;
	case 0x98:
		pf("v_cmpx_class_f32");
		break;
	case 0xa8:
		pf("v_cmp_class_f64");
		break;
	case 0xb8:
		pf("v_cmpx_class_f64");
		break;
	default:
		pf("unknown_vopc_op(%u)", op);
		break;
	}
exit:
	return str;
}

static void vopc(void)
{
	u8 op;
	u8 *op_str;

	u32 literal;
	bool literal_was_read;

	u16 src0;
	u8 *src0_str;

	u8 vsrc1;

	literal_was_read = false;

	op = (u8)(w >> 17); /* 8 bits */
	src0 = (u16)(w) & 0x1ff; /* 9 bits */
	vsrc1 = (u8)(w >> 9); /* 8 bits */

	op_str = vopc_op_to_str(op);
	src0_str = v_opd_to_str(op, &literal_was_read, &literal);

	out("%s src0=%s vsrc1=vgpr%u\n", op_str, src0_str, vsrc1);
}

static void export(void)
{
	u8 r;
	u32 w1;

	u8 en;
	u8 tgt;
	u8 compr;
	u8 done;
	u8 vm;

	u8 vsrc0;
	u8 vsrc1;
	u8 vsrc2;
	u8 vsrc3;

	r = read_w(&w1);
	if (r != OK) {
		out("ERROR:export:missing 2nd instruction word\n");
		exit(1);
	}

	en = ((u8)w) & 0x0f; /* 4 bits */
	tgt = (u8)(w >> 4) & 0x3f; /* 6 bits */
	compr = (u8)(w >> 10) & 1; /* 1 bit */
	done = (u8)(w >> 11) & 1; /* 1 bit */
	vm = (u8)(w >> 12) & 1; /* 1 bit */

	vsrc0 = (u8)(w1); /* 8 bits */
	vsrc1 = (u8)(w1 >> 8); /* 8 bits */
	vsrc2 = (u8)(w1 >> 16); /* 8 bits */
	vsrc3 = (u8)(w1 >> 24); /* 8 bits */

	out("export en=%u tgt=%u compr=%s done=%s vm=%s vsrc0=vgpr%u vsrc1=vgpr%u vsrc2=vgpr%u vsrc3=%u\n", en , tgt, (compr != 0) ? "true" : "false", (done != 0) ? "true" : "false", (vm != 0) ? "true" : "false", vsrc0, vsrc1, vsrc2, vsrc3);
}

static u8 *vintrp_op_to_str(u8 op)
{
	static u8 str[STR_SZ_MAX];

	switch (op) {
	CASE_STR(0, "vinterp_p1_f32" );
	CASE_STR(1, "vinterp_p2_f32" );
	CASE_STR(2, "vinterp_mov_f32" );
	default:
		pf("unknown_vinterp_op(%u)", op);
		break;
	}
	return str;
}

static void vintrp(void)
{
	u8 vsrc;
	u8 attrchan;
	u8 attr;
	u8 op;
	u8 *op_str;
	u8 vdst;

	vsrc = (u8)w; /* 8 bits */
	attrchan = (u8)(w >> 8) & 0x03; /* 2 bits */
	attr = (u8)(w >> 10) & 0x3f; /* 6 bits */
	op = (u8)(w >> 16) & 0x03; /* 2 bits */

	op_str = vintrp_op_to_str(op);

	out("%s vsrc=vgpr%u attrchan=%u attr=%u vdst=vgpr%u\n", op_str, vsrc, attrchan, attr, vdst);
}

static u8 *vop3b_op_to_str(u16 op)
{
	static u8 str[STR_SZ_MAX];

	if (293 <= op && op <= 298) {
		strcpy(str, v_op_blk_1_to_str((u8)(op - 293)));
		goto exit;
	}
	
	switch (op) {
	CASE_STR(365, "v_div_scale_f32");
	CASE_STR(366, "v_div_scale_f64");
	default:
		pf("unknown_vop3b_op(%u)", op);
		break;
	}
exit:
	return str;
}

static void vop3b(u16 op, u32 w1)
{
	u8 vdst;
	u8 sdst;
	u8 *sdst_str;
	u8 *op_str;
	u16 src0;
	u8 *src0_str;
	u16 src1;
	u8 *src1_str;
	u16 src2;
	u8 *src2_str;
	u8 omod;
	u8 neg;

	bool literal_was_read;
	u32 literal;

	literal_was_read = false;

	vdst = (u8)w; /* 8 bits */
	sdst = (u8)(w >> 8) & 0x7f; /* 7 bits */
	src0 = (u16)w1 & 0x1ff; /* 9 bits */
	/* documentation seems wrong on bit offsets, see vop3a */
	src1 = (u16)(w1 >> 9) & 0x1ff; /* 9 bits */
	src2 = (u16)(w1 >> 18) & 0x1ff; /* 9 bits */
	omod = (u8)(w1 >> 27) & 0x03; /* 2 bits */ 
	neg = (u8)(w1 >> 29) & 0x07; /* 3 bits */

	op_str = vop3b_op_to_str(op);
	sdst_str = v_opd_to_str((u16)sdst, &literal_was_read, &literal);
	src0_str = v_opd_to_str(src0, &literal_was_read, &literal);
	src1_str = v_opd_to_str(src1, &literal_was_read, &literal);
	src2_str = v_opd_to_str(src2, &literal_was_read, &literal);

	out("%s vdst=vgpr%u sdst=%s src0=%s src1=%s src2=%s omod=%u neg=%u\n", op_str, vdst, sdst_str, src0_str, src1_str, src2_str, omod, neg);
}

static u8 *vop3a_op_to_str(u16 op)
{
	static u8 str[STR_SZ_MAX];

	if (op <= 255) {
		strcpy(str, vopc_op_to_str((u8)op));
		goto exit;
	}
	if (256 <= op && op <= 292) {
		strcpy(str, v_op_blk_0_to_str((u8)(op - 256)));
		goto exit;
	}
	if ((293 <= op && op <= 298) || (365 <= op && op <= 366)) {
		strcpy(str, vop3b_op_to_str(op));
		goto exit;
	}
	if (299 <= op && op <= 305) {
		strcpy(str, v_op_blk_2_to_str((u8)(op - 299)));
		goto exit;
	}
	if (384 <= op && op <= 452) {
		strcpy(str, vop1_op_to_str((u8)(op - 384)));
		goto exit;
	}
	switch (op) {
	CASE_STR(320, "v_mad_legacy_f32");
	CASE_STR(321, "v_mad_f32");
	CASE_STR(322, "v_mad_i32_i24");
	CASE_STR(323, "v_mad_u32_u24");
	CASE_STR(324, "v_cubeid_f32");
	CASE_STR(325, "v_cubesc_f32");
	CASE_STR(326, "v_cubetc_f32");
	CASE_STR(327, "v_cubema_f32");
	CASE_STR(328, "v_bfe_i32");
	CASE_STR(329, "v_bfe_u32");
	CASE_STR(330, "v_bfi_b32");
	CASE_STR(331, "v_fma_f32");
	CASE_STR(332, "v_fma_f64");
	CASE_STR(333, "v_lerp_u8");
	CASE_STR(334, "v_alignbit_b32");
	CASE_STR(335, "v_alignbyte_b32");
	CASE_STR(336, "v_mullit_f32");
	CASE_STR(337, "v_min3_f32");
	CASE_STR(338, "v_min3_i32");
	CASE_STR(339, "v_min3_u32");
	CASE_STR(340, "v_max3_f32");
	CASE_STR(341, "v_max3_i32");
	CASE_STR(342, "v_max3_u32");
	CASE_STR(343, "v_med3_f32");
	CASE_STR(344, "v_med3_i32");
	CASE_STR(345, "v_med3_u32");
	CASE_STR(346, "v_sad_u8");
	CASE_STR(347, "v_sad_hi_u8");
	CASE_STR(348, "v_sad_u16");
	CASE_STR(349, "v_sad_u32");
	CASE_STR(350, "v_cvt_pk_u8_f32");
	CASE_STR(351, "v_div_fixup_f32");
	CASE_STR(352, "v_div_fixup_f64");
	CASE_STR(353, "v_lshl_b64");
	CASE_STR(354, "v_lshr_b64");
	CASE_STR(355, "v_ashr_i64");
	CASE_STR(356, "v_add_f64");
	CASE_STR(357, "v_mul_f64");
	CASE_STR(358, "v_min_f64");
	CASE_STR(359, "v_max_f64");
	CASE_STR(360, "v_ldexp_f64");
	CASE_STR(361, "v_mul_lo_u32");
	CASE_STR(362, "v_mul_hi_u32");
	CASE_STR(363, "v_mul_lo_i32");
	CASE_STR(364, "v_mul_hi_i32");
	CASE_STR(367, "v_div_fmas_f32");
	CASE_STR(368, "v_div_fmas_f64");
	CASE_STR(369, "v_msad_u8");
	CASE_STR(370, "v_qsad_u8");
	CASE_STR(371, "v_mqsad_u8");
	CASE_STR(372, "v_trig_preop_f64");
	default:
		pf("unknown_vop3a_op(%u)", op);
	}
exit:
	return str;
}

static void vop3a(u16 op, u32 w1)
{
	u8 vdst;
	u8 abs;
	u8 clamp;
	u8 *op_str;
	u16 src0;
	u8 *src0_str;
	u16 src1;
	u8 *src1_str;
	u16 src2;
	u8 *src2_str;
	u8 omod;
	u8 neg;

	bool literal_was_read;
	u32 literal;

	literal_was_read = false;

	vdst = (u8)w; /* 8 bits */
	abs = (u8)(w >> 8) & 0x07; /* 3 bits */
	clamp = (u8)(w >> 11) & 0x01; /* 1 bits */
	src0 = (u16)w1 & 0x1ff; /* 9 bits */
	src1 = (u16)(w1 >> 9) & 0x1ff; /* 9 bits */
	src2 = (u16)(w1 >> 18) & 0x1ff; /* 9 bits */
	omod = (u8)(w1 >> 27) & 0x03; /* 2 bits */ 
	neg = (u8)(w1 >> 29) & 0x07; /* 3 bits */
	
	op_str = vop3a_op_to_str(op);
	src0_str = v_opd_to_str(src0, &literal_was_read, &literal);
	src1_str = v_opd_to_str(src1, &literal_was_read, &literal);
	src2_str = v_opd_to_str(src2, &literal_was_read, &literal);

	out("%s vdst=vgpr%u abs=%u clamp=%s src0=%s src1=%s src2=%s omod=%u neg=%u\n", op_str, vdst, abs, (clamp != 0) ? "yes" : "no", src0_str, src1_str, src2_str, omod, neg);
}

static void vop3(void)
{
	u8 r;
	u32 w1;
	u16 op;

	r = read_w(&w1);
	if (r != OK) {
		out("ERROR:vop3[ab]:missing 2nd instruction word\n");
		exit(1);
	}

	op = (u16)(w >> 17) & 0x1f;

	if (293 <= op && op <= 366)
		vop3b(op, w1);
	else
		vop3a(op, w1);
}

static u8 *ds_op_to_str(u8 op)
{
	static u8 str[STR_SZ_MAX];

	switch (op) {
	CASE_STR(  0, "ds_add_u32");
	CASE_STR(  1, "ds_sub_u32");
	CASE_STR(  2, "ds_rsub_u32");
	CASE_STR(  3, "ds_inc_u32");
	CASE_STR(  4, "ds_dec_u32");
	CASE_STR(  5, "ds_min_i32");
	CASE_STR(  6, "ds_max_i32");
	CASE_STR(  7, "ds_min_u32");
	CASE_STR(  8, "ds_max_u32");
	CASE_STR(  9, "ds_and_b32");
	CASE_STR( 10, "ds_or_b32");
	CASE_STR( 11, "ds_xor_b32");
	CASE_STR( 12, "ds_mskor_b32");
	CASE_STR( 13, "ds_write_b32");
	CASE_STR( 14, "ds_write2_b32");
	CASE_STR( 15, "ds_write2st64_b32");
	CASE_STR( 16, "ds_cmpst_b32");
	CASE_STR( 17, "ds_cmpst_f32");
	CASE_STR( 18, "ds_min_f32");
	CASE_STR( 19, "ds_max_f32");
	CASE_STR( 25, "ds_gws_init");
	CASE_STR( 26, "ds_gws_sema_v");
	CASE_STR( 27, "ds_gws_sema_br");
	CASE_STR( 28, "ds_gws_sema_p");
	CASE_STR( 29, "ds_gws_barrier");
	CASE_STR( 30, "ds_write_b8");
	CASE_STR( 31, "ds_write_b16");
	CASE_STR( 32, "ds_add_rtn_u32");
	CASE_STR( 33, "ds_sub_rtn_u32");
	CASE_STR( 34, "ds_rsub_rtn_u32");
	CASE_STR( 35, "ds_inc_rtn_u32");
	CASE_STR( 36, "ds_dec_rtn_u32");
	CASE_STR( 37, "ds_min_rtn_i32");
	CASE_STR( 38, "ds_max_rtn_i32");
	CASE_STR( 39, "ds_min_rtn_u32");
	CASE_STR( 40, "ds_max_rtn_u32");
	CASE_STR( 41, "ds_and_rtn_b32");
	CASE_STR( 42, "ds_or_rtn_b32");
	CASE_STR( 43, "ds_xor_rtn_b32");
	CASE_STR( 44, "ds_mskor_rtn_b32");
	CASE_STR( 45, "ds_wrxchg_rtn_b32");
	CASE_STR( 46, "ds_wrxchg2_rtn_b32");
	CASE_STR( 47, "ds_wrxchg2st64_rtn_b32");
	CASE_STR( 48, "ds_cmpst_rtn_b32");
	CASE_STR( 49, "ds_cmpst_rtn_f32");
	CASE_STR( 50, "ds_min_rtn_f32");
	CASE_STR( 51, "ds_max_rtn_f32");
	CASE_STR( 53, "ds_swizzle_b32");
	CASE_STR( 54, "ds_read_b32");
	CASE_STR( 55, "ds_read2_b32");
	CASE_STR( 56, "ds_read2st_b32");
	CASE_STR( 57, "ds_read_i8");
	CASE_STR( 58, "ds_read_u8");
	CASE_STR( 59, "ds_read_i16");
	CASE_STR( 60, "ds_read_u16");
	CASE_STR( 61, "ds_consume");
	CASE_STR( 62, "ds_append");
	CASE_STR( 63, "ds_ordered_count");
	CASE_STR( 64, "ds_add_u64");
	CASE_STR( 65, "ds_sub_u64");
	CASE_STR( 66, "ds_rsub_u64");
	CASE_STR( 67, "ds_inc_u64");
	CASE_STR( 68, "ds_dec_u64");
	CASE_STR( 69, "ds_min_i64");
	CASE_STR( 70, "ds_max_i64");
	CASE_STR( 71, "ds_min_u64");
	CASE_STR( 72, "ds_max_u64");
	CASE_STR( 73, "ds_and_b64");
	CASE_STR( 74, "ds_or_b64");
	CASE_STR( 75, "ds_xor_b64");
	CASE_STR( 76, "ds_mskor_b64");
	CASE_STR( 77, "ds_write_b64");
	CASE_STR( 78, "ds_write2_b64");
	CASE_STR( 79, "ds_write2st64_b64");
	CASE_STR( 80, "ds_cmpst_b64");
	CASE_STR( 81, "ds_cmpst_f64");
	CASE_STR( 82, "ds_min_f64");
	CASE_STR( 83, "ds_max_f64");
	CASE_STR( 96, "ds_add_rtn_u64");
	CASE_STR( 97, "ds_sub_rtn_u64");
	CASE_STR( 98, "ds_rsub_rtn_u64");
	CASE_STR( 99, "ds_inc_rtn_u64");
	CASE_STR(100, "ds_dec_rtn_u64");
	CASE_STR(101, "ds_min_rtn_i64");
	CASE_STR(102, "ds_max_rtn_i64");
	CASE_STR(103, "ds_min_rtn_u64");
	CASE_STR(104, "ds_max_rtn_u64");
	CASE_STR(105, "ds_and_rtn_b64");
	CASE_STR(106, "ds_or_rtn_b64");
	CASE_STR(107, "ds_xor_rtn_b64");
	CASE_STR(108, "ds_mskxor_rtn_b64");
	CASE_STR(109, "ds_wrxchg_rtn_b64");
	CASE_STR(110, "ds_wrxchg2_rtn_b64");
	CASE_STR(111, "ds_wrxchg2st64_rtn_b64");
	CASE_STR(112, "ds_cmpst_rtn_b64");
	CASE_STR(113, "ds_cmpst_rtn_f64");
	CASE_STR(114, "ds_min_rtn_f64");
	CASE_STR(115, "ds_max_rtn_f64");
	CASE_STR(118, "ds_read_b64");
	CASE_STR(119, "ds_read2_b64");
	CASE_STR(120, "ds_read2st64_b64");
	CASE_STR(128, "ds_add_src2_u32");
	CASE_STR(129, "ds_sub_src2_u32");
	CASE_STR(130, "ds_rsub_src2_u32");
	CASE_STR(131, "ds_inc_src2_u32");
	CASE_STR(132, "ds_dec_src2_u32");
	CASE_STR(133, "ds_min_src2_i32");
	CASE_STR(134, "ds_max_src2_i32");
	CASE_STR(135, "ds_min_src2_u32");
	CASE_STR(136, "ds_max_src2_u32");
	CASE_STR(137, "ds_and_src2_b32");
	CASE_STR(138, "ds_or_src2_b32");
	CASE_STR(139, "ds_xor_src2_b32");
	CASE_STR(140, "ds_write_src2_b32");
	CASE_STR(146, "ds_min_src2_f32");
	CASE_STR(147, "ds_max_src2_f32");
	CASE_STR(192, "ds_add_src2_u64");
	CASE_STR(193, "ds_sub_src2_u64");
	CASE_STR(194, "ds_rsub_src2_u64");
	CASE_STR(195, "ds_inc_src2_u64");
	CASE_STR(196, "ds_dec_src2_u64");
	CASE_STR(197, "ds_min_src2_i64");
	CASE_STR(198, "ds_max_src2_i64");
	CASE_STR(199, "ds_min_src2_u64");
	CASE_STR(200, "ds_max_src2_u64");
	CASE_STR(201, "ds_and_src2_b64");
	CASE_STR(202, "ds_or_src2_b64");
	CASE_STR(203, "ds_xor_src2_b64");
	CASE_STR(204, "ds_write_src2_b64");
	CASE_STR(210, "ds_min_src2_f64");
	CASE_STR(211, "ds_max_src2_f64");
	default:
		pf("unknown_ds_op(%u)", op);
		break;
	}

}

static void ds(void)
{
	u8 r;
	u32 w1;

	u8 offset0;
	u8 offset1;
	u8 gds;
	u8 op;
	u8 *op_str;
	u8 addr;
	u8 data0;
	u8 data1;
	u8 vdst;

	r = read_w(&w1);
	if (r != OK) {
		out("ERROR:ds:missing 2nd instruction word\n");
		exit(1);
	}

	offset0 = (u8)w; /* 8 bits */
	offset1 = (u8)(w >> 8); /* 8 bits */
	gds = (u8)(w >> 17) & 0x01; /* 1 bit */
	op = (u8)(w >> 18); /* 8 bits */
	addr = (u8)w1; /* 8 bits */
	data0 = (u8)(w1 >> 8); /* 8 bits */
	data1 = (u8)(w1 >> 16); /* 8 bits */
	vdst = (u8)(w1 >> 24); /* 8 bits */

	op_str = ds_op_to_str(op);

	out("%s offset0=%u offset1=%u %s addr=vgpr%u data0=vgpr%u data1=vgpr%u vdst=vgpr%u\n", op_str, offset0, offset1, (gds != 0) ? "gds" : "lds", addr, data0, data1, vdst);
}

static u8 *mubuf_op_to_str(u8 op)
{
	static u8 str[STR_SZ_MAX];

	switch (op) {
	CASE_STR(  0, "buffer_load_format_x");
	CASE_STR(  1, "buffer_load_format_xy");
	CASE_STR(  2, "buffer_load_format_xyz");
	CASE_STR(  3, "buffer_load_format_xyzw");
	CASE_STR(  4, "buffer_store_format_x");
	CASE_STR(  5, "buffer_store_format_xy");
	CASE_STR(  6, "buffer_store_format_xyz");
	CASE_STR(  7, "buffer_store_format_xyzw");
	CASE_STR(  8, "buffer_load_ubyte");
	CASE_STR(  9, "buffer_load_sbyte");
	CASE_STR( 10, "buffer_load_ushort");
	CASE_STR( 11, "buffer_load_sshort");
	CASE_STR( 12, "buffer_load_dword");
	CASE_STR( 13, "buffer_load_dwordx2");
	CASE_STR( 14, "buffer_load_dwordx4");
	CASE_STR( 24, "buffer_store_byte");
	CASE_STR( 26, "buffer_store_short");
	CASE_STR( 28, "buffer_store_dword");
	CASE_STR( 29, "buffer_store_dwordx2");
	CASE_STR( 30, "buffer_store_dwordx4");
	CASE_STR( 48, "buffer_atomic_swap");
	CASE_STR( 49, "buffer_atomic_cmpswap");
	CASE_STR( 50, "buffer_atomic_add");
	CASE_STR( 51, "buffer_atomic_sub");
	CASE_STR( 52, "buffer_atomic_rsub");
	CASE_STR( 53, "buffer_atomic_smin");
	CASE_STR( 54, "buffer_atomic_umin");
	CASE_STR( 55, "buffer_atomic_smax");
	CASE_STR( 56, "buffer_atomic_umax");
	CASE_STR( 57, "buffer_atomic_and");
	CASE_STR( 58, "buffer_atomic_or");
	CASE_STR( 59, "buffer_atomic_xor");
	CASE_STR( 60, "buffer_atomic_inc");
	CASE_STR( 61, "buffer_atomic_dec");
	CASE_STR( 62, "buffer_atomic_fcmpswap");
	CASE_STR( 63, "buffer_atomic_fmin");
	CASE_STR( 64, "buffer_atomic_fmax");
	CASE_STR( 80, "buffer_atomic_swap_x2");
	CASE_STR( 81, "buffer_atomic_cmpswap_x2");
	CASE_STR( 82, "buffer_atomic_add_x2");
	CASE_STR( 83, "buffer_atomic_sub_x2");
	CASE_STR( 84, "buffer_atomic_rsub_x2");
	CASE_STR( 85, "buffer_atomic_smin_x2");
	CASE_STR( 86, "buffer_atomic_umin_x2");
	CASE_STR( 87, "buffer_atomic_smax_x2");
	CASE_STR( 88, "buffer_atomic_umax_x2");
	CASE_STR( 89, "buffer_atomic_and_x2");
	CASE_STR( 90, "buffer_atomic_or_x2");
	CASE_STR( 91, "buffer_atomic_xor_x2");
	CASE_STR( 92, "buffer_atomic_inc_x2");
	CASE_STR( 93, "buffer_atomic_dec_x2");
	CASE_STR( 94, "buffer_atomic_fcmpswap_x2");
	CASE_STR( 95, "buffer_atomic_fmin_x2");
	CASE_STR( 96, "buffer_atomic_fmax_x2");
	CASE_STR(112, "buffer_wbinvl1_sc");
	CASE_STR(113, "buffer_wbinvl1");
	default:
		pf("unknown_mubuf_op(%u)", op);
		break;
	}
	return str;
}

static void mubuf(void)
{
	u8 r;
	u32 w1;

	u16 offset;
	u8 offen;
	u8 idxen;
	u8 glc;
	u8 addr64;
	u8 lds;
	u8 op;
	u8 *op_str;
	u8 vaddr;
	u8 vdata;
	u8 srsrc;
	u8 slc;
	u8 tfe;
	u8 soffset;
	u8 *soffset_str;

	u32 literal;
	bool literal_was_read;

	r = read_w(&w1);
	if (r != OK) {
		out("ERROR:mubuf:missing 2nd instruction word\n");
		exit(1);
	}

	literal_was_read = false;

	offset = (u16)w & 0x0fff; /* 12 bits */
	offen = (u8)(w >> 12) & 0x01; /* 1 bit */
	idxen = (u8)(w >> 13) & 0x01; /* 1 bit */
	glc = (u8)(w >> 14) & 0x01; /* 1 bit */
	addr64 = (u8)(w >> 15) & 0x01; /* 1 bit */
	lds = (u8)(w >> 16) & 0x01; /* 1 bit */
	op = (u8)(w >> 18) & 0x7f; /* 7 bits */
	vaddr = (u8)w1 & 0xff; /* 8 bits */
	vdata = (u8)(w1 >> 8) & 0xff; /* 8 bits */
	srsrc = (u8)(w1 >> 16) & 0x1f; /* 5 bits */
	slc = (u8)(w1 >> 22) & 0x01; /* 1 bits */
	tfe = (u8)(w1 >> 23) & 0x01; /* 1 bits */
	soffset = (u8)(w1 >> 24) & 0xff; /* 8 bits */

	op_str = mubuf_op_to_str(op);
	soffset_str = s_opd_to_str(soffset, &literal_was_read, &literal);

	out("%s offset=%u offen=%u idxen=%u glc=%u addr64=%u lds=%u vaddr=vgpr%u vdata=vgpr%u srsrc=%u(sgpr%u) slc=%u tfe=%u soffset=%s\n", op_str, offset, offen, idxen, glc, addr64, lds, vaddr, vdata, srsrc, srsrc << 2, slc, tfe, soffset_str);
}

static u8 *mtbuf_op_to_str(u8 op)
{
	static u8 str[STR_SZ_MAX];

	switch (op) {
	CASE_STR(0, "tbuffer_load_format_x");
	CASE_STR(1, "tbuffer_load_format_xy");
	CASE_STR(2, "tbuffer_load_format_xyz");
	CASE_STR(3, "tbuffer_load_format_xyzw");
	CASE_STR(4, "tbuffer_store_format_x");
	CASE_STR(5, "tbuffer_store_format_xy");
	CASE_STR(6, "tbuffer_store_format_xyz");
	CASE_STR(8, "tbuffer_store_format_xyzw");
	default:
		pf("unknown_mtbuf_op(%u)", op);
		break;
	}
	return str;
}

static void mtbuf(void)
{
	u8 r;
	u32 w1;

	u8 offset;
	u8 offen;
	u8 idxen;
	u8 glc;
	u8 addr64;
	u8 op;
	u8 *op_str;
	u8 dfmt;
	u8 nfmt;
	u8 vaddr;
	u8 vdata;
	u8 srsrc;
	u8 slc;
	u8 tfe;
	u8 soffset;
	u8 *soffset_str;

	u32 literal;
	bool literal_was_read;

	r = read_w(&w1);
	if (r != OK) {
		out("ERROR:mtbuf:missing 2nd instruction word\n");
		exit(1);
	}

	literal_was_read = false;

	offset = (u16)w & 0x0fff; /* 12 bits */
	offen = (u8)(w >> 12) & 0x01; /* 1 bit */
	idxen = (u8)(w >> 13) & 0x01; /* 1 bit */
	glc = (u8)(w >> 14) & 0x01; /* 1 bit */
	addr64 = (u8)(w >> 15) & 0x01; /* 1 bit */
	op = (u8)(w >> 16) & 0x07; /* 3 bits */
	dfmt = (u8)(w >> 19) & 0x0f; /* 4 bits */
	nfmt = (u8)(w >> 23) & 0x07; /* 3 bits */
	vaddr = (u8)w1 & 0xff; /* 8 bits */
	vdata = (u8)(w1 >> 8) & 0xff; /* 8 bits */
	srsrc = (u8)(w1 >> 16) & 0x1f; /* 5 bits */
	slc = (u8)(w1 >> 22) & 0x01; /* 1 bits */
	tfe = (u8)(w1 >> 23) & 0x01; /* 1 bits */
	soffset = (u8)(w1 >> 24) & 0xff; /* 8 bits */

	op_str = mtbuf_op_to_str(op);
	soffset_str = s_opd_to_str(soffset, &literal_was_read, &literal);

	out("%s offset=%u offen=%u idxen=%u glc=%u addr64=%u dfmt=%u nfmt=%u vaddr=vgpr%u vdata=vgpr%u srsrc=%u(sgpr%u) slc=%u tfe=%u soffset=%s\n", op_str, offset, offen, idxen, glc, addr64, dfmt, nfmt, vaddr, vdata, srsrc, srsrc << 2, slc, tfe, soffset_str);
}

static u8 *mimg_op_to_str(u8 op)
{
	static u8 str[STR_SZ_MAX];

	switch (op) {
	CASE_STR(  0, "image_load");
	CASE_STR(  1, "image_load_mip");
	CASE_STR(  2, "image_load_pck");
	CASE_STR(  3, "image_load_pck_sgn");
	CASE_STR(  4, "image_load_mip_pck");
	CASE_STR(  5, "image_load_mip_pck_sgn");
	CASE_STR(  8, "image_store");
	CASE_STR(  9, "image_store_mip");
	CASE_STR( 10, "image_store_pck");
	CASE_STR( 11, "image_store_mip_pck");
	CASE_STR( 14, "image_res_info");
	CASE_STR( 15, "image_atomic_swap");
	CASE_STR( 16, "image_atomic_cmpswap");
	CASE_STR( 17, "image_atomic_add");
	CASE_STR( 18, "image_atomic_sub");
	CASE_STR( 19, "image_atomic_rsub");
	CASE_STR( 20, "image_atomic_smin");
	CASE_STR( 21, "image_atomic_umin");
	CASE_STR( 22, "image_atomic_smax");
	CASE_STR( 23, "image_atomic_umax");
	CASE_STR( 24, "image_atomic_and");
	CASE_STR( 25, "image_atomic_or");
	CASE_STR( 26, "image_atomic_xor");
	CASE_STR( 27, "image_atomic_inc");
	CASE_STR( 28, "image_atomic_dec");
	CASE_STR( 29, "image_atomic_fcmpswap");
	CASE_STR( 30, "image_atomic_fmin");
	CASE_STR( 31, "image_atomic_fmax");
	CASE_STR( 32, "image_sample");
	CASE_STR( 33, "image_sample_cl");
	CASE_STR( 34, "image_sample_d");
	CASE_STR( 35, "image_sample_d_cl");
	CASE_STR( 36, "image_sample_l");
	CASE_STR( 37, "image_sample_b");
	CASE_STR( 38, "image_sample_b_cl");
	CASE_STR( 39, "image_sample_lz");
	CASE_STR( 40, "image_sample_c");
	CASE_STR( 41, "image_sample_c_cl");
	CASE_STR( 42, "image_sample_c_d");
	CASE_STR( 43, "image_sample_c_d_cl");
	CASE_STR( 44, "image_sample_c_l");
	CASE_STR( 45, "image_sample_c_b");
	CASE_STR( 46, "image_sample_c_b_cl");
	CASE_STR( 47, "image_sample_c_lz");
	CASE_STR( 48, "image_sample_o");
	CASE_STR( 49, "image_sample_cl_o");
	CASE_STR( 50, "image_sample_d_o");
	CASE_STR( 51, "image_sample_d_cl_o");
	CASE_STR( 52, "image_sample_l_o");
	CASE_STR( 53, "image_sample_b_o");
	CASE_STR( 54, "image_sample_b_cl_o");
	CASE_STR( 55, "image_sample_b_lz_o");
	CASE_STR( 56, "image_sample_c_o");
	CASE_STR( 57, "image_sample_c_cl_o");
	CASE_STR( 58, "image_sample_c_d_o");
	CASE_STR( 59, "image_sample_c_d_cl_o");
	CASE_STR( 60, "image_sample_c_l_o");
	CASE_STR( 61, "image_sample_c_b_o");
	CASE_STR( 62, "image_sample_c_b_cl_o");
	CASE_STR( 63, "image_sample_c_lz_o");
	CASE_STR( 64, "image_gather4");
	CASE_STR( 65, "image_gather4_cl");
	CASE_STR( 66, "image_gather4_l");
	CASE_STR( 67, "image_gather4_b");
	CASE_STR( 68, "image_gather4_b_cl");
	CASE_STR( 69, "image_gather4_lz");
	CASE_STR( 70, "image_gather4_c");
	CASE_STR( 71, "image_gather4_c_cl");
	CASE_STR( 76, "image_gather4_c_cl");
	CASE_STR( 77, "image_gather4_c_b");
	CASE_STR( 78, "image_gather4_c_b_cl");
	CASE_STR( 79, "image_gather4_c_lz");
	CASE_STR( 80, "image_gather4_o");
	CASE_STR( 81, "image_gather4_cl_o");
	CASE_STR( 84, "image_gather4_l_o");
	CASE_STR( 85, "image_gather4_b_o");
	CASE_STR( 86, "image_gather4_b_cl_o");
	CASE_STR( 87, "image_gather4_lz_o");
	CASE_STR( 88, "image_gather4_c_o");
	CASE_STR( 89, "image_gather4_c_cl_o");
	CASE_STR( 92, "image_gather4_c_l_o");
	CASE_STR( 93, "image_gather4_c_b_o");
	CASE_STR( 94, "image_gather4_c_b_cl_o");
	CASE_STR( 95, "image_gather4_c_lz_o");
	CASE_STR( 96, "image_get_lod");
	CASE_STR(104, "image_sample_cd");
	CASE_STR(105, "image_sample_cd_ld");
	CASE_STR(106, "image_sample_c_cd");
	CASE_STR(107, "image_sample_c_cd_cl");
	CASE_STR(108, "image_sample_cd_o");
	CASE_STR(109, "image_sample_cd_cl_o");
	CASE_STR(110, "image_sample_c_cd_o");
	CASE_STR(111, "image_sample_c_cd_cl_o");
	efault:
		pf("unknown_mimg_op(%u)", op);
		break;
	}
	return str;
}

static void mimg(void)
{
	u8 r;
	u32 w1;

	u8 dmask;
	u8 unorm;
	u8 glc;
	u8 da;
	u8 r128;
	u8 tfe;
	u8 lwe;
	u8 op;
	u8 *op_str;
	u8 slc;
	u8 vaddr;
	u8 vdata;
	u8 srsrc;
	u8 ssamp;
	
	r = read_w(&w1);
	if (r != OK) {
		out("ERROR:mimg:missing 2nd instruction word\n");
		exit(1);
	}

	dmask = (u8)(w >> 8) & 0x0f; /* 4 bits */
	unorm = (u8)(w >> 12) & 0x01; /* 1 bit */
	glc = (u8)(w >> 13) & 0x01; /* 1 bit */
	da = (u8)(w >> 14) & 0x01; /* 1 bit */
	r128 = (u8)(w >> 15) & 0x01; /* 1 bit */
	tfe = (u8)(w >> 16) & 0x01; /* 1 bit */
	lwe = (u8)(w >> 17) & 0x01; /* 1 bit */
	op = (u8)(w >> 18) & 0xff; /* 8 bits */
	slc = (u8)(w >> 25) & 0x01; /* 1 bit */
	vaddr = (u8)w1 & 0xff; /* 8 bits */
	vdata = (u8)(w1 >> 8) & 0xff; /* 8 bits */
	srsrc = (u8)(w1 >> 16) & 0x1f; /* 5 bits */
	ssamp = (u8)(w1 >> 21) & 0x1f; /* 5 bits */

	op_str = mimg_op_to_str(op);

	out("%s dmask=%u unorm=%u glc=%u da=%u r128=%u tfe=%u lwe=%u slc=%u vaddr=vgpr%u vdata=vgpr%u srsrc=%u(sgpr%u) ssamp=%u(sgpr%u)\n", op_str, dmask, unorm, glc, da, r128, tfe, lwe, slc, vaddr, vdata, srsrc, srsrc << 2, ssamp, ssamp << 2);
}

static u8 *smrd_op_to_str(u8 op)
{
	static u8 str[STR_SZ_MAX];

	switch (op) {
	CASE_STR( 0,"s_load_dword");
	CASE_STR( 1,"s_load_dwordx2");
	CASE_STR( 2,"s_load_dwordx4");
	CASE_STR( 3,"s_load_dwordx8");
	CASE_STR( 4,"s_load_dwordx16");
	CASE_STR( 8,"s_buffer_load_dword");
	CASE_STR( 9,"s_buffer_load_dwordx2");
	CASE_STR(10,"s_buffer_load_dwordx4");
	CASE_STR(11,"s_buffer_load_dwordx8");
	CASE_STR(12,"s_buffer_load_dwordx16");
	CASE_STR(30,"s_memtime");
	CASE_STR(31,"s_dcache_inv");
	default:
		pf("unknown_smrd_op(%u)", op);
		break;
	}
	return str;
}

static void smrd(void)
{
	u8 offset;
	u8 imm;
	u8 sbase;
	u8 sdst;
	u8 *sdst_str;
	u8 op;
	u8 *op_str;
	
	u32 literal;
	bool literal_was_read;
	
	literal_was_read = false;

	offset = (u8)w;
	imm = (u8)(w >> 8) & 0x01; /* 1 bit */
	sbase = (u8)(w >> 9) & 0x3f; /* 6 bits */
	sdst = (u8)(w >> 15) & 0xef; /* 7 bits */
	op = (u8)(w >> 22) & 0x1f; /* 5 bits */

	sdst_str = s_opd_to_str(sdst, &literal_was_read, &literal);
	op_str = smrd_op_to_str(op);

	out("%s offset=%u imm=%u sbase=%u sdst=%s\n", op_str, offset, imm, sbase, sdst_str);
}

static u8 *sopk_op_to_str(u8 op)
{
	static u8 str[STR_SZ_MAX];

	switch (op) {
	CASE_STR( 0, "s_movk_i32");
	CASE_STR( 2, "s_cmovk_i32");
	CASE_STR( 3, "s_cmpk_eq_i32");
	CASE_STR( 4, "s_cmpk_lg_i32");
	CASE_STR( 5, "s_cmpk_gt_i32");
	CASE_STR( 6, "s_cmpk_ge_i32");
	CASE_STR( 7, "s_cmpk_lt_i32");
	CASE_STR( 8, "s_cmpk_le_i32");
	CASE_STR( 9, "s_cmpk_eq_u32");
	CASE_STR(10, "s_cmpk_lg_u32");
	CASE_STR(11, "s_cmpk_gt_u32");
	CASE_STR(12, "s_cmpk_ge_u32");
	CASE_STR(13, "s_cmpk_lt_u32");
	CASE_STR(14, "s_cmpk_le_u32");
	CASE_STR(15, "s_addk_i32");
	CASE_STR(16, "s_mulk_i32");
	CASE_STR(17, "s_cbranch_i_fork");
	CASE_STR(18, "s_getreg_b32");
	CASE_STR(19, "s_setreg_b32");
	CASE_STR(21, "s_setreg_imm32_b32");
	default:
		pf("unknown_sopk_op(%u)", op);
		break;	}
	return str;
}

static void sopk(void)
{
	u16 simm16;
	u8 *simm16_str;
	u8 xdst; /* carefull: actually used as a src or dest */
	u8 *xdst_str;
	u8 op;
	u8 *op_str;
	u32 imm32;

	u32 literal;
	bool literal_was_read;
	
	literal_was_read = false;

	simm16 = (u16)w & 0xffff; /* 16 bits */
	xdst = (u8)(w >> 16) & 0x7f; /* 7 bits */
	op = (u8)(w >> 23) & 0x1f; /* 5 bits */

	op_str = sopk_op_to_str(op);
	xdst_str = s_opd_to_str(xdst, &literal_was_read, &literal);

	out("%s simm16=0x%04x xdst=%s", op_str, simm16, xdst_str);

	if (op == 21) { /* s_setreg_imm32_b32 */
		u8 r;

		r = read_w(&imm32);
		if (r != OK) {
			out("ERROR:sopk:s_setreg_imm32_b32:missing imm32 word\n");
			exit(1);
		}
		out("0x%08x\n", imm32);
	} else {
		out("\n");
	}
}

static u8 *sop2_op_to_str(u8 op)
{
	static u8 str[STR_SZ_MAX];

	switch (op) {
	CASE_STR( 0, "s_add_u32");
	CASE_STR( 1, "s_sub_u32");
	CASE_STR( 2, "s_add_i32");
	CASE_STR( 3, "s_sub_i32");
	CASE_STR( 4, "s_addc_u32");
	CASE_STR( 5, "s_subb_u32");
	CASE_STR( 6, "s_min_i32");
	CASE_STR( 7, "s_min_u32");
	CASE_STR( 8, "s_max_i32");
	CASE_STR( 9, "s_max_u32");
	CASE_STR(10, "s_cselect_b32");
	CASE_STR(11, "s_cselect_b64");
	CASE_STR(14, "s_and_b32");
	CASE_STR(15, "s_and_b64");
	CASE_STR(16, "s_or_b32");
	CASE_STR(17, "s_or_b64");
	CASE_STR(18, "s_xor_b32");
	CASE_STR(19, "s_xor_b64");
	CASE_STR(20, "s_andn2_b32");
	CASE_STR(21, "s_andn2_b64");
	CASE_STR(22, "s_orn2_b32");
	CASE_STR(23, "s_orn2_b64");
	CASE_STR(24, "s_nand_b32");
	CASE_STR(25, "s_nand_b64");
	CASE_STR(26, "s_nor_b32");
	CASE_STR(27, "s_nor_b64");
	CASE_STR(28, "s_xnor_b32");
	CASE_STR(29, "s_xnor_b64");
	CASE_STR(30, "s_lshl_b32");
	CASE_STR(31, "s_lshl_b64");
	CASE_STR(32, "s_lshr_b32");
	CASE_STR(33, "s_lshr_b64");
	CASE_STR(34, "s_ashr_i32");
	CASE_STR(35, "s_ashr_i64");
	CASE_STR(36, "s_bfm_b32");
	CASE_STR(37, "s_bfm_b64");
	CASE_STR(38, "s_mul_i32");
	CASE_STR(39, "s_bfe_u32");
	CASE_STR(40, "s_bfe_i32");
	CASE_STR(41, "s_bfe_u64");
	CASE_STR(42, "s_bfe_i64");
	CASE_STR(43, "s_cbranch_g_fork");
	CASE_STR(44, "s_absdiff_i32");
	default:
		pf("unknown_sop2_op(%u)", op);
		break;
	}
	return str;
}

static void sop2(void)
{
	u8 ssrc0;
	u8 *ssrc0_str;
	u8 ssrc1;
	u8 *ssrc1_str;
	u8 sdst;
	u8 *sdst_str;
	u8 op;
	u8 *op_str;

	u32 literal;
	bool literal_was_read;

	literal_was_read = false;

	ssrc0 = (u8)w & 0xff; /* 8 bits */
	ssrc1 = (u8)(w >> 8) & 0xff; /* 8 bits */
	sdst = (u8)(w >> 16) & 0x7f; /* 8 bits */

	ssrc0_str = s_opd_to_str(ssrc0, &literal_was_read, &literal);
	ssrc1_str = s_opd_to_str(ssrc0, &literal_was_read, &literal);
	sdst_str = s_opd_to_str(ssrc0, &literal_was_read, &literal);
	op_str = sop2_op_to_str(op);

	out("%s ssrc0=%s ssrc1=%s sdst=%s\n", op_str, ssrc0_str, ssrc1_str, sdst_str);
}

static u8 *vop2_op_to_str(u8 op)
{
	static u8 str[STR_SZ_MAX];

	if (op <= 36) {
		strcpy(str, v_op_blk_0_to_str(op));
		goto exit;
	}
	if (37 <= op && op <= 42) {
		strcpy(str, v_op_blk_1_to_str(op - 37));
		goto exit;
	}
	if (43 <= op && op <= 49) {
		strcpy(str, v_op_blk_2_to_str(op - 43));
		goto exit;
	}
	pf("unknown_vop2_op(%u)", op);
exit:
	return str;
}

static void vop2(void)
{
	u16 src0;
	u8 *src0_str;
	u8 vsrc1;
	u8 vdst;
	u8 op;
	u8 *op_str;

	u32 literal;
	bool literal_was_read;

	literal_was_read = false;
	
	src0 = (u16)w & 0x01ff; /*  9 bits */
	vsrc1 = (u8)(w >> 9) & 0xff; /* 8 bits */
	vdst = (u8)(w >> 17) & 0xff; /* 8 bits */
	op = (u8)(w >> 25) & 0x3f; /* 6 bits */

	src0_str = v_opd_to_str(src0, &literal_was_read, &literal);
	op_str = vop2_op_to_str(op);

	out("%s src0=%s vsrc1=vgpr%u vdst=vgpr%u\n", op_str, src0_str, vsrc1, vdst);
}

static void microcode_route(void)
{
	     if ((w & 0xff800000) == 0xbe800000) /* 9 bits */
		sop1();
	else if ((w & 0xff800000) == 0xbf000000) /* 9 bits */
		sopc();
	/*-------------------------------------------------*/
	else if ((w & 0xfe000000) == 0xee000000) /* 7 bits */
		vop1();
	else if ((w & 0xfe000000) == 0x7b000000) /* 7 bits */
		vopc();
	else if ((w & 0xfe000000) == 0x76000000) /* 7 bits */
		export();
	/*-------------------------------------------------*/
	else if ((w & 0xfc000000) == 0xc8000000) /* 6 bits */
		vintrp();
	else if ((w & 0xfc000000) == 0xd4000000) /* 6 bits */
		vop3();
	else if ((w & 0xfc000000) == 0xd8000000) /* 6 bits */
		ds();
	else if ((w & 0xfc000000) == 0xe0000000) /* 6 bits */
		mubuf();
	else if ((w & 0xfc000000) == 0xe8000000) /* 6 bits */
		mtbuf();
	else if ((w & 0xfc000000) == 0xf0000000) /* 6 bits */
		mimg();
	/*-------------------------------------------------*/
	else if ((w & 0xf8000000) == 0xc0000000) /* 5 bits */
		smrd();
	/*-------------------------------------------------*/
	else if ((w & 0xf0000000) == 0xb0000000) /* 4 bits */
		sopk();
	/*-------------------------------------------------*/
	else if	((w & 0xc0000000) == 0x80000000) /* 2 bits */
		sop2();
	/*-------------------------------------------------*/
	else if ((w & 0x80000000) == 0x00000000) /* 1 bit */
		vop2();
}
#undef MATCH

int main(void)
{
	u8 r;
	clearerr(stdin);
	loop {
		r = read_w(&w);
		if (r != OK)
			exit(0);

		microcode_route();
	}
}
