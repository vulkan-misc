#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
/*
 * ABBREVIATIONS:
 * blk : BLocK
 * cfg : ConFiG
 * coher : COHERent
 * ctx : ConTeX
 * desc : DESCription
 * dest : DESTination
 * hdr :HeaDeR
 * idx : InDeX
 * me : Micro Engine
 * n : couNt
 * oft(s) : OFfseT(S)
 * pfp : PreFetch Processor
 * pkt : PacKeT
 * reg(s) : REGister(S)
 * sel : SELection
 * src : SouRCe
 * w(s) : Word(S)
 */
#define u8 uint8_t
#define u16 uint16_t
#define u32 uint32_t
#define u64 uint64_t
#define constant enum
#define loop for(;;)
#define out(fmt,...) fprintf(stdout,fmt, ##__VA_ARGS__)
#define ARRAY_N(x) ((u64)(sizeof(x) / sizeof(x[0])))
/*----------------------------------------------------------------------------*/
static u32 pkt_hdr_w;
#define PKT_HDR_TYPE (pkt_hdr_w >> 30)
/* 0-based index of the last body w from the first body w */
#define PKT_HDR_BODY_WS_LAST ((pkt_hdr_w & 0x3fff0000) >> 16)
/* n of ws in body is all pkt types bits [29:16], aka 14 bits */
static u32 pkt_body_ws[0x3fff];
static u32 pkt_body_ws_n;
/*----------------------------------------------------------------------------*/
#define PKT3_OPCODE_NOP				0x10
#define PKT3_OPCODE_SET_BASE			0x11
#define PKT3_OPCODE_INDEX_BUFFER_SIZE		0x13
/* vulkan indirect draw */
#define PKT3_OPCODE_DRAW_INDIRECT		0x24
/* vulkan indirect draw */
#define PKT3_OPCODE_DRAW_INDEX_INDIRECT		0x25
#define PKT3_OPCODE_INDEX_BASE			0x26
/* vulkan draw_indexed */
#define PKT3_OPCODE_DRAW_INDEX_2		0x27
/* vulkan indirect draw */
#define PKT3_OPCODE_DRAW_INDIRECT_MULTI		0x2c
/* vulkan draw */
#define PKT3_OPCODE_DRAW_INDEX_AUTO		0x2d
#define PKT3_OPCODE_NUM_INSTANCES		0x2f
#define PKT3_OPCODE_WRITE_DATA			0x37
/* vulkan indirect draw */
#define PKT3_OPCODE_DRAW_INDEX_INDIRECT_MULTI	0x38
#define PKT3_OPCODE_COPY_DATA			0x40
#define PKT3_OPCODE_PFP_SYNC_ME			0x42
#define PKT3_OPCODE_SURFACE_SYNC		0x43 /* deprecated on CIK chips */
#define PKT3_OPCODE_EVENT_WRITE			0x46
#define PKT3_OPCODE_SET_CONFIG			0x68
#define PKT3_OPCODE_SET_CONTEXT			0x69
#define PKT3_OPCODE_SET_SH_REG			0x76
/*----------------------------------------------------------------------------*/
#define OK 0
#define END 1
#define ERR 2
static u8 pkt_hdr_w_read(void)
{
	size_t read_bytes_n;

	read_bytes_n = fread(&pkt_hdr_w, sizeof(pkt_hdr_w), 1, stdin);
	if (read_bytes_n != sizeof(pkt_hdr_w)) {
		if (feof(stdin) != 0)
			return END;
		if (ferror(stdin) != 0)
			return ERR;
	}
}

static u8 pkt_body_ws_read(void)
		
{
	size_t read_bytes_n;

	read_bytes_n = fread(pkt_body_ws, sizeof(u32), pkt_body_ws_n, stdin);
	if (read_bytes_n != (sizeof(u32) * pkt_body_ws_n)) {
		if (feof(stdin) != 0)
			return END;
		if (ferror(stdin) != 0)
			return ERR;
	}
}
/*----------------------------------------------------------------------------*/
#include "regs.h"
static u8 *reg_name(u8 blk, u64 w_oft)
{
	u64 i;
	u8 *name;
	static u8 desc[256];
	u64 global_w_oft;

	i = 0;
	loop {
		if (i == ARRAY_N(reg_descs)) {
			name = "no_register_description";
			break;
		}
		if (reg_descs[i].blk == blk && reg_descs[i].w_oft == w_oft) {
			name = reg_descs[i].name;
			break;
		}
		++i;
	}
	global_w_oft = reg_blk_w_ofts[blk] + w_oft;
	snprintf(desc, 256, "%s[0x%016lx global byte offset/%lu global byte offset/0x%016lx global word offset]", name, global_w_oft * 4, global_w_oft * 4, global_w_oft);
	return desc;
}
/*----------------------------------------------------------------------------*/
#define PKT0_BASE_IDX (pkt_hdr_w & 0x0000ffff) 
static void pkt0_out(void)
{
	u16 i;

	out("\nPKT0(%u words):base_idx_w=0x%08x\n", pkt_body_ws_n, PKT0_BASE_IDX);

	i = 0;
	loop {
		if (i == pkt_body_ws_n)
			break;
		out("\tREG_DATA[%u]=0x%08x\n", i, pkt_body_ws[i]);
		++i;
	}
}

static void pkt1_out(void)
{
	out("\nPKT1(UNSUPPORTED)\n");
}

static void pkt2_out(void)
{
	out("\nPKT2(FILLER)\n");
}

static u8 *shader_type_to_str(u8 type)
{
	if (type != 0)
		return "compute";
	return "graphics";
}

static u8 *engine_to_str(u8 engine)
{
	switch (engine) {
	case 0x00:
		return "micro_engine";
	case 0x01:
		return "prefect_engine(unsupported)";
	case 0x02:
		return "constant_engine";
	case 0x04:
		return "reserved";
	default:
		return "unknown_engine_code";
	}
}

static u8 *count_sel_to_str(u8 count_sel)
{
	switch (count_sel) {
	case 0:
		return "32bits";
	case 1:
		return "64bits";
	default:
		return "unknown_w_count_selection_code";
	}
}

static u8 *dest_sel_to_str(u8 dest_sel)
{
	switch (dest_sel) {
	case 0x00:
		return "memory_mapped_register";
	case 0x01:
		return "memory(synchronous)";
	case 0x02:
		return "texture_cache(level_2)";
	case 0x03:
		return "global_data_share";
	case 0x04:
		return "reserved";
	case 0x05:
		return "memory(asynchronous)";
	default:
		return "unknown_destination_selection_code";
	}
}

static u8 *src_sel_to_str(u8 src_sel)
{
	switch (src_sel) {
	case 0x00:
		return "memory_mapped_register";
	case 0x01:
		return "memory";
	case 0x02:
		return "texture_cache(level_2)";
	case 0x03:
		return "global_data_share";
	case 0x04:
		return "reserved";
	case 0x05:
		return "immediate_data";
	case 0x06:
		return "atomic_return_data";
	case 0x07:
		return "global_data_share_return_data_0";
	case 0x08:
		return "global_data_share_return_data_1";
	default:
		return "unknown_source_selection_code";
	}
}

#define WRITE_DATA_ENGINE ((u8)(pkt_body_ws[0] & 0xc0000000) >> 31)
#define WRITE_DATA_WRITE_IS_CONFIRMED ((pkt_body_ws[0] & 0x00100000) != 0)
#define WRITE_DATA_DOES_INCREMENT_ADDRESS ((u8)(pkt_body_ws[0] & 0x00010000) != 0)
#define WRITE_DATA_DEST_SEL ((u8)((pkt_body_ws[0] & 0x000000f00) >> 8))
static void opcode_write_data_out(void)
{
	u16 i;

	out("\tcontrol:engine=%s wait_write_confirmation=%s increment_address=%s destination_selection=%s\n", engine_to_str(WRITE_DATA_ENGINE), WRITE_DATA_WRITE_IS_CONFIRMED ? "yes" : "no", WRITE_DATA_DOES_INCREMENT_ADDRESS ? "yes" : "no", dest_sel_to_str(WRITE_DATA_DEST_SEL));
	out("\tdestination_address_lo=0x%08x\n", pkt_body_ws[1]);
	out("\tdestination_address_hi=0x%08x\n", pkt_body_ws[2]);

	i = 3;
	loop {
		if (i == pkt_body_ws_n)
			break;
		out("\tdata[%u]=0x%08x\n", i - 3, pkt_body_ws[i]);
		++i;
	}
}

#define COPY_DATA_ENGINE ((u8)(pkt_body_ws[0] & 0xc0000000) >> 31)
#define COPY_DATA_WRITE_IS_CONFIRMED ((pkt_body_ws[0] & 0x00100000) != 0)
#define COPY_DATA_COUNT_SEL ((u8)(pkt_body_ws[0] & 0x00010000) >> 16)
#define COPY_DATA_DEST_SEL ((u8)((pkt_body_ws[0] & 0x000000f00) >> 8))
#define COPY_DATA_SRC_SEL ((u8)(pkt_body_ws[0] & 0x0000000f))
static void opcode_copy_data_out(void)
{
	out("\tcontrol:engine=%s wait_write_confirmation=%s count_selection=%s destination_selection=%s source_selection=%s\n", engine_to_str(COPY_DATA_ENGINE), COPY_DATA_WRITE_IS_CONFIRMED ? "yes" : "no", count_sel_to_str(COPY_DATA_COUNT_SEL), dest_sel_to_str(COPY_DATA_DEST_SEL), src_sel_to_str(COPY_DATA_SRC_SEL));
	out("\tsrc_addr_lo=0x%08x\n", pkt_body_ws[1]);
	out("\tsrc_addr_hi=0x%08x\n", pkt_body_ws[2]);
	out("\tdest_addr_lo=0x%08x\n", pkt_body_ws[3]);
	out("\tdest_addr_hi=0x%08x\n", pkt_body_ws[4]);
}

static u8 *opcode_to_str(u8 opcode)
{
	/* TODO */
	switch (opcode) {
	case PKT3_OPCODE_NOP:
		return "nop";
	case PKT3_OPCODE_SET_SH_REG:
		return "set_sh_reg";
	case PKT3_OPCODE_WRITE_DATA:
		return "write_data";
	case PKT3_OPCODE_COPY_DATA:
		return "copy_data";
	case PKT3_OPCODE_DRAW_INDIRECT:
		return "draw_indirect";
	case PKT3_OPCODE_DRAW_INDEX_INDIRECT:
		return "draw_index_indirect";
	case PKT3_OPCODE_DRAW_INDEX_2:
		return "draw_index_2";
	case PKT3_OPCODE_DRAW_INDIRECT_MULTI:
		return "draw_indirect_multi";
	case PKT3_OPCODE_DRAW_INDEX_AUTO:
		return "draw_index_auto";
	case PKT3_OPCODE_DRAW_INDEX_INDIRECT_MULTI:
		return "draw_index_indirect_multi";
	case PKT3_OPCODE_SET_BASE:
		return "set_base";
	case PKT3_OPCODE_INDEX_BASE:
		return "index_base";
	case PKT3_OPCODE_INDEX_BUFFER_SIZE:
		return "index_buffer_size";
	case PKT3_OPCODE_SET_CONTEXT:
		return "set_context";
	case PKT3_OPCODE_SET_CONFIG:
		return "set_config";
	case PKT3_OPCODE_NUM_INSTANCES:
		return "num_instances";
	case PKT3_OPCODE_EVENT_WRITE:
		return "event_write";
	case PKT3_OPCODE_PFP_SYNC_ME:
		return "pfp_sync_me";
	case PKT3_OPCODE_SURFACE_SYNC:
		return "surface_sync(deprecated on cik chips)";
	default:
		return "unknown_opcode";
	}
}

/* an all chips, the register byte offset is 0x8000, or 0x2000 as a w offset */
static void opcode_set_config_reg_out(void)
{
	u16 i;

	out("\treg_offset_w=0x%08x\n", pkt_body_ws[0]);

	i = 1;
	loop {
		if (i == pkt_body_ws_n)
			break;
		out("\treg_data[%u]=0x%08x %s\n", i - 1, pkt_body_ws[i], reg_name(reg_blk_cfg, (u64)pkt_body_ws[0] + i - 1));
		++i;
	}
}

/* an all chips, the register byte offset is 0xb000, or 0x2c00 as a w offset */
static void opcode_set_sh_reg_out(void)
{
	u16 i;

	out("\treg_offset_w=0x%08x\n", pkt_body_ws[0]);

	i = 1;
	loop {
		if (i == pkt_body_ws_n)
			break;
		out("\treg_data[%u]=0x%08x %s\n", i - 1, pkt_body_ws[i], reg_name(reg_blk_sh, (u64)pkt_body_ws[0] + i - 1));
		++i;
	}
}

/* an all chips, the register byte offset is 0x28000, or 0xa000 as a w offset */
static void opcode_set_context_reg_out(void)
{
	u16 i;

	out("\treg_offset_w=0x%08x\n", pkt_body_ws[0]);

	i = 1;
	loop {
		if (i == pkt_body_ws_n)
			break;
		out("\treg_data[%u]=0x%08x %s\n", i - 1, pkt_body_ws[i], reg_name(reg_blk_ctx, (u64)pkt_body_ws[0] + i - 1));
		++i;
	}
}

static void opcode_event_write_out(void)
{
	out("\tevent_cntl=0x%08x\n", pkt_body_ws[0]);
	if (pkt_body_ws_n >= 2)
		out("\taddress_lo=0x%08x", pkt_body_ws[1]);
	if (pkt_body_ws_n >= 3)
		out("\taddress_hi=0x%08x", pkt_body_ws[2]);
}

#define PKT3_HDR_IS_PREDICATE ((pkt_hdr_w & 0x1) != 0)
#define PKT3_HDR_SHADER_TYPE ((u8)((pkt_hdr_w & 0x2) >> 1))
#define PKT3_HDR_OPCODE ((u8)((pkt_hdr_w & 0x0000ff00) >> 8))
static void pkt3_out(void)
{
	out("\nPKT3(%u words):opcode=%s(0x%x) is_predicate=%s shader_type_is=%s\n", pkt_body_ws_n, opcode_to_str(PKT3_HDR_OPCODE), PKT3_HDR_OPCODE, PKT3_HDR_IS_PREDICATE ? "yes" : "no", shader_type_to_str(PKT3_HDR_SHADER_TYPE));

	/* TODO */
	switch (PKT3_HDR_OPCODE) {
	case PKT3_OPCODE_NOP:
		/* discard the body ws */
		break;
	case PKT3_OPCODE_SET_SH_REG:
		opcode_set_sh_reg_out();
		break;
	case PKT3_OPCODE_WRITE_DATA:
		opcode_write_data_out();
		break;
	case PKT3_OPCODE_COPY_DATA:
		opcode_copy_data_out();
		break;
	case PKT3_OPCODE_DRAW_INDEX_AUTO:
		out("\tindexes_n(written to VGT_NUM_INDICE)=%u\n", pkt_body_ws[0]);
		out("\tvgt_draw_initiator=0x%08x\n", pkt_body_ws[1]);
		break;
	case PKT3_OPCODE_DRAW_INDEX_2:
		/* this is the hard limit for the dma engine */
		out("\tindexes_n_max=%u\n", pkt_body_ws[0]);
		out("\tindexes_buf_addr_lo=0x%08x\n", pkt_body_ws[1]);
		out("\tindexes_buf_addr_hi=0x%08x\n", pkt_body_ws[2]);
		out("\tindexes_n=%u\n", pkt_body_ws[3]);
		out("\tvgt_draw_initiator=0x%08x\n", pkt_body_ws[4]);
		break;
	/*===================================================================*/
	/*
	 * the command processor will read structs in a vram buffer which
	 * describes what to do. The vram buffer is setup with the PKT3
	 * SET_BASE, and for the index version, the index buffer should be 
	 * setup (considering DMA memory access granularity) with PKT3
	 * INDEX_BASE and INDEX_BUFFER_SIZE (see AMD programing manuals).
	 */
	case PKT3_OPCODE_DRAW_INDIRECT:
		out("\tdata_offset=0x%08x\n", pkt_body_ws[0]);
		out("\tbase_vertex_location=0x%08x(user sgpr selected via a register shader block word offset\n", pkt_body_ws[1]);
		out("\tstart_instance_location=0x%08x\n(user sgpr selected via a register shader block word offset)", pkt_body_ws[2]);
		out("\tvgt_draw_initiator=0x%08x\n", pkt_body_ws[3]);
		break;
	case PKT3_OPCODE_DRAW_INDEX_INDIRECT:
		out("\tdata_offset=0x%08x\n", pkt_body_ws[0]);
		out("\tbase_vertex_location=0x%08x(user sgpr selected via a register shader block word offset)\n", pkt_body_ws[1]);
		out("\tstart_instance_location=0x%08x\n(user sgpr selected via a register shader block word offset)", pkt_body_ws[2]);
		out("\tvgt_draw_initiator=0x%08x\n", pkt_body_ws[3]);
		break;
	/*===================================================================*/
	/*
	 * non documented. based on driver code (radv_cmd_buffer.c). it seems
	 * to be the same than above, but with an array of lines of such
	 * buffers, hence with a stride to go from one line to the next one,
	 * and with a count specific for each line, namely with a array of
	 * counts in vram.
	 */
	case PKT3_OPCODE_DRAW_INDIRECT_MULTI:
	case PKT3_OPCODE_DRAW_INDEX_INDIRECT_MULTI:
		out("\tdata_offset=0x%08x\n", pkt_body_ws[0]);
		out("\tbase_vertex_location=0x%08x(user sgpr selected via a register shader block word offset)\n", pkt_body_ws[1]);
		out("\tstart_instance_location=0x%08x\n(user sgpr selected via a register shader block word offset)", pkt_body_ws[2]);
		out("\tnon_documented=0x%08x\n(user sgpr selected via a register shader block word offset with bits 31/30 enabling the index mode/indirect count)", pkt_body_ws[3]);
		out("\tdraw_count=%u\n", pkt_body_ws[4]);
		out("\tcount_va=0x%08x\n", pkt_body_ws[5]);
		out("\tcount_va_hi=0x%08x\n", pkt_body_ws[6]);
		out("\tstride=0x%08x\n", pkt_body_ws[7]);
		out("\tvgt_draw_initiator=0x%08x\n", pkt_body_ws[8]);
		break;
	/*===================================================================*/
	case PKT3_OPCODE_SET_BASE:
		out("\tbase_index=0x%08x\n", pkt_body_ws[0]);
		out("\taddress_0=0x%08x\n", pkt_body_ws[1]);
		out("\taddress_1=0x%08x\n", pkt_body_ws[2]);
		break;
	case PKT3_OPCODE_INDEX_BASE:
		out("\tindex_base_lo=0x%08x\n", pkt_body_ws[0]);
		out("\tindex_base_hi=0x%08x\n", pkt_body_ws[1]);
		break;
	case PKT3_OPCODE_INDEX_BUFFER_SIZE:
		out("\tindex_buffer_size=0x%08x\n", pkt_body_ws[0]);
		break;
	case PKT3_OPCODE_SET_CONTEXT:
		opcode_set_context_reg_out();
		break;
	case PKT3_OPCODE_SET_CONFIG:
		opcode_set_config_reg_out();
		break;
	case PKT3_OPCODE_NUM_INSTANCES:
		out("\tnum_instances=%u\n", pkt_body_ws[0]);
		break;
	case PKT3_OPCODE_EVENT_WRITE:
		opcode_event_write_out();
		break;
	case PKT3_OPCODE_PFP_SYNC_ME:
		out("\tdummy_data=0x%08x\n", pkt_body_ws[0]);
		break;
	case PKT3_OPCODE_SURFACE_SYNC:
		out("\tword[0]=0x%08x\n", pkt_body_ws[0]);
		out("\tcoher_size=0x%08x\n", pkt_body_ws[1]);
		out("\tcoher_base=0x%08x\n", pkt_body_ws[2]);
		out("\tpoll_interval=0x%08x\n", pkt_body_ws[3]);
		break;
	default:
		/* unknow opcode, we do not interpret the body ws */
		break;
	}
}

static void pkt_out(void)
{
	switch (PKT_HDR_TYPE) {
	case 0:
		pkt0_out();
		break;
	case 1:
		pkt1_out();
		break;
	case 2:
		pkt2_out();
		break;
	case 3:
		pkt3_out();
		break;
	}
}

int main(void)
{
	clearerr(stdin);
	loop {
		u8 r;

		r = pkt_hdr_w_read();	
		if (r != OK)
			exit(0);

		pkt_body_ws_n = PKT_HDR_BODY_WS_LAST + 1;

		r = pkt_body_ws_read();	

		pkt_out();

		if (r != OK) {
			out("ERROR:packet is incomplete\n");
			exit(1);
		}
	}
	exit(0);
}
