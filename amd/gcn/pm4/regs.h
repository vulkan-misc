#ifndef REGS_H
#define REGS_H
/*****************************************************************************/
/* GFX6 */
/*
 * ABBREVIATIONS:
 *
 * AA : AntiAliasing (MultiSample AntiAliasing, MSAA)
 * ADDR : ADDRess
 * ADJ : ADJus
 * BARYC : BARYCentric
 * BR : Bottom Right
 * CB : Color Block
 * CL : CLipper
 * CNTL: CoNTroL
 * COL : COLor
 * CONFIG : CONFIGuration
 * DB : Depth Block
 * DISC : DISCard
 * EN/ENA : ENAble
 * GB : Guard Band
 * GS : Geometry Shader
 * HI : HIgh ("high 32 bits of a 64 bits word)
 * HORZ : HORiZontal
 * IB : Indirect Buffer
 * IN : INput
 * INFO : INFOrmation
 * INTERP : INTERPolator
 * LO : LOw ("low 32 bits" of a 64 bits word)
 * LOCS : LOCationS
 * OFF : OFFset
 * PA : Primitive Assembler
 * PGM : ProGraM
 * POS : POSition
 * PRIM : PRIMitive
 * PS : Pixel Shader
 * RECT : RECTangle
 * RSRC : ReSouRCe
 * SC : Scan Converter
 * SPI : Shader Processor Interpolator
 * SU : Setup Unit 
 * TE : Transform/Tesselation Engine
 * TL : Top Left
 * TMP : TeMPorary
 * VERT : VERTical
 * VGT : Vertex Grouper Tesselator
 * VPORT : ViewPORT
 * VS : Vertex Shader
 * VTE : Vertex Transform Engine
 * VTX : VerTeX
 * UCP: User Clip Plane
 */
/*****************************************************************************/
constant {
	reg_blk_lo = 0,
	reg_blk_cfg = 1, /* only 1 unlike ctxs */
	reg_blk_sh = 2,
	reg_blk_ctx = 3 /* there are N (depends on GFX) ctxs */
};

/*
 * gfx[6-10]
 * 64KiB reg space:
 * low 32KiB, use PKT0:
 *   0x00000000-0x00007ffff byte ofts
 *   0x00000000-0x00001ffff w ofts
 * hi 32KiB, use PKT3 with the right SET_* opcode
 *   0x00008000-0x0000fffff byte ofts
 *   0x00002000-0x00003ffff w ofts
 */
u64 reg_blk_w_ofts[] = {
	[reg_blk_lo]	= 0,
	/*-------------------------------------------------------------------*/
	[reg_blk_cfg]	= 0x0000000000002000,
	[reg_blk_sh]	= 0x0000000000002c00,
	[reg_blk_ctx]	= 0x000000000000a000,
};

struct reg_desc {
	u8 blk;
	u64 w_oft; 
	u8 *name;
};

/* may add a gfx blk version mask, gfx6 for now */
struct reg_desc reg_descs[] = {
	{
		reg_blk_cfg,
		0x256,
		"VGT_PRIMITIVE_TYPE"
	},
	/*-------------------------------------------------------------------*/
	{
		reg_blk_sh,
		0x8,
		"SPI_SHADER_PGM_LO_PS"
	},
	{
		reg_blk_sh,
		0x9,
		"SPI_SHADER_PGM_HI_PS"
	},
	{
		reg_blk_sh,
		0xa,
		"SPI_SHADER_PGM_RSRC1_PS"
	},
	{
		reg_blk_sh,
		0xb,
		"SPI_SHADER_PGM_RSRC2_PS"
	},
	{
		reg_blk_sh,
		0xe,
		"SPI_SHADER_USER_DATA_PS_2"
	},
	{
		reg_blk_sh,
		0xf,
		"SPI_SHADER_USER_DATA_PS_3"
	},
	{
		reg_blk_sh,
		0x48,
		"SPI_SHADER_PGM_LO_VS"
	},
	{
		reg_blk_sh,
		0x49,
		"SPI_SHADER_PGM_HI_VS"
	},
	{
		reg_blk_sh,
		0x4a,
		"SPI_SHADER_PGM_RSRC1_VS"
	},
	{
		reg_blk_sh,
		0x4b,
		"SPI_SHADER_PGM_RSRC2_VS"
	},
	{
		reg_blk_sh,
		0x4e,
		"SPI_SHADER_USER_DATA_VS_2"
	},
	{
		reg_blk_sh,
		0x4f,
		"SPI_SHADER_USER_DATA_VS_3"
	},
	{
		reg_blk_sh,
		0x50,
		"SPI_SHADER_USER_DATA_VS_4"
	},
	{
		reg_blk_sh,
		0x51,
		"SPI_SHADER_USER_DATA_VS_5"
	},
	{
		reg_blk_sh,
		0x52,
		"SPI_SHADER_USER_DATA_VS_6"
	},
	{
		reg_blk_sh,
		0x53,
		"SPI_SHADER_USER_DATA_VS_7"
	},
	/*-------------------------------------------------------------------*/
	{
		reg_blk_ctx,
		0x0,
		"DB_RENDER_CONTROL"
	},
	{
		reg_blk_ctx,
		0x3,
		"DB_RENDER_OVERRIDE"
	},
	{
		reg_blk_ctx,
		0x4,
		"DB_RENDER_OVERRIDE2"
	},
	{
		reg_blk_ctx,
		0x10,
		"DB_Z_INFO"
	},
	{
		reg_blk_ctx,
		0x11,
		"DB_STENCIL_INFO"
	},
	{
		reg_blk_ctx,
		0x82,
		"PA_SC_WINDOW_SCISSOR_BR"
	},
	{
		reg_blk_ctx,
		0x8e,
		"CB_TARGET_MASK"
	},
	{
		reg_blk_ctx,
		0x83,
		"PA_SC_CLIPRECT_RULE"
	},
	{
		reg_blk_ctx,
		0x8f,
		"CB_SHADER_MASK"
	},
	{
		reg_blk_ctx,
		0x94,
		"PA_SC_VPORT_SCISSOR_0_TL"
	},
	{
		reg_blk_ctx,
		0x95,
		"PA_SC_VPORT_SCISSOR_0_BR"
	},
	{
		reg_blk_ctx,
		0xb4,
		"PA_SC_VPORT_ZMIN_0"
	},
	{
		reg_blk_ctx,
		0xb5,
		"PA_SC_VPORT_ZMAX_0"
	},
	{
		reg_blk_ctx,
		0x10b,
		"DB_STENCIL_CONTROL"
	},
	{
		reg_blk_ctx,
		0x10f,
		"PA_CL_VPORT_XSCALE"
	},
	{
		reg_blk_ctx,
		0x110,
		"PA_CL_VPORT_XOFFSET"
	},
	{
		reg_blk_ctx,
		0x111,
		"PA_CL_VPORT_YSCALE"
	},
	{
		reg_blk_ctx,
		0x112,
		"PA_CL_VPORT_YOFFSET"
	},
	{
		reg_blk_ctx,
		0x113,
		"PA_CL_VPORT_ZSCALE"
	},
	{
		reg_blk_ctx,
		0x114,
		"PA_CL_VPORT_ZOFFSET"
	},
	{
		reg_blk_ctx,
		0x191,
		"SPI_PS_INPUT_CNTL_0"
	},
	{
		reg_blk_ctx,
		0x1b1,
		"SPI_VS_OUT_CONFIG"
	},
	{
		reg_blk_ctx,
		0x1b3,
		"SPI_PS_INPUT_ENA"
	},
	{
		reg_blk_ctx,
		0x1b4,
		"SPI_PS_INPUT_ADDR"
	},
	{
		reg_blk_ctx,
		0x1b5,
		"SPI_INTERP_CONTROL_0"
	},
	{
		reg_blk_ctx,
		0x1b6,
		"SPI_PS_IN_CONTROL"
	},
	{
		reg_blk_ctx,
		0x1b8,
		"SPI_BARYC_CNTL"
	},
	{
		reg_blk_ctx,
		0x1ba,
		"SPI_TMPRING_SIZE"
	},
	{
		reg_blk_ctx,
		0x1c3,
		"SPI_SHADER_POS_FORMAT"
	},
	{
		reg_blk_ctx,
		0x1c4,
		"SPI_SHADER_Z_FORMAT"
	},
	{
		reg_blk_ctx,
		0x1c5,
		"SPI_SHADER_COL_FORMAT"
	},
	{
		reg_blk_ctx,
		0x1e0,
		"CB_BLEND0_CONTROL"
	},
	{
		reg_blk_ctx,
		0x1e1,
		"CB_BLEND1_CONTROL"
	},
	{
		reg_blk_ctx,
		0x1e2,
		"CB_BLEND2_CONTROL"
	},
	{
		reg_blk_ctx,
		0x1e3,
		"CB_BLEND3_CONTROL"
	},
	{
		reg_blk_ctx,
		0x1e4,
		"CB_BLEND4_CONTROL"
	},
	{
		reg_blk_ctx,
		0x1e5,
		"CB_BLEND5_CONTROL"
	},
	{
		reg_blk_ctx,
		0x1e6,
		"CB_BLEND6_CONTROL"
	},
	{
		reg_blk_ctx,
		0x1e7,
		"CB_BLEND7_CONTROL"
	},
	{
		reg_blk_ctx,
		0x200,
		"DB_DEPTH_CONTROL"
	},
	{
		reg_blk_ctx,
		0x201,
		"DB_EQAA"
	},
	{
		reg_blk_ctx,
		0x202,
		"CB_COLOR_CONTROL"
	},
	{
		reg_blk_ctx,
		0x203,
		"DB_SHADER_CONTROL"
	},
	{
		reg_blk_ctx,
		0x204,
		"PA_CL_CLIP_CNTL"
	},
	{
		reg_blk_ctx,
		0x205,
		"PA_SU_SC_MODE_CNTL"
	},
	{
		reg_blk_ctx,
		0x206,
		"PA_CL_VTE_CNTL"
	},
	{
		reg_blk_ctx,
		0x207,
		"PA_CL_VS_OUT_CNTL"
	},
	{
		reg_blk_ctx,
		0x20b,
		"PA_SU_PRIM_FILTER_CNTL"
	},
	{
		reg_blk_ctx,
		0x290,
		"VGT_GS_MODE"
	},
	{
		reg_blk_ctx,
		0x292,
		"PA_SC_MODE_CNTL_0"
	},
	{
		reg_blk_ctx,
		0x293,
		"PA_SC_MODE_CNTL_1"
	},
	{
		reg_blk_ctx,
		0x29b,
		"VGT_GS_OUT_PRIM_TYPE"
	},
	{
		reg_blk_ctx,
		0x2a1,
		"VGT_PRIMITIVEID_EN"
	},
	{
		reg_blk_ctx,
		0x2a5,
		"VGT_MULTI_PRIM_IB_RESET_EN"
	},
	{
		reg_blk_ctx,
		0x2aa,
		"IA_MULTI_VGT_PARAM"
	},
	{
		reg_blk_ctx,
		0x2ad,
		"VGT_REUSE_OFF"
	},
	{
		reg_blk_ctx,
		0x2d5,
		"VGT_SHADER_STAGES_EN"
	},
	{
		reg_blk_ctx,
		0x2dc,
		"DB_ALPHA_TO_MASK"
	},
	{
		reg_blk_ctx,
		0x2f5,
		"PA_SC_CENTROID_PRIORITY_0"
	},
	{
		reg_blk_ctx,
		0x2f6,
		"PA_SC_CENTROID_PRIORITY_1"
	},
	{
		reg_blk_ctx,
		0x2f7,
		"PA_SC_LINE_CNTL"
	},
	{
		reg_blk_ctx,
		0x2f8,
		"PA_SC_AA_CONFIG"
	},
	{
		reg_blk_ctx,
		0x2f9,
		"PA_SU_VTX_CNTL"
	},
	{
		reg_blk_ctx,
		0x2fa,
		"PA_CL_GB_VERT_CLIP_ADJ"
	},
	{
		reg_blk_ctx,
		0x2fb,
		"PA_CL_GB_VERT_DISC_ADJ"
	},
	{
		reg_blk_ctx,
		0x2fc,
		"PA_CL_GB_HORZ_CLIP_ADJ"
	},
	{
		reg_blk_ctx,
		0x2fd,
		"PA_CL_GB_HORZ_DISC_ADJ"
	},
	{
		reg_blk_ctx,
		0x2fe,
		"PA_SC_AA_SAMPLE_LOCS_PIXEL_X0Y0_0"
	},
	{
		reg_blk_ctx,
		0x302,
		"PA_SC_AA_SAMPLE_LOCS_PIXEL_X1Y0_0"
	},
	{
		reg_blk_ctx,
		0x306,
		"PA_SC_AA_SAMPLE_LOCS_PIXEL_X0Y1_0"
	},
	{
		reg_blk_ctx,
		0x30a,
		"PA_SC_AA_SAMPLE_LOCS_PIXEL_X1Y1_0"
	},
	{
		reg_blk_ctx,
		0x30e,
		"PA_SC_AA_MASK_X0Y0_X1Y0"
	},
	{
		reg_blk_ctx,
		0x30f,
		"PA_SC_AA_MASK_X0Y1_X1Y1"
	},
	{
		reg_blk_ctx,
		0x313,
		"PA_SC_CONSERVATIVE_RASTERIZATION_CNTL"
	},
	{
		reg_blk_ctx,
		0x318,
		"CB_COLOR0_BASE"
	},
	{
		reg_blk_ctx,
		0x319,
		"CB_COLOR0_PITCH"
	},
	{
		reg_blk_ctx,
		0x31a,
		"CB_COLOR0_SLICE"
	},
	{
		reg_blk_ctx,
		0x31b,
		"CB_COLOR0_VIEW"
	},
	{
		reg_blk_ctx,
		0x31c,
		"CB_COLOR0_INFO"
	},
	{
		reg_blk_ctx,
		0x31d,
		"CB_COLOR0_ATTRIB"
	},
	{
		reg_blk_ctx,
		0x31e,
		"CB_COLOR0_DCC_CONTROL"
	},
	{
		reg_blk_ctx,
		0x31f,
		"CB_COLOR0_CMASK"
	},
	{
		reg_blk_ctx,
		0x320,
		"CB_COLOR0_CMASK_SLICE"
	},
	{
		reg_blk_ctx,
		0x321,
		"CB_COLOR0_FMASK"
	},
	{
		reg_blk_ctx,
		0x322,
		"CB_COLOR0_FMASK_SLICE"
	},
	{
		reg_blk_ctx,
		0x32b,
		"CB_COLOR1_INFO"
	},
	{
		reg_blk_ctx,
		0x33a,
		"CB_COLOR2_INFO"
	},
	{
		reg_blk_ctx,
		0x349,
		"CB_COLOR3_INFO"
	},
	{
		reg_blk_ctx,
		0x358,
		"CB_COLOR4_INFO"
	},
	{
		reg_blk_ctx,
		0x367,
		"CB_COLOR5_INFO"
	},
	{
		reg_blk_ctx,
		0x376,
		"CB_COLOR6_INFO"
	},
	{
		reg_blk_ctx,
		0x385,
		"CB_COLOR7_INFO"
	},
}; 
#endif
