#ifndef APP_STATE_TYPES_H
#define APP_STATE_TYPES_H
/*
 * this is public domain without any warranties of any kind
 * Sylvain BERTRAND
 */
/* XXX: KEEP AN EYE ON ABBREVIATIONS, ALWAYS */
#include <stddef.h>
#include <xcb.h>
#include "app_core_types.h"
#include "nyanvk/types.h"
#include "nyanvk/syms_dev.h"
/* this is the default initial cpu rendering size */
#define APP_CPU_IMG_WIDTH 50
#define APP_CPU_IMG_HEIGHT 50
#define APP_WIN_WIDTH 100
#define APP_WIN_HEIGHT 100
/*----------------------------------------------------------------------------*/
#include "namespace/app_state_types.h"
/*----------------------------------------------------------------------------*/
constant {
	state_run = 1,
	state_quit = 2
};

constant {
	swpchn_imgs_n_max = 3
};

struct swpchn_t {
	void *vk;

	u32 imgs_n;
	void *imgs[swpchn_imgs_n_max];
};

struct phydev_t {
	void  *vk;
	u8 q_fam;
	bool is_discret_gpu;
	struct vk_surf_texel_mem_blk_conf_core_t
					selected_texel_mem_blk_conf_core;
	u32 mem_types_n;
	struct  vk_mem_type_t mem_types[VK_MEM_TYPES_N_MAX];
};

struct cpu_img_t {
	void *vk;
	struct vk_subrsrc_layout_t layout;
	void *dev_mem; /* TODO: we should use 1 big dev_mem chunk */
	void *data;
};

constant {
	sem_acquire_img_done = 0,
	sem_blit_done = 1,
	sems_n = 2,
};

struct dev_t {
	void *vk;
	struct phydev_t phydev;
	struct swpchn_t swpchn;
	void *q;
	void *cp;
	void *cbs[swpchn_imgs_n_max];
	void *sems[sems_n];
	struct app_cpu_img_t cpu_imgs[swpchn_imgs_n_max];
	VK_DEV_SYMS_FULL /* should cherry pick the syms we use */
};

struct surf_t {
	void *vk;
	struct dev_t dev;
};
/*============================================================================*/
/* tmp state */
constant {
	tmp_phydevs_n_max = 16,
	tmp_phydev_q_fams_n_max = 16,
	tmp_present_modes_n_max = 16
};

struct tmp_phydev_t {
	void *vk;
	u8 q_fams_n;
	struct vk_q_fam_props_t q_fams[tmp_phydev_q_fams_n_max];
	bool is_discret_gpu;
	struct vk_phydev_mem_props_t mem_props;
	bool q_fams_surf_support[tmp_phydev_q_fams_n_max];
};
/*----------------------------------------------------------------------------*/
#define CLEANUP
#include "namespace/app_state_types.h"
#undef CLEANUP
/*----------------------------------------------------------------------------*/
#endif /* APP_STATE_TYPES_H  */
