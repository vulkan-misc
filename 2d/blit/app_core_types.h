#ifndef APP_CORE_TYPES_H
#define APP_CORE_TYPES_H
/*
 * this is public domain without any warranties of any kind
 * Sylvain BERTRAND
 */
/* XXX: KEEP AN EYE ON ABBREVIATIONS, ALWAYS */
#define ARRAY_N(x) (sizeof(x) / sizeof(x[0]))
#include <stdbool.h>
#include <stdint.h>
#define u8 uint8_t
#define u16 uint16_t
#define s16 int16_t
#define u32 uint32_t
#define s32 int32_t
#define s32_max 0x7fffffff
#define u64 uint64_t
#define u64_max 0xffffffffffffffff
#define f32 float
#define loop for(;;)
#define constant enum
#endif /* APP_CORE_TYPES_H */
