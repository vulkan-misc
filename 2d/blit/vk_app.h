#ifndef VK_APP_H
#define VK_APP_H
/*
 * this is public domain without any warranties of any kind
 * Sylvain BERTRAND
 */
/*
 * this is the simplification and taylorization of vk api for the specific
 * uses of the app.
 */
#define vk_get_dev_q() \
app_surf.dev.dl_vk_get_dev_q(app_surf.dev.vk, app_surf.dev.phydev.q_fam, 0, &app_surf.dev.q)

#define vk_create_cp(info) \
r = app_surf.dev.dl_vk_create_cp(app_surf.dev.vk, info, 0, &app_surf.dev.cp)

#define vk_create_swpchn(info) \
r = app_surf.dev.dl_vk_create_swpchn(app_surf.dev.vk, info, 0, &app_surf.dev.swpchn.vk)

#define vk_get_swpchn_imgs() \
r = app_surf.dev.dl_vk_get_swpchn_imgs(app_surf.dev.vk, app_surf.dev.swpchn.vk, &app_surf.dev.swpchn.imgs_n, app_surf.dev.swpchn.imgs)

#define vk_create_img(info, img) \
r = app_surf.dev.dl_vk_create_img(app_surf.dev.vk, info, 0, img)

#define vk_get_img_mem_rqmts(...) \
r = app_surf.dev.dl_vk_get_img_mem_rqmts(app_surf.dev.vk,##__VA_ARGS__)

#define vk_alloc_mem(info, dev_mem) \
r = app_surf.dev.dl_vk_alloc_mem(app_surf.dev.vk, info, 0, dev_mem)

#define vk_bind_img_mem(infos) \
r = app_surf.dev.dl_vk_bind_img_mem(app_surf.dev.vk, app_surf.dev.swpchn.imgs_n, infos)

#define vk_map_mem(dev_mem, data) \
r = app_surf.dev.dl_vk_map_mem(app_surf.dev.vk, dev_mem, 0, vk_whole_sz, 0, data)

#define vk_alloc_cbs(info) \
r = app_surf.dev.dl_vk_alloc_cbs(app_surf.dev.vk, info, app_surf.dev.cbs)

#define vk_begin_cb(...) \
r = app_surf.dev.dl_vk_begin_cb(__VA_ARGS__)

#define vk_end_cb(...) \
r = app_surf.dev.dl_vk_end_cb(__VA_ARGS__)

#define vk_cmd_pl_barrier(cb, b) \
app_surf.dev.dl_vk_cmd_pl_barrier(cb, vk_pl_stage_top_of_pipe_bit, vk_pl_stage_top_of_pipe_bit, 0, 0, 0, 0, 0, 1, b)

#define vk_q_submit(info) \
r = app_surf.dev.dl_vk_q_submit(app_surf.dev.q, 1, info, 0)

#define vk_q_wait_idle() \
r = app_surf.dev.dl_vk_q_wait_idle(app_surf.dev.q)

#define vk_get_img_subrsrc_layout(...) \
app_surf.dev.dl_vk_get_img_subrsrc_layout(app_surf.dev.vk, ##__VA_ARGS__)

#define vk_acquire_next_img(...) \
r = app_surf.dev.dl_vk_acquire_next_img(app_surf.dev.vk,##__VA_ARGS__)

#define vk_reset_cb(cb) \
r = app_surf.dev.dl_vk_reset_cb(cb, 0)

#define vk_cmd_blit_img(cb, src_img, dst_img, region) \
app_surf.dev.dl_vk_cmd_blit_img(cb, src_img, vk_img_layout_general, dst_img, vk_img_layout_present, 1, region, 0)

#define vk_q_present(info) \
r = app_surf.dev.dl_vk_q_present(app_surf.dev.q, info)

#define vk_create_sem(info, sem) \
r = app_surf.dev.dl_vk_create_sem(app_surf.dev.vk, info, 0, sem)
/******************************************************************************/
/* cherry picked from nyanvk/syms_global.h */
#define VK_GLOBAL_SYMS \
	static void *(*dl_vk_get_instance_proc_addr)(void *instance, u8 *name); \
	static void *(*dl_vk_get_dev_proc_addr)(void *dev, u8 *name); \
	static s32 (*dl_vk_enumerate_instance_version)(u32 *version); \
	static s32 (*dl_vk_enumerate_instance_layer_props)( \
				u32 *props_n, \
				struct vk_layer_props_t *props); \
	static s32 (*dl_vk_enumerate_instance_ext_props)( \
				u8 *layer_name, \
				u32 *props_n, \
				struct vk_ext_props_t *props); \
	static s32 (*dl_vk_create_instance)( \
				struct vk_instance_create_info_t *info, \
				void *allocator, \
				void **instance); \
	static s32 (*dl_vk_enumerate_phydevs)( \
				void *instance, \
				u32 *phydevs_n, \
				void **phydevs); \
	static s32 (*dl_vk_enumerate_dev_ext_props)( \
				void *phydev, \
				u8 *layer_name, \
				u32 *props_n, \
				struct vk_ext_props_t *props); \
	static void (*dl_vk_get_phydev_props)( \
				void *phydev, \
				struct vk_phydev_props_t *props); \
	static s32 (*dl_vk_create_dev)( \
			void *phydev, \
			struct vk_dev_create_info_t *create_info, \
			void *allocator, \
			void **dev); \
	static void (*dl_vk_get_phydev_q_fam_props)( \
			void *phydev, \
			u32 *q_fam_props_n, \
			struct vk_q_fam_props_t *props); \
	static s32 (*dl_vk_create_xcb_surf)( \
				void *instance, \
				struct vk_xcb_surf_create_info_t *info, \
				void *allocator, \
				void **surf); \
	static s32 (*dl_vk_get_phydev_surf_support)( \
					void *phydev, \
					u32 q_fam, \
					void *surf, \
					u32 *supported); \
	static s32  (*dl_vk_get_phydev_surf_texel_mem_blk_confs)( \
				void *phydev, \
				struct vk_phydev_surf_info_t *info, \
				u32 *confs_n, \
				struct vk_surf_texel_mem_blk_conf_t *confs); \
	static void (*dl_vk_get_phydev_mem_props)( \
			void *phydev, \
			struct vk_phydev_mem_props_t *props); \
	static s32 (*dl_vk_get_phydev_surf_caps)( \
					void *phydev, \
					struct vk_phydev_surf_info_t *info, \
					struct vk_surf_caps_t *caps); \
	static s32 (*dl_vk_get_phydev_surf_present_modes)( \
					void *phydev, \
					void *surf, \
					u32 *modes_n, \
					u32 *modes);
/******************************************************************************/
#define vk_get_instance_proc_addr dl_vk_get_instance_proc_addr

#define vk_get_dev_proc_addr dl_vk_get_dev_proc_addr

#define vk_enumerate_instance_version \
r = dl_vk_enumerate_instance_version

#define vk_enumerate_instance_layer_props \
r = dl_vk_enumerate_instance_layer_props

#define vk_enumerate_instance_ext_props(...) \
r = dl_vk_enumerate_instance_ext_props(0,##__VA_ARGS__)

#define vk_create_instance(info) \
r = dl_vk_create_instance(info, 0, &app_instance)

#define vk_enumerate_phydevs(...) \
r = dl_vk_enumerate_phydevs(app_instance,##__VA_ARGS__)

#define vk_enumerate_dev_ext_props(phydev, props_n, props) \
r = dl_vk_enumerate_dev_ext_props(phydev, 0, props_n, props)

#define vk_get_phydev_props dl_vk_get_phydev_props

#define vk_create_dev(info) \
r = dl_vk_create_dev(app_surf.dev.phydev.vk, info, 0, &app_surf.dev.vk)

#define vk_get_phydev_q_fam_props dl_vk_get_phydev_q_fam_props

#define vk_create_xcb_surf(info) \
r = dl_vk_create_xcb_surf(app_instance, info, 0, &app_surf.vk)

#define vk_get_phydev_surf_support(phydev, q_fam, supported) \
r = dl_vk_get_phydev_surf_support(phydev, q_fam, app_surf.vk, supported)

#define vk_get_phydev_surf_texel_mem_blk_confs(info, ...) \
r = dl_vk_get_phydev_surf_texel_mem_blk_confs(app_surf.dev.phydev.vk, info, ##__VA_ARGS__)

#define vk_get_phydev_mem_props dl_vk_get_phydev_mem_props

#define vk_get_phydev_surf_caps(info, caps) \
r = dl_vk_get_phydev_surf_caps(app_surf.dev.phydev.vk, info, caps)

#define vk_get_phydev_surf_present_modes() \
r = dl_vk_get_phydev_surf_present_modes(app_surf.dev.phydev.vk, app_surf.vk, &app_tmp_present_modes_n, app_tmp_present_modes)
#endif
