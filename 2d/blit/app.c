#ifndef APP_C
#define APP_C
/*
 * this is public domain without any warranties of any kind
 * Sylvain BERTRAND
 */
/* XXX: KEEP AN EYE ON ABBREVIATIONS, ALWAYS */
/*
 * XXX vk abstraction is much more complex than real hardware, aka the cost of
 * most software generalisation/abstraction (and some hardware has lost its
 * way...). Better know some real hardware programming (for instance AMD
 * open gpu) and keep that in mind while dealing with vk.
 * since it's complex, have a "safe mode", kind of minimal use of vk. doing
 * fancy stuff above this "safe mode" must be validated by hardware vendors
 * then the user... or you literally walking on eggs.
 *
 * The vulkan API is, here, simplified and taylored for the app using the C
 * preprocessor. In other words, when there is no ambiguity in the context of
 * this code, vulkan API function parameters get simplified out in C
 * preprocessor macros.
 *
 * XXX: this is a "One Compilation Unit" source code with preprocessor
 * namespace support. this allow the project to grow to very large and keep the
 * global identifier space in check (= tradeoff). Each source file, *.h *.c,
 * should compile without errors.
 *
 * XXX: you may have to track the dependencies of vk objs that in order to be
 * able to deal with any of their brutal state changes:
 *   - a dev can be lost (i.e. power management evt or bad hardware)
 *   - a surf can be lost (i.e. power management evt or something went
 *     really bad)
 *   - a swpchn can become out-of-date. for instance the win system did resz
 *     the parent surf of the swpchn... if you did allow it to happen
 *     asynchronously.
 * here we choose that any of those evts will be fatal for simplicity.  for
 * instance, if you choose to support the swpchn out-of-date state, if your
 * rendering state was too much "pre-configured", you would have to
 * "re-pre-configure" everything... or you should drop "pre-configuring" and
 * program everything *again* for each swpchn img you draw.
 * 
 * display programing is demonstrated "officially" in khronos vk cube.c and
 * there is a tutorial slide "1-Vulkan-Tutorial.pdf" (just abstract away c++
 * cr*p) which is "the programming manual" on top of specs. this code is
 * different:
 *   - only 1 "main" synchronous loop
 *   - only xcb wsi. xcb is a client library on top of the x11 protocol.
 *     we know wayland ("x12") should be added.
 *   - dynamic loading of xcb.
 *   - no need of vk headers (using directly the ABI with custom headers).
 *
 * WARNING: vk core q fam props discovery is not used for the discovery of
 * q fams able to support disp, aka wsi. This is the case because disp
 * handling (wsi), is done as an ext and is not core (vk can be use without
 * a wsi).
 *
 * a phydev must have a q with gfx and compute. additionally, any q with gfx or
 * compute does implicitely support transfer. basically, it is possible to have
 * qs with only transfer support, and we are guaranteed to have a q with gfx
 * and compute and transfer support. Keep in mind that many vk resources must
 * pay the cost of transfering from 1 q fam to another q fam: then think twice
 * on how you want to spread your work load on the q fams.
 *
 * for proper keyboard support, joypad way or/and text input way, read the
 * included KEYBOARD file. here, since we use basic layout independent standard
 * keys, the x11 core keyboard protocol is fairly enough.
 *
 * TODO: use as less as possible device memory object, namely try to allocate
 * one big chunk and manage alignment constraint ourself. vk api does provide
 * a way to query for the memory alignment constraints.
 */
#include <stdlib.h>
#include <string.h>
#include <xcb.h>
#include "app_core_types.h"
#include "nyanvk/consts.h"
#include "nyanvk/types.h"
#include "vk_app.h"
#include "app_state_types.h"
#include "log.h"
#include "vk_syms.c"
#include "app_state.c"
#include "xcb.c"
/*---------------------------------------------------------------------------*/
#include "namespace/app.c"
#include "namespace/vk_syms.c"
#include "namespace/app_state_types.h"
#include "namespace/app_state.c"
/*---------------------------------------------------------------------------*/
#define VK_FATAL(fmt, ...) \
if (r < 0) {\
	LOG(fmt, ##__VA_ARGS__);\
	exit(1);\
}

#define FATAL(fmt, ...) \
{\
	LOG(fmt, ##__VA_ARGS__);\
	exit(1);\
}
/* the phydev q fam selected */
static void dev_create(void)
{
	struct vk_dev_create_info_t info;
	struct vk_dev_q_create_info_t q_info;
	float q_prio;
	static u8 *exts[] = {
		/* 1.1 promoted */
		"VK_KHR_bind_memory2",
		/* 1.1 promoted */
		"VK_KHR_get_memory_requirements2",
		"VK_KHR_swapchain"};

	s32 r;

	memset(&info, 0, sizeof(info));
	memset(&q_info, 0, sizeof(q_info));
	/*--------------------------------------------------------------------*/
	q_info.type = vk_struct_type_dev_q_create_info;
	q_info.q_fam = surf_g.dev.phydev.q_fam;
	q_info.qs_n = 1;
	q_info.q_prios = &q_prio;
	q_prio = 1.0f;
	/*--------------------------------------------------------------------*/
	info.type = vk_struct_type_dev_create_info;
	info.q_create_infos_n = 1;
	info.q_create_infos = &q_info;
	info.enabled_exts_n = ARRAY_N(exts);
	info.enabled_ext_names = exts;
	vk_create_dev(&info);
	VK_FATAL("0:MAIN:FATAL:%d:physical device:%p:unable to create a vulkan device\n", r, surf_g.dev.phydev.vk)
	LOG("0:MAIN:physical device:%p:vulkan device created with one proper queue:%p\n", surf_g.dev.phydev.vk, surf_g.dev.vk);
}

static void instance_create(void)
{
	s32 r;
	struct vk_instance_create_info_t info;
	static u8 *exts[] = {
		/*
		 * XXX: not 1.1 promoted, should not use it, but it is fixing
		 * some non-consistency from 1.0
		 */
		"VK_KHR_get_surface_capabilities2",
		/* 1.1 promoted */
		"VK_KHR_get_physical_device_properties2",
		"VK_KHR_xcb_surface",
		"VK_KHR_surface"};
	u32 i;

	i = 0;
	loop {
		if (i == ARRAY_N(exts))
			break;
		LOG("0:MAIN:will use vulkan instance_g extension %s\n", exts[i]);
		++i;
	}
	memset(&info, 0, sizeof(info));
	info.type = vk_struct_type_instance_create_info;
	info.enabled_exts_n = ARRAY_N(exts);
	info.enabled_ext_names = exts;
	vk_create_instance(&info);
	VK_FATAL("0:MAIN:FATAL:%d:unable to create a vulkan instance_g\n", r)
	LOG("0:MAIN:vulkan instance_g handle %p\n", instance_g);
}

/* in theory, this could change on the fly */
static void instance_exts_dump(void)
{
#define EXTS_N_MAX 512
	struct vk_ext_props_t exts[EXTS_N_MAX];
	u32 n;
	s32 r;

	memset(exts, 0, sizeof(exts));
	n = EXTS_N_MAX;
	vk_enumerate_instance_ext_props(&n, exts);
	if (r != vk_success && r != vk_incomplete) {
		LOG("0:MAIN:ERROR:%d:unable to enumerate instance_g extension(s)\n", r);
		return;
	} 
	if (r == vk_incomplete) {
		LOG("0:MAIN:ERROR:too many extensions (%u/%u), dumping disabled", n, EXTS_N_MAX);
		return;
	}
	/* vk_success */
	LOG("0:MAIN:have %u instance_g extension(s)\n", n);
	loop {
		if (n == 0)
			break;
		LOG("0:MAIN:instance_g extension:name=%s:specification version=%u\n", exts[n - 1].name, exts[n - 1].spec_version);
		n--;
	}
#undef EXTS_N_MAX
}

/* in theory, this could change on the fly */
static void instance_layers_dump(void)
{
#define LAYERS_N_MAX 32
	struct vk_layer_props_t layers[LAYERS_N_MAX];
	u32 n;
	s32 r;

	memset(layers, 0, sizeof(layers));
	n = LAYERS_N_MAX;
	vk_enumerate_instance_layer_props(&n, layers);
	if (r != vk_success && r != vk_incomplete) {
		LOG("0:MAIN:ERROR:%d:unable to enumerate instance_g layer(s)\n", r);
		return;
	}
	if (r == vk_incomplete) {
		LOG("0:MAIN:ERROR:too many layers (%u/%u), dumping disabled", n, LAYERS_N_MAX);
		return;
	}
	/* vk_success */
	LOG("0:MAIN:have %u instance_g layer(s)\n", n);
	loop {
		if (n == 0)
			break;
		LOG("0:MAIN:instance_g layer:%u:name=%s:specification version=%u:implementation version=%u:description=%s\n", n, layers[n].name, layers[n].spec_version, layers[n].implementation_version, layers[n].desc);
		n--;
	}
#undef LAYERS_N_MAX
}

static void tmp_phydevs_get(void)
{
	void *phydevs[tmp_phydevs_n_max];
	u32 n;
	s32 r;

	memset(phydevs, 0, sizeof(phydevs));
	n = tmp_phydevs_n_max;
	vk_enumerate_phydevs(&n, phydevs);
	if (r != vk_success && r != vk_incomplete)
		FATAL("0:MAIN:FATAL:%ld:unable to enumerate physical devices\n",r)
	if (r == vk_incomplete)
		FATAL("0:MAIN:FATAL:too many vulkan physical devices %u/%u for our temporary storage\n", n, tmp_phydevs_n_max)
	/* vk_success */
	LOG("0:MAIN:detected %u physical devices\n", n);
	if (n == 0)
		FATAL("0:MAIN:no vulkan physical devices, exiting\n")
	tmp_phydevs_n_g = n;
	memset(tmp_phydevs_g, 0, sizeof(tmp_phydevs_g));
	n = 0;	
	loop {
		if (n == tmp_phydevs_n_g)
			break;
		tmp_phydevs_g[n].vk = phydevs[n];
		++n;
	};
}

static void phydev_exts_dump(void *phydev)
{
#define EXTS_N_MAX 512
	struct vk_ext_props_t exts[EXTS_N_MAX];
	u32 n;
	s32 r;

	memset(exts, 0, sizeof(exts));
	n = EXTS_N_MAX;
	vk_enumerate_dev_ext_props(phydev, &n, exts);
	if (r != vk_success && r != vk_incomplete) {
		LOG("0:MAIN:ERROR:physical device:%p:%d:unable to enumerate device extension(s)\n", phydev, r);
		return;
	} 
	if (r == vk_incomplete) {
		LOG("0:MAIN:ERROR:physical device:%p:too many extensions (%u/%u), dumping disabled", phydev, n, EXTS_N_MAX);
		return;
	}
	/* vk_success */
	LOG("0:MAIN:physical device:%p:have %u device extension(s)\n", phydev, n);
	loop {
		if (n == 0)
			break;
		LOG("0:MAIN:physical device:%p:device extension:name=%s:specification version=%u\n", phydev, exts[n - 1].name, exts[n - 1].spec_version);
		n--;
	}
#undef EXTS_N_MAX
}

static void tmp_phydevs_exts_dump(void)
{
	u8 i;

	i = 0;
	loop {
		if (i == tmp_phydevs_n_g)
			break;
		phydev_exts_dump(tmp_phydevs_g[i].vk);
		++i;
	}
}

static u8 *dev_type_str(u32 type)
{
	switch (type) {
	case vk_phydev_type_other:
		return "other";
	case vk_phydev_type_integrated_gpu:
		return "integrated gpu";
	case vk_phydev_type_discrete_gpu:
		return "discrete gpu";
	case vk_phydev_type_virtual_gpu:
		return "virtual gpu";
	case vk_phydev_type_cpu:
		return "cpu";
	default:
		return "UNKNOWN";
	}
}

static u8 *uuid_str(u8 *uuid)
{
	static u8 uuid_str[VK_UUID_SZ * 2 + 1];
	u8 i;

	memset(uuid_str, 0, sizeof(uuid_str));
	i = 0;
	loop {
		if (i == VK_UUID_SZ)
			break;
		/* XXX: always write a terminating  0, truncated or not */
		snprintf(uuid_str + i * 2, 3, "%02x", uuid[i]);
		++i;
	}	
	return uuid_str;
}

static void tmp_phydevs_props_dump(void)
{
	u32 i;

	i = 0;
	loop {
		struct vk_phydev_props_t props;
		struct tmp_phydev_t *p;

		if (i == tmp_phydevs_n_g)
			break;
		p = &tmp_phydevs_g[i];
		memset(&props, 0, sizeof(props));
		props.type = vk_struct_type_phydev_props;
		vk_get_phydev_props(p->vk, &props);
		LOG("0:MAIN:physical device:%p:properties:api version=%#x=%u.%u.%u\n", p->vk, props.core.api_version, VK_VERSION_MAJOR(props.core.api_version), VK_VERSION_MINOR(props.core.api_version), VK_VERSION_PATCH(props.core.api_version));
		LOG("0:MAIN:physical device:%p:properties:driver version=%#x=%u.%u.%u\n", p->vk, props.core.driver_version, VK_VERSION_MAJOR(props.core.driver_version), VK_VERSION_MINOR(props.core.driver_version), VK_VERSION_PATCH(props.core.driver_version));
		LOG("0:MAIN:physical device:%p:properties:vendor id=%#x\n", p->vk, props.core.vendor_id);
		LOG("0:MAIN:physical device:%p:properties:device id=%#x\n", p->vk, props.core.dev_id);
		LOG("0:MAIN:physical device:%p:properties:type=%s\n", p->vk, dev_type_str(props.core.dev_type));
		if (props.core.dev_type == vk_phydev_type_discrete_gpu)
			p->is_discret_gpu = true;
		else
			p->is_discret_gpu = false;
		LOG("0:MAIN:physical device:%p:properties:name=%s\n", p->vk, props.core.name);
		LOG("0:MAIN:physical device:%p:properties:pipeline cache uuid=%s\n", p->vk, uuid_str(props.core.pl_cache_uuid));
		/* display the limits and sparse props at log level 1, if needed */
		++i;
	}
}

static void tmp_phydev_q_fams_get(struct tmp_phydev_t *p)
{
	u8 i;
	u32 n;

	n = 0;
	vk_get_phydev_q_fam_props(p->vk, &n, 0);
	if (n > tmp_phydev_q_fams_n_max)
		FATAL("0:MAIN:FATAL:physical device:%p:too many queue families %u/%u\n", p->vk, n, tmp_phydev_q_fams_n_max)
	memset(p->q_fams, 0, sizeof(p->q_fams));
	i = 0;
	loop {
		if (i == tmp_phydev_q_fams_n_max)
			break;
		p->q_fams[i].type = vk_struct_type_q_fam_props;
		++i;
	}
	vk_get_phydev_q_fam_props(p->vk, &n, p->q_fams);
	p->q_fams_n = n;
	LOG("0:MAIN:physical device:%p:have %u queue families\n", p->vk, p->q_fams_n);
}

static void tmp_phydevs_q_fams_get(void)
{
	u8 i;

	i = 0;
	loop {
		if (i == tmp_phydevs_n_g)
			break;
		tmp_phydev_q_fams_get(&tmp_phydevs_g[i]);
		++i;
	}
}

static void tmp_phydev_q_fams_dump(struct tmp_phydev_t *p)
{
	u8 i;

	i = 0;
	loop {
		if (i == p->q_fams_n)
			break;
		if ((p->q_fams[i].core.flags & vk_q_gfx_bit) != 0)
			LOG("0:MAIN:physical device:%p:queue family:%u:flags:graphics\n", p->vk, i);
		if ((p->q_fams[i].core.flags & vk_q_compute_bit) != 0)
			LOG("0:MAIN:physical device:%p:queue family:%u:flags:compute\n", p->vk, i);
		if ((p->q_fams[i].core.flags & vk_q_transfer_bit) != 0)
			LOG("0:MAIN:physical device:%p:queue family:%u:flags:transfer\n", p->vk, i);
		if ((p->q_fams[i].core.flags & vk_q_sparse_binding_bit) != 0)
			LOG("0:MAIN:physical device:%p:queue family:%u:flags:sparse binding\n", p->vk, i);
		if ((p->q_fams[i].core.flags & vk_q_protected_bit) != 0)
			LOG("0:MAIN:physical device:%p:queue family:%u:flags:protected\n", p->vk, i);
		LOG("0:MAIN:physical device:%p:queue family:%u:%u queues\n", p->vk, i, p->q_fams[i].core.qs_n);
		LOG("0:MAIN:physical device:%p:queue family:%u:%u bits timestamps\n", p->vk, i, p->q_fams[i].core.timestamp_valid_bits);
		LOG("0:MAIN:physical device:%p:queue family:%u:(width=%u,height=%u,depth=%u) minimum image transfer granularity\n", p->vk, i, p->q_fams[i].core.min_img_transfer_granularity.width, p->q_fams[i].core.min_img_transfer_granularity.height, p->q_fams[i].core.min_img_transfer_granularity.depth);
		++i;
	}
}

static void cp_create(void)
{
	s32 r;
	struct vk_cp_create_info_t info;

	memset(&info, 0, sizeof(info));
	info.type = vk_struct_type_cp_create_info;
	info.flags = vk_cp_create_reset_cb_bit;
	info.q_fam = surf_g.dev.phydev.q_fam;
	vk_create_cp(&info);
	VK_FATAL("0:MAIN:FATAL:%d:unable create the commmand pool\n", r)
	LOG("0:MAIN:device:%p:queue family:%u:created command pool %p\n", surf_g.dev.vk, surf_g.dev.phydev.q_fam, surf_g.dev.cp);
}

static void tmp_phydevs_q_fams_dump(void)
{
	u8 i;

	i = 0;
	loop {
		if (i == tmp_phydevs_n_g)
			break;
		tmp_phydev_q_fams_dump(&tmp_phydevs_g[i]);
		++i;
	}
}

static void q_get(void)
{
	LOG("0:MAIN:device:%p:getting queue:family=%u queue=0\n", surf_g.dev.vk, surf_g.dev.phydev.q_fam);
	vk_get_dev_q();
	LOG("0:MAIN:device:%p:got queue:%p\n", surf_g.dev.vk, surf_g.dev.q);
}

static void check_vk_version(void)
{
	u32 api_version;
	s32 r;

	vk_enumerate_instance_version(&api_version);
	if (r != vk_success)
		FATAL("0:MAIN:FATAL:%d:unable to enumerate instance_g version\n", r)
	LOG("0:MAIN:vulkan instance_g version %#x = %u.%u.%u\n", api_version, VK_VERSION_MAJOR(api_version), VK_VERSION_MINOR(api_version), VK_VERSION_PATCH(api_version));
	if (VK_VERSION_MAJOR(api_version) == 1
					&& VK_VERSION_MINOR(api_version) == 0)
		FATAL("0:MAIN:FATAL:vulkan instance_g version too old\n")
}
/*
 * the major obj to use in vk abstraction of gfx hardware is the q. In this
 * abstraction, many core objs like bufs/imgs are "own" by a specific q, and
 * transfer of such ownership to other qs can be expensive. we know it's not
 * really the case on AMD hardware, but if vk abstraction insists on this, it
 * probably means it is important on some hardware of other vendors.
 */
static void tmp_phydevs_q_fams_surf_support_get(void)
{
	u8 i;

	i = 0;
	loop {
		struct tmp_phydev_t *p;
		u8 j;

		if (i == tmp_phydevs_n_g)
			break;
		p = &tmp_phydevs_g[i];
		j = 0;
		loop {
			s32 r;
			u32 supported;

			if (j == p->q_fams_n)
				break;
			supported = vk_false;
			vk_get_phydev_surf_support(p->vk, j, &supported);
			VK_FATAL("0:MAIN:FATAL:%d:physical device:%p:queue family:%u:surface:%p:unable to query queue family wsi/(image presentation to our surface) support\n", r, p->vk, j, surf_g.vk)
			if (supported == vk_true) {
				LOG("0:MAIN:physical device:%p:queue family:%u:surface:%p:does support wsi/(image presentation to our surface) \n", p->vk, j, surf_g.vk);
				p->q_fams_surf_support[j] = true;
			} else {
				LOG("0:MAIN:physical device:%p:queue family:%u:surface:%p:does not support wsi/(image presentation to our surface)\n", p->vk, j, surf_g.vk);
				p->q_fams_surf_support[j] = false; 
			}
			++j;
		}
		++i;
	}
}

static void tmp_selected_phydev_cherry_pick(u8 i)
{
	struct tmp_phydev_t *p;

	p = &tmp_phydevs_g[i];
	surf_g.dev.phydev.vk = p->vk;
	surf_g.dev.phydev.is_discret_gpu = p->is_discret_gpu;
	surf_g.dev.phydev.mem_types_n = p->mem_props.core.mem_types_n;
	memcpy(surf_g.dev.phydev.mem_types, p->mem_props.core.mem_types,
					sizeof(surf_g.dev.phydev.mem_types));
}

/*
 * we ask qs of phydevs which one is able to present imgs to the
 * external pe surf_g. Additionally we require this q to support gfx. we
 * select basically the first q from the first phydev fitting what we are
 * looking for.
 */
static void tmp_phydev_and_q_fam_select(void)
{
	u8 i;
		
	i = 0;
	loop {
		u8 j;
		struct tmp_phydev_t *p;

		if (i == tmp_phydevs_n_g)
			break;
		p = &tmp_phydevs_g[i];
		j = 0;
		loop {
			if (j == p->q_fams_n)
				break;
			/*
			 * we are looking for a q fam with:
			 *  - img presentation to our surf_g
			 *  - gfx
			 *  - transfer (implicit with gfx)
			 */
			if (p->q_fams_surf_support[j]
				&& (p->q_fams[j].core.flags & vk_q_gfx_bit)
									!= 0) {
				surf_g.dev.phydev.q_fam = j;
				tmp_selected_phydev_cherry_pick(i);
				LOG("0:MAIN:physical device %p selected for (wsi/image presentation to our surface %p) using its queue family %u\n", surf_g.dev.phydev.vk, surf_g.vk, surf_g.dev.phydev.q_fam);
				return;
			}
			++j;
		}
		++i;
	}
}

/*
 * XXX: the surf_g is an obj at the instance_g lvl, NOT THE [PHYSICAL]
 * DEV LVL.
 */
static void surf_create(void)
{
	struct vk_xcb_surf_create_info_t xcb_info;
	s32 r;

	memset(&surf_g, 0, sizeof(surf_g));
	memset(&xcb_info, 0, sizeof(xcb_info));
	xcb_info.type = vk_struct_type_xcb_surf_create_info;
	xcb_info.c = app_xcb.c;
	xcb_info.win = app_xcb.win_id;
	vk_create_xcb_surf(&xcb_info);
	VK_FATAL("0:MAIN:FATAL:%d:xcb:%s:screen:%d:root window id:%#x:window id:%#x:unable to create a vulkan surface from this x11 window\n", r, app_xcb.disp_env, app_xcb.scr_idx, app_xcb.scr->root, app_xcb.win_id)
	LOG("0:MAIN:xcb:'%s':screen:%d:root window id:%#x:window id:%#x:created vk_surface=%p\n", app_xcb.disp_env, app_xcb.scr_idx, app_xcb.scr->root, app_xcb.win_id, surf_g.vk);
}

static void texel_mem_blk_confs_dump(u32 confs_n,
				struct vk_surf_texel_mem_blk_conf_t *confs)
{
	u32 i;

	i = 0;
	loop {
		if (i == confs_n)
			break;
		LOG("0:MAIN:physical device:%p:surface:%p:texel memory block configuration:format=%u color_space=%u\n", surf_g.dev.phydev.vk, surf_g.vk, confs[i].core.fmt, confs[i].core.color_space);
		++i;
	}
}

/*
 * we only know this phydev/q is "able to present imgs" to the external
 * pe surf_g. Here we choose the conf of textel blk
 */
#define CONFS_N_MAX 1024
static void texel_mem_blk_conf_select(void)
{
	struct vk_phydev_surf_info_t info;
	struct vk_surf_texel_mem_blk_conf_t confs[CONFS_N_MAX];
	struct vk_surf_texel_mem_blk_conf_core_t *cc;
	s32 r;
	u32 confs_n;
	u32 i;

	memset(&info, 0, sizeof(info));
	info.type = vk_struct_type_phydev_surf_info;
	info.surf = surf_g.vk;
	vk_get_phydev_surf_texel_mem_blk_confs(&info, &confs_n, 0);
	VK_FATAL("0:MAIN:FATAL:%d:physical device:%p:surface:%p:unable get the count of valid surface texel memory block configurations\n", r, surf_g.dev.phydev.vk, surf_g.vk)
	if (confs_n > CONFS_N_MAX)
		FATAL("0:MAIN:FATAL:physical device:%p:surface:%p:too many surface texel memory block configurations %u/%u\n", surf_g.dev.phydev.vk, surf_g.vk, confs_n, CONFS_N_MAX)

	memset(confs, 0, sizeof(confs[0]) * confs_n);
	i = 0;
	loop {
		if (i == confs_n)
			break;
		confs[i].type = vk_struct_type_surf_texel_mem_blk_conf;
		++i;
	}	
	vk_get_phydev_surf_texel_mem_blk_confs(&info, &confs_n, confs);
	VK_FATAL("0:MAIN:FATAL:%d:physical device:%p:surface:%p:unable get the valid surface texel memory block configurations\n", r, surf_g.dev.phydev.vk, surf_g.vk)
	if (confs_n == 0)
		FATAL("0:MAIN:FATAL:physical device:%p:surface:%p:no valid surface texel memory block configuration\n", surf_g.dev.phydev.vk, surf_g.vk)
	texel_mem_blk_confs_dump(confs_n, confs);

	cc = &surf_g.dev.phydev.selected_texel_mem_blk_conf_core;	
	if ((confs_n == 1) && (confs[0].core.fmt
					== vk_texel_mem_blk_fmt_undefined)) {
		/* this means the dev let us choose our the fmt */
		cc->fmt = vk_texel_mem_blk_fmt_b8g8r8a8_srgb;
		LOG("0:MAIN:physical device:%p:surface:%p:using our surface texel memory block format %u\n", surf_g.dev.phydev.vk, surf_g.vk, cc->fmt);
		cc->color_space = vk_color_space_srgb_nonlinear;
		LOG("0:MAIN:physical device:%p:surface:%p:using prefered surface texel memory block color space %u\n", surf_g.dev.phydev.vk, surf_g.vk, cc->color_space);
	} else {
		/* the first valid fmt is the prefered fmt */
		surf_g.dev.phydev.selected_texel_mem_blk_conf_core.fmt =
							confs[0].core.fmt;
		LOG("0:MAIN:physical device:%p:surface:%p:using prefered surface texel memory block format %u\n", surf_g.dev.phydev.vk, surf_g.vk, surf_g.dev.phydev.selected_texel_mem_blk_conf_core.fmt);
		cc->color_space = confs[0].core.color_space;
		LOG("0:MAIN:physical device:%p:surface:%p:using prefered surface texel memory block color space %u\n", surf_g.dev.phydev.vk, surf_g.vk, cc->color_space);
	}
}

static void tmp_phydev_mem_props_get(struct tmp_phydev_t *p)
{
	memset(&p->mem_props, 0, sizeof(p->mem_props));
	p->mem_props.type = vk_struct_type_phydev_mem_props;
	vk_get_phydev_mem_props(p->vk, &p->mem_props);
}

static void tmp_phydevs_mem_props_get(void)
{
	u8 i;

	i = 0;
	loop {
		if (i == tmp_phydevs_n_g)
			break;
		tmp_phydev_mem_props_get(&tmp_phydevs_g[i]);
		++i;
	}
}

static void phydev_mem_heap_dump(void *phydev, u8 i,
						struct vk_mem_heap_t *heap)
{
	LOG("0:MAIN:physical device:%p:memory heap:%u:size:%u bytes\n", phydev, i, heap->sz);
	LOG("0:MAIN:physical device:%p:memory heap:%u:flags:%#08x\n", phydev, i, heap->flags);
	if ((heap->flags & vk_mem_heap_dev_local_bit) != 0)
		LOG("0:MAIN:physical device:%p:memory heap:%u:device local\n", phydev, i);
	if ((heap->flags & vk_mem_heap_multi_instance_bit) != 0)
		LOG("0:MAIN:physical device:%p:memory type:%u:multi instance_g\n", phydev, i);
}

static void phydev_mem_type_dump(void *phydev, u8 i,
						struct vk_mem_type_t *type)
{
	LOG("0:MAIN:physical device:%p:memory type:%u:heap:%u\n", phydev, i, type->heap);
	LOG("0:MAIN:physical device:%p:memory type:%u:flags:%#08x\n", phydev, i, type->prop_flags);
	if ((type->prop_flags & vk_mem_prop_dev_local_bit) != 0)
		LOG("0:MAIN:physical device:%p:memory type:%u:device local\n", phydev, i);
	if ((type->prop_flags & vk_mem_prop_host_visible_bit) != 0)
		LOG("0:MAIN:physical device:%p:memory type:%u:host visible\n", phydev, i);
	if ((type->prop_flags & vk_mem_prop_host_cached_bit) != 0)
		LOG("0:MAIN:physical device:%p:memory type:%u:host cached\n", phydev, i);
}

static void tmp_phydev_mem_types_dump(struct tmp_phydev_t *p)
{
	u8 i;

	LOG("0:MAIN:physical device:%p:%u memory types\n", p->vk, p->mem_props.core.mem_types_n);
	i = 0;
	loop {
		if (i == p->mem_props.core.mem_types_n)
			break;
		phydev_mem_type_dump(p->vk, i,
					&p->mem_props.core.mem_types[i]);
		++i;
	}
}

static void tmp_phydev_mem_heaps_dump(struct tmp_phydev_t *p)
{
	u8 i;

	LOG("0:MAIN:physical device:%p:%u memory heaps\n", p->vk, p->mem_props.core.mem_heaps_n);
	i = 0;
	loop {
		if (i == p->mem_props.core.mem_heaps_n)
			break;
		phydev_mem_heap_dump(p->vk, i,
					&p->mem_props.core.mem_heaps[i]);
		++i;
	}

}

static void tmp_phydev_mem_props_dump(struct tmp_phydev_t *p)
{
	tmp_phydev_mem_types_dump(p);
	tmp_phydev_mem_heaps_dump(p);
}

static void tmp_phydevs_mem_props_dump(void)
{
	u8 i;

	i = 0;
	loop {
		if (i == tmp_phydevs_n_g)
			break;
		tmp_phydev_mem_props_dump(&tmp_phydevs_g[i]);
		++i;
	}
}

static void tmp_surf_caps_get(void)
{
	s32 r;
	struct vk_phydev_surf_info_t info;

	memset(&info, 0, sizeof(info));
	info.type = vk_struct_type_phydev_surf_info;
	info.surf = surf_g.vk;
	memset(&tmp_surf_caps_g, 0, sizeof(tmp_surf_caps_g));
	tmp_surf_caps_g.type = vk_struct_type_surf_caps;
	vk_get_phydev_surf_caps(&info, &tmp_surf_caps_g);
	VK_FATAL("0:MAIN:FATAL:%d:physical device:%p:surface:%p:unable to get our surface capabilities in the context of the selected physical device\n", r, surf_g.dev.phydev.vk, surf_g.vk)
	/* we have room for a maximum of 3 images per swapchain */
	if (tmp_surf_caps_g.core.imgs_n_min > swpchn_imgs_n_max)
		FATAL("0:MAIN:FATAL:physical device:%p:surface:%p:we have room for %u images per swapchain, but this swapchain requires a minimum of %u images\n", surf_g.dev.phydev.vk, surf_g.vk, swpchn_imgs_n_max, tmp_surf_caps_g.core.imgs_n_min)
}

static void tmp_surf_caps_dump(void)
{
	LOG("0:MAIN:physical device:%p:surface:%p:imgs_n_min=%u\n", surf_g.dev.phydev.vk, surf_g.vk, app_tmp_surf_caps.core.imgs_n_min);
	LOG("0:MAIN:physical device:%p:surface:%p:imgs_n_max=%u\n", surf_g.dev.phydev.vk, surf_g.vk, tmp_surf_caps_g.core.imgs_n_max);
	LOG("0:MAIN:physical device:%p:surface:%p:current extent=(width=%u, height=%u)\n", surf_g.dev.phydev.vk, surf_g.vk, tmp_surf_caps_g.core.current_extent.width, tmp_surf_caps_g.core.current_extent.height);
	LOG("0:MAIN:physical device:%p:surface:%p:minimal extent=(width=%u, height=%u)\n", surf_g.dev.phydev.vk, surf_g.vk, tmp_surf_caps_g.core.img_extent_min.width, tmp_surf_caps_g.core.img_extent_min.height);
	LOG("0:MAIN:physical device:%p:surface:%p:maximal extent=(width=%u, height=%u)\n", surf_g.dev.phydev.vk, surf_g.vk, tmp_surf_caps_g.core.img_extent_max.width, tmp_surf_caps_g.core.img_extent_max.height);
	LOG("0:MAIN:physical device:%p:surface:%p:img_array_layers_n_max=%u\n", surf_g.dev.phydev.vk, surf_g.vk, tmp_surf_caps_g.core.img_array_layers_n_max);
	LOG("0:MAIN:physical device:%p:surface:%p:supported_transforms=%#08x\n", surf_g.dev.phydev.vk, surf_g.vk, tmp_surf_caps_g.core.supported_transforms);
	LOG("0:MAIN:physical device:%p:surface:%p:current_transform=%#08x\n", surf_g.dev.phydev.vk, surf_g.vk, tmp_surf_caps_g.core.current_transform);
	LOG("0:MAIN:physical device:%p:surface:%p:supported_composite_alpha=%#08x\n", surf_g.dev.phydev.vk, surf_g.vk, tmp_surf_caps_g.core.supported_composite_alpha);
	LOG("0:MAIN:physical device:%p:surface:%p:supported_img_usage_flags=%#08x\n", surf_g.dev.phydev.vk, surf_g.vk, tmp_surf_caps_g.core.supported_img_usage_flags);
}

static void swpchn_imgs_get(void)
{
	s32 r;

	surf_g.dev.swpchn.imgs_n = swpchn_imgs_n_max;
	vk_get_swpchn_imgs();
	VK_FATAL("0:MAIN:FATAL:%d:device:%p:surface:%p:swapchain:%p:unable to get the swapchain images\n", r, surf_g.dev.vk, surf_g.vk, surf_g.dev.swpchn.vk)
	LOG("0:MAIN:device:%p:surface:%p:swapchain:%p:got %u swapchain images\n", surf_g.dev.vk, surf_g.vk, surf_g.dev.swpchn.vk, surf_g.dev.swpchn.imgs_n);
}

static void swpchn_init(void)
{
	struct vk_swpchn_create_info_t info;
	struct phydev_t *p;
	s32 r;

	memset(&info, 0, sizeof(info));
	p = &surf_g.dev.phydev;
	info.type = vk_struct_type_swpchn_create_info;
	info.surf = surf_g.vk;
	info.imgs_n_min = tmp_surf_caps_g.core.imgs_n_min;
	info.img_texel_mem_blk_fmt = p->selected_texel_mem_blk_conf_core.fmt;
	info.img_color_space = p->selected_texel_mem_blk_conf_core.color_space;
	memcpy(&info.img_extent, &tmp_surf_caps_g.core.current_extent,
						sizeof(info.img_extent));
	info.img_layers_n = 1;
	info.img_usage = vk_img_usage_color_attachment_bit
					| vk_img_usage_transfer_dst_bit;
	info.img_sharing_mode = vk_sharing_mode_exclusive;
	info.pre_transform = vk_surf_transform_identity_bit;
	info.composite_alpha = vk_composite_alpha_opaque_bit;
	info.present_mode = vk_present_mode_fifo;
	info.clipped = vk_true;
	vk_create_swpchn(&info);
	VK_FATAL("0:MAIN:FATAL:%d:device:%p:surface:%p:unable to create the initial swapchain\n", r, surf_g.dev.vk, surf_g.vk)
	LOG("0:MAIN:device:%p:surface:%p:swapchain created %p\n", surf_g.dev.vk, surf_g.vk, surf_g.dev.swpchn.vk);
}

static void tmp_present_modes_get(void)
{
	s32 r;

	tmp_present_modes_n_g = tmp_present_modes_n_max;
	vk_get_phydev_surf_present_modes();
	VK_FATAL("0:MAIN:FATAL:%d:physical device:%p:surface:%p:unable to get the physical device present mode for our surface\n", r, surf_g.dev.phydev.vk, surf_g.vk)
}

static u8 *present_mode_to_str(u32 mode)
{
	switch (mode) {
	case vk_present_mode_immediate:
		return "immediate";
	case vk_present_mode_mailbox:
		return "mailbox";
	case vk_present_mode_fifo:
		return "fifo";
	case vk_present_mode_fifo_relaxed:
		return "fifo relaxed";
	default:
		return "unknown";
	}
}

static void tmp_present_modes_dump(void)
{
	u8 i;

	i = 0;
	LOG("0:MAIN:physical device:%p:surface:%p:%u present modes\n", surf_g.dev.phydev.vk, surf_g.vk, tmp_present_modes_n_g);
	loop {
		if (i == (u8)tmp_present_modes_n_g)
			break;
		LOG("0:MAIN:physical device:%p:surface:%p:present mode=%s\n", surf_g.dev.phydev.vk, surf_g.vk, present_mode_to_str(tmp_present_modes_g[i]));
		++i;
	}
}

static void cpu_img_create(u8 i)
{
	struct vk_img_create_info_t info;
	s32 r;

	memset(&info, 0, sizeof(info));
	info.type = vk_struct_type_img_create_info;
	info.flags = vk_img_create_flag_2d_array_compatible_bit;
	info.img_type = vk_img_type_2d;
	info.texel_mem_blk_fmt = vk_texel_mem_blk_fmt_b8g8r8a8_unorm;
	info.extent.width = APP_CPU_IMG_WIDTH;
	info.extent.height = APP_CPU_IMG_HEIGHT;
	info.extent.depth = 1;
	info.mip_lvls_n = 1;
	info.samples_n = vk_samples_n_1_bit;
	info.array_layers_n = 1;
	info.img_tiling = vk_img_tiling_linear;
	info.usage = vk_img_usage_transfer_src_bit;
	info.initial_layout = vk_img_layout_undefined;
	vk_create_img(&info, &surf_g.dev.cpu_imgs[i].vk);
	VK_FATAL("0:MAIN:FATAL:%d:device:%p:unable to create swapchain cpu image %u\n", r, surf_g.dev.vk, i)
	LOG("0:MAIN:device:%p:swapchain cpu image %u created %p\n", surf_g.dev.vk, i, surf_g.dev.cpu_imgs[i].vk);
}

static void cpu_imgs_create(void)
{
	u8 i;

	i = 0;
	loop {
		if (i == surf_g.dev.swpchn.imgs_n)
			break;
		cpu_img_create(i);
		++i;
	}
}

static void img_mem_barrier_run_once(u8 i, struct vk_img_mem_barrier_t *b)
{
	s32 r;
	struct vk_cb_begin_info_t begin_info;
	struct vk_submit_info_t submit_info;

	memset(&begin_info, 0, sizeof(begin_info));
	begin_info.type = vk_struct_type_cb_begin_info;
	begin_info.flags = vk_cb_usage_one_time_submit_bit;
	vk_begin_cb(surf_g.dev.cbs[i], &begin_info);
	VK_FATAL("0:MAIN:FATAL:%d:command buffer:%p:unable to begin recording the initial layout transition command buffer\n", r, surf_g.dev.cbs[i])
	/*--------------------------------------------------------------------*/
	vk_cmd_pl_barrier(app_surf.dev.cbs[i], b);
	/*--------------------------------------------------------------------*/
	vk_end_cb(surf_g.dev.cbs[i]);
	VK_FATAL("0:MAIN:FATAL:%d:command buffer:%p:unable to end recording of the initial layout transition command buffer\n", r, surf_g.dev.cbs[i])
	/*--------------------------------------------------------------------*/
	memset(&submit_info, 0, sizeof(submit_info));
	submit_info.type = vk_struct_type_submit_info;
	submit_info.cbs_n = 1;
	submit_info.cbs = &surf_g.dev.cbs[i];
	vk_q_submit(&submit_info);
	VK_FATAL("0:MAIN:FATAL:%d:queue:%p:unable to submit the initial layout transition command buffer\n", r, surf_g.dev.q)
	/*--------------------------------------------------------------------*/
	vk_q_wait_idle();
	VK_FATAL("0:MAIN:FATAL:%d:queue:%p:unable to wait for idle or completion of initial layout transition command buffer\n", r, surf_g.dev.q)
	/*--------------------------------------------------------------------*/
	/*
	 * since it is tagged to run once its state_g is invalid, we need to
	 * reset it to the initial state_g
	 */
	vk_reset_cb(surf_g.dev.cbs[i]);
	VK_FATAL("0:MAIN:FATAL:%d:command buffer:%p:unable to reset the initial layout transition command buffer\n", r, surf_g.dev.cbs[i])
}

static void cpu_img_layout_to_general(u8 i)
{
	struct vk_img_mem_barrier_t b;
	struct vk_img_subrsrc_range_t *r;

	memset(&b, 0, sizeof(b));
	b.type = vk_struct_type_img_mem_barrier;
	b.old_layout = vk_img_layout_undefined;
	b.new_layout = vk_img_layout_general;
	b.src_q_fam = vk_q_fam_ignored;
	b.dst_q_fam = vk_q_fam_ignored;
	b.img = surf_g.dev.cpu_imgs[i].vk;
	r = &b.subrsrc_range;
	r->aspect = vk_img_aspect_color_bit;
	r->lvls_n = 1;
	r->array_layers_n = 1;
	img_mem_barrier_run_once(i, &b);
	LOG("0:MAIN:cpu image:%p[%u]:transition to general layout successful\n", surf_g.dev.cpu_imgs[i].vk, i);
}

/* once in general layout, the dev sees the img */
static void cpu_imgs_layout_to_general(void)
{
	u8 i;

	i = 0;
	loop {
		if (i == surf_g.dev.swpchn.imgs_n)
			break;
		cpu_img_layout_to_general(i);
		++i;
	}
}

static void tmp_cpu_img_mem_rqmts_get(u8 i)
{
	struct vk_img_mem_rqmts_info_t info;
	struct vk_mem_rqmts_t *rqmts;
	s32 r;

	memset(&info, 0, sizeof(info));
	info.type = vk_struct_type_img_mem_rqmts_info;
	info.img = surf_g.dev.cpu_imgs[i].vk;
	rqmts = &tmp_mem_rqmts_g[i];
	memset(rqmts, 0, sizeof(*rqmts));
	rqmts->type = vk_struct_type_mem_rqmts;
	vk_get_img_mem_rqmts(&info, rqmts);
	VK_FATAL("0:MAIN:FATAL:%d:device:%p:unable to get memory requirements for cpu image %u\n", r, surf_g.dev.vk, i)
	LOG("0:MAIN:device:%p:cpu image %u core requirements are size=%lu bytes, alignment=%lu bytes, memory type=%#08x\n", surf_g.dev.vk, i, (long)rqmts->core.sz, (long)rqmts->core.alignment, rqmts->core.mem_type_bits);
}

static void tmp_cpu_imgs_mem_rqmts_get(void)
{
	u8 i;

	i = 0;
	loop {
		if (i == surf_g.dev.swpchn.imgs_n)
			break;
		tmp_cpu_img_mem_rqmts_get(i);
		++i;
	}
}

#define WANTED_MEM_PROPS (vk_mem_prop_host_visible_bit \
						| vk_mem_prop_host_cached_bit)
#define IS_DEV_LOCAL(x) (((x)->prop_flags & vk_mem_prop_dev_local_bit) != 0)
static bool match_mem_type(u8 mem_type_idx,
		struct vk_mem_rqmts_t *img_rqmts, bool ignore_gpu_is_discret)
{
	struct vk_mem_type_t *mem_type;

	/* first check this mem type is in our img rqmts */
	if (((1 << mem_type_idx) & img_rqmts->core.mem_type_bits) == 0)
		return false;
	mem_type = &surf_g.dev.phydev.mem_types[mem_type_idx];
	if (!ignore_gpu_is_discret)
		if (surf_g.dev.phydev.is_discret_gpu && IS_DEV_LOCAL(mem_type))
			return false;
	if ((mem_type->prop_flags & WANTED_MEM_PROPS) == WANTED_MEM_PROPS)
		return true;
	return false;
}
#undef WANTED_MEM_PROPS
#undef IS_DEV_LOCAL

static bool try_alloc_cpu_img_dev_mem(u8 i,
			struct vk_mem_rqmts_t *img_rqmts, u8 mem_type_idx)
{
	struct vk_mem_alloc_info_t info;
	s32 r;

	memset(&info, 0, sizeof(info));
	info.type = vk_struct_type_mem_alloc_info;
	info.sz = img_rqmts->core.sz;
	info.mem_type_idx = mem_type_idx;
	vk_alloc_mem(&info, &surf_g.dev.cpu_imgs[i].dev_mem);
	if (r < 0) {
		LOG("0:MAIN:WARNING:%d:device:%p:cpu image:%u:unable to allocate %lu bytes from physical dev %p memory type %u\n", r, surf_g.dev.vk, img_rqmts->core.sz, surf_g.dev.phydev.vk, mem_type_idx);
		return false;
	}
	LOG("0:MAIN:device:%p:physical device:%p:cpu image:%u:%lu bytes allocated from memory type %u\n", surf_g.dev.vk, surf_g.dev.phydev.vk, i, img_rqmts->core.sz, mem_type_idx);
	return true;
}

/*
 * we are looking for host visible and host cached mem. on discret gpu we would
 * like non dev local mem that in order to avoid wasting video ram. if we have
 * a discret gpu but could not find a mem type without dev local mem, let's
 * retry with only host visible and host cached mem.
 */
#define IGNORE_GPU_IS_DISCRET true
static void cpu_img_dev_mem_alloc(u8 i)
{
	struct vk_mem_rqmts_t *img_rqmts;
	u8 mem_type;

	img_rqmts = &tmp_mem_rqmts_g[i];
	mem_type = 0;
	loop {
		if (mem_type == surf_g.dev.phydev.mem_types_n)
			break;
		if (match_mem_type(mem_type, img_rqmts,
						!IGNORE_GPU_IS_DISCRET)) {
			if (try_alloc_cpu_img_dev_mem(i, img_rqmts,
								mem_type))
				return;
		}
		++mem_type;
	}
	if (!surf_g.dev.phydev.is_discret_gpu)
		FATAL("0:MAIN:FATAL:physical device:%p:cpu image:%u:unable to find proper memory type or to allocate memory\n", surf_g.dev.phydev.vk, i)
	/*
	 * lookup again, but relax the match based on discret gpu constraint for 
	 * gpu
	 */
	mem_type = 0;
	loop {
		if (mem_type == surf_g.dev.phydev.mem_types_n)
			break;
		if (match_mem_type(mem_type, img_rqmts, IGNORE_GPU_IS_DISCRET)
		    && try_alloc_cpu_img_dev_mem(i, img_rqmts, mem_type))
				return;
		++mem_type;
	}
	FATAL("0:MAIN:FATAL:physical device:%p:cpu image:%u:unable to find proper memory type or to allocate memory\n", surf_g.dev.phydev.vk, i)
}
#undef IGNORE_GPU_IS_DISCRET

static void cpu_imgs_dev_mem_alloc(void)
{
	u8 i;

	i = 0;
	loop {
		if (i == surf_g.dev.swpchn.imgs_n)
			break;
		cpu_img_dev_mem_alloc(i);
		++i;
	}
}

static void cpu_imgs_dev_mem_bind(void)
{
	struct vk_bind_img_mem_info_t infos[swpchn_imgs_n_max];
	u8 i;
	s32 r;

	memset(&infos, 0, sizeof(infos[0]) * surf_g.dev.swpchn.imgs_n);
	i = 0;
	loop {
		if (i == surf_g.dev.swpchn.imgs_n)
			break;
		infos[i].type = vk_struct_type_bind_img_mem_info;
		infos[i].img = surf_g.dev.cpu_imgs[i].vk;
		infos[i].mem = surf_g.dev.cpu_imgs[i].dev_mem;
		++i;
	}
	vk_bind_img_mem(infos);
	VK_FATAL("0:MAIN:FATAL:%d:device:%p:cpu images:unable to bind device memory to images\n", r, surf_g.dev.vk)
	LOG("0:MAIN:device:%p:cpu images:bound device memory to images\n", surf_g.dev.vk);
}

static void cpu_imgs_dev_mem_map(void)
{
	u8 i;

	i = 0;
	loop {
		s32 r;

		if (i == surf_g.dev.swpchn.imgs_n)
			break;
		vk_map_mem(surf_g.dev.cpu_imgs[i].dev_mem,
						&surf_g.dev.cpu_imgs[i].data);
		VK_FATAL("0:MAIN:FATAL:%d:device:%p:cpu image:%u:unable to map image memory\n", r, surf_g.dev.vk, i)
		LOG("0:MAIN:device:%p:cpu image:%u:image memory mapped\n", surf_g.dev.vk, i);
		++i;
	}
}

static void cpu_img_subrsrc_layout_get(u8 i)
{
	struct vk_img_subrsrc_t s;

	memset(&s, 0, sizeof(s));
	/* 1 subrsrc = uniq color plane of mip lvl 0 and array 0 */
	s.aspect = vk_img_aspect_color_bit;
	vk_get_img_subrsrc_layout(surf_g.dev.cpu_imgs[i].vk, &s,
						&surf_g.dev.cpu_imgs[i].layout);
	LOG("0:MAIN:device:%p:cpu image:%u:layout:offset=%lu bytes size=%lu bytes row_pitch=%lu bytes array_pitch=%lu bytes depth_pitch=%lu bytes\n", surf_g.dev.vk, i, surf_g.dev.cpu_imgs[i].layout.offset, surf_g.dev.cpu_imgs[i].layout.sz, surf_g.dev.cpu_imgs[i].layout.row_pitch, surf_g.dev.cpu_imgs[i].layout.array_pitch, surf_g.dev.cpu_imgs[i].layout.depth_pitch);
}

static void cpu_imgs_subrsrc_layout_get(void)
{
	u8 i;

	i = 0;
	loop {
		if (i == surf_g.dev.swpchn.imgs_n)
			break;
		cpu_img_subrsrc_layout_get(i);
		++i;
	}
}

static void sems_create(void)
{
	s32 r;
	struct vk_sem_create_info_t info;
	u8 sem;

	sem = 0;
	loop {
		if (sem == sems_n)
			break;
		memset(&info, 0, sizeof(info));
		info.type = vk_struct_type_sem_create_info;
		vk_create_sem(&info, &surf_g.dev.sems[sem]);
		VK_FATAL("0:MAIN:FATAL:%d:device:%p:unable to create a semaphore %u for our swapchain\n", r, surf_g.dev.vk, sem)
		LOG("0:MAIN:device:%p:semaphore %u for our swapchain created %p\n", surf_g.dev.vk, sem, surf_g.dev.sems[sem]);
		++sem;
	}
}

static void cbs_create(void)
{
	s32 r;
	struct vk_cb_alloc_info_t alloc_info;

	memset(&alloc_info, 0, sizeof(alloc_info));
	alloc_info.type = vk_struct_type_cb_alloc_info;
	alloc_info.cp = surf_g.dev.cp;
	alloc_info.lvl = vk_cb_lvl_primary;
	alloc_info.cbs_n = surf_g.dev.swpchn.imgs_n;
	vk_alloc_cbs(&alloc_info);
	VK_FATAL("0:MAIN:FATAL:%d:device:%p:unable to allocate command buffers for our swapchain images from %p command pool\n", r, surf_g.dev.vk, surf_g.dev.cp)
	LOG("0:MAIN:device:%p:allocated %u command buffers for our swapchain images from %p command pool\n", surf_g.dev.vk, surf_g.dev.swpchn.imgs_n, surf_g.dev.cp);
}

static void cb_rec(u8 i)
{
	s32 r;
	struct vk_cb_begin_info_t begin_info;
	struct vk_img_mem_barrier_t b;
	struct vk_img_blit_t region;
	/*--------------------------------------------------------------------*/
	memset(&begin_info, 0, sizeof(begin_info));
	begin_info.type = vk_struct_type_cb_begin_info;
	vk_begin_cb(surf_g.dev.cbs[i], &begin_info);
	VK_FATAL("0:MAIN:FATAL:%d:swapchain img:%u:command buffer:%p:unable to begin recording\n", r, i, surf_g.dev.cbs[i])
	/*--------------------------------------------------------------------*/
	/* acquired img (undefined layout) to presentation layout */
	memset(&b, 0, sizeof(b));
	b.type = vk_struct_type_img_mem_barrier;
	b.old_layout = vk_img_layout_undefined;
	b.new_layout = vk_img_layout_present;
	b.src_q_fam = vk_q_fam_ignored;
	b.dst_q_fam = vk_q_fam_ignored;
	b.img = surf_g.dev.swpchn.imgs[i];
	b.subrsrc_range.aspect = vk_img_aspect_color_bit;
	b.subrsrc_range.lvls_n = 1;
	b.subrsrc_range.array_layers_n = 1;
	vk_cmd_pl_barrier(surf_g.dev.cbs[i], &b);
	/*--------------------------------------------------------------------*/
	/* blit from cpu img to pe img */
	memset(&region, 0, sizeof(region));
	region.src_subrsrc.aspect = vk_img_aspect_color_bit;
	region.src_subrsrc.array_layers_n = 1;
	region.src_offsets[1].x = APP_CPU_IMG_WIDTH;
	region.src_offsets[1].y = APP_CPU_IMG_HEIGHT;
	region.dst_subrsrc.aspect = vk_img_aspect_color_bit;
	region.dst_subrsrc.array_layers_n = 1;
	/* XXX: it is a scaling blit: you can use APP_WIN_WIDTH/APP_WIN_HEIGHT */
	region.dst_offsets[1].x = APP_CPU_IMG_WIDTH;
	region.dst_offsets[1].y = APP_CPU_IMG_HEIGHT;
	vk_cmd_blit_img(surf_g.dev.cbs[i], surf_g.dev.cpu_imgs[i].vk,
					surf_g.dev.swpchn.imgs[i], &region);
	/*--------------------------------------------------------------------*/
	vk_end_cb(surf_g.dev.cbs[i]);
	VK_FATAL("0:MAIN:FATAL:%d:swapchain img:%u:command buffer:%p:unable to end recording\n", r, i, surf_g.dev.cbs[i])
}

static void cbs_rec(void)
{
	u8 i;

	i = 0;
	loop {
		if (i == surf_g.dev.swpchn.imgs_n)
			break;
		cb_rec(i);
		++i;
	}
}

static void phydev_init(void)
{
	tmp_phydevs_get();
	/*--------------------------------------------------------------------*/
	tmp_phydevs_exts_dump();
	tmp_phydevs_props_dump();
	tmp_phydevs_mem_props_get();
	tmp_phydevs_mem_props_dump();
	/*--------------------------------------------------------------------*/
	tmp_phydevs_q_fams_get();
	tmp_phydevs_q_fams_dump();
	tmp_phydevs_q_fams_surf_support_get();
	/*--------------------------------------------------------------------*/
	tmp_phydev_and_q_fam_select();
	/*--------------------------------------------------------------------*/
	texel_mem_blk_conf_select();
	/*--------------------------------------------------------------------*/
	tmp_surf_caps_get();
	tmp_surf_caps_dump();
	/*--------------------------------------------------------------------*/
	tmp_present_modes_get();
	tmp_present_modes_dump();
}

static void dev_init(void)
{
	phydev_init();
	/*--------------------------------------------------------------------*/
	dev_create();
	dev_syms();
	q_get();
	cp_create();
}

static void surf_init(void)
{
	surf_create();
	dev_init();
	swpchn_init();
	swpchn_imgs_get();
	/* our cpu imgs for swpchn imgs */
	cpu_imgs_create();
	sems_create();
	cbs_create();
	cpu_imgs_layout_to_general();
	cpu_imgs_subrsrc_layout_get();
	tmp_cpu_imgs_mem_rqmts_get();
	cpu_imgs_dev_mem_alloc();
	cpu_imgs_dev_mem_bind();
	cpu_imgs_dev_mem_map();
	cbs_rec();
}

static void init_vk(void)
{
	load_vk_loader();
	loader_syms();
	instance_static_syms();
	check_vk_version();
	instance_exts_dump();
	instance_layers_dump();
	/*--------------------------------------------------------------------*/
	instance_create();
	instance_syms();
	/*--------------------------------------------------------------------*/
	surf_init();
}

static void swpchn_acquire_next_img(u32 *i)
{
	struct vk_acquire_next_img_info_t info;
	s32 r;

	memset(&info, 0, sizeof(info));
	info.type = vk_struct_type_acquire_next_img_info;
	info.swpchn = surf_g.dev.swpchn.vk;
	info.timeout = u64_max; /* infinite */
	info.devs = 0x00000001; /* no device group then 1 */
	info.sem = surf_g.dev.sems[sem_acquire_img_done];
	vk_acquire_next_img(&info, i);
	VK_FATAL("0:MAIN:FATAL:%d:device:%p:unable to acquire next image from swapchain %p\n", r, surf_g.dev.vk, surf_g.dev.swpchn.vk)
	LOG("0:MAIN:device:%p:swapchain:%p:acquired image %u\n", surf_g.dev.vk, surf_g.dev.swpchn.vk, *i);
}

/* solid color */
static void cpu_img_draw(u8 i)
{
	u32 *texel;
	u64 row;
	u64 col;

	texel = (u32*)surf_g.dev.cpu_imgs[i].data;
	row = 0;
	loop {
		if (row == APP_CPU_IMG_HEIGHT)
			break;
		col = 0;
		loop {
			struct vk_subrsrc_layout_t *l;
			u64 o; /* _byte_ offset */
			u64 o_w; /* _32 bits_ word offset */

			if (col == APP_CPU_IMG_WIDTH)
				break;
			l = &surf_g.dev.cpu_imgs[i].layout;
			o = row * l->row_pitch + col * sizeof(*texel);
			o_w = o >> 2;
			texel[o_w] = fill_texel_g;
			++col;
		}
		++row;
	}
}

static void cpu_img_to_pe(u8 i)
{
	s32 r;
	struct vk_submit_info_t submit_info;
	struct vk_present_info_t present_info;
	u32 wait_dst_stage;
	u32 idxs[1];

	memset(&submit_info, 0, sizeof(submit_info));
	submit_info.type = vk_struct_type_submit_info;
	submit_info.wait_sems_n = 1;
	submit_info.wait_sems = &surf_g.dev.sems[sem_acquire_img_done];
	wait_dst_stage = vk_pl_stage_bottom_of_pipe_bit;
	submit_info.wait_dst_stages = &wait_dst_stage;
	submit_info.cbs_n = 1;
	submit_info.cbs = &surf_g.dev.cbs[i];
	submit_info.signal_sems_n = 1;
	submit_info.signal_sems = &surf_g.dev.sems[app_sem_blit_done];
	LOG("MAIN:queue:%p\n", surf_g.dev.q);

	vk_q_submit(&submit_info);
	VK_FATAL("0:MAIN:FATAL:%d:queue:%p:unable to submit the image pre-recorded command buffer\n", r, surf_g.dev.q)
	/*--------------------------------------------------------------------*/
	idxs[0] = i;
	memset(&present_info, 0, sizeof(present_info));
	present_info.type = vk_struct_type_present_info;
	present_info.wait_sems_n = 1;
	present_info.wait_sems = &surf_g.dev.sems[sem_blit_done];
	present_info.swpchns_n = 1;
	present_info.swpchns = &surf_g.dev.swpchn.vk;
	present_info.idxs = idxs;
	present_info.results = 0;
	vk_q_present(&present_info);
	VK_FATAL("0:MAIN:FATAL:%d:queue:%p:unable to submit the image %u to the presentation engine\n", r, surf_g.dev.q, i)
}

static void render(void)
{
	u32 i;

	swpchn_acquire_next_img(&i);
	cpu_img_draw(i); /* cpu rendering */
	cpu_img_to_pe(i);
	do_render_g = false;
	if (fill_texel_g == 0x0000ff00)
		fill_texel_g = 0x00ff0000;
	else
		fill_texel_g = 0x0000ff00;
}

static void run(void)
{
	state_g = state_run;

	render();

	loop {
		xcb_generic_event_t *e;	

		do_render_g = false;
		/* "evts which could lead to change what we display" */
		e = dl_xcb_wait_for_event(app_xcb.c);
		if (e == 0) { /* i/o err */
			LOG("0:MAIN:xcb:'%s':connection:%p:event:input/output error | x11 server connection lost\n", app_xcb.disp_env, app_xcb.c);
			break;
		}
		loop { /* drain evts */
			app_xcb_evt_handle(e);
			free(e);
			if (state_g == state_quit)
				return;
			e = dl_xcb_poll_for_event(app_xcb.c);
			if (e == 0)
				break;
		}
		/* synchronous rendering */
		if (do_render_g)
			render();
	}
}

int main(void)
{
	LOG("0:starting app\n");
	app_xcb_init();
	init_vk();
	fill_texel_g = 0x0000ff00;
	run();
	LOG("0:exiting app\n");
	exit(0);
}
#undef VK_FATAL
#undef FATAL
/*---------------------------------------------------------------------------*/
#define CLEANUP
#include "namespace/app.c"
#include "namespace/vk_syms.c"
#include "namespace/app_state_types.h"
#include "namespace/app_state.c"
#undef CLEANUP
/*---------------------------------------------------------------------------*/
#endif
