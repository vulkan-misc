#ifndef APP_XCB_C
#define APP_XCB_C
/*
 * this is public domain without any warranties of any kind
 * Sylvain BERTRAND
 */
/* XXX: KEEP AN EYE ON ABBREVIATIONS, ALWAYS */
#include <string.h>
#include <stdlib.h>
#include <xcb.h>
#include "app_core_types.h"
#include "log.h"
#include "xcb_syms.c"
#include "app_state.c"

#define LINUX_KEY_ESC	0x01
#define LINUX_KEY_SPACE	0x39
/* x11 keycodes do offset linux keycodes by 8 */
#define X11_KEYCODE_ESC		(LINUX_KEY_ESC + 8)
#define X11_KEYCODE_SPACE	(LINUX_KEY_SPACE + 8)

struct app_xcb_t {
	u8 *disp_env;
	xcb_connection_t *c;
	xcb_setup_t *setup;
	int scr_idx;
	xcb_screen_t *scr;
	u32 win_id;
};
static struct app_xcb_t app_xcb;

static void app_xcb_win_create(void)
{
	u32 value_mask;
	u32 value_list[2];
	xcb_void_cookie_t cookie;
	xcb_generic_error_t *e;

	app_xcb.win_id = dl_xcb_generate_id(app_xcb.c);
	LOG("0:MAIN:xcb:'%s':connection:%p:screen:%d:root window id:%#x:window id=%#x\n", app_xcb.disp_env, app_xcb.c, app_xcb.scr_idx, app_xcb.scr->root, app_xcb.win_id);

	value_mask = XCB_CW_BACK_PIXEL | XCB_CW_EVENT_MASK;
	value_list[0] = app_xcb.scr->black_pixel;
	value_list[1] = XCB_EVENT_MASK_KEY_RELEASE
					| XCB_EVENT_MASK_RESIZE_REDIRECT;

	dl_xcb_create_window(app_xcb.c, XCB_COPY_FROM_PARENT, app_xcb.win_id,
					app_xcb.scr->root, 0, 0,
					APP_WIN_WIDTH, APP_WIN_HEIGHT, 0,
					XCB_WINDOW_CLASS_INPUT_OUTPUT,
					app_xcb.scr->root_visual,
					value_mask, value_list);
    	cookie = dl_xcb_map_window_checked(app_xcb.c, app_xcb.win_id);
	LOG("0:MAIN:xcb:'%s':connection:%p:screen:%d:root window id:%#x:window id:%#x:map window request cookie=%#x\n", app_xcb.disp_env, app_xcb.c, app_xcb.scr_idx, app_xcb.scr->root, app_xcb.win_id, cookie);

	e = dl_xcb_request_check(app_xcb.c, cookie);
	if (e != 0) {
		LOG("0:MAIN:FATAL:xcb:'%s':connection:%p:screen:%d:root window id:%#x:window id:%#x:unable to map window\n", app_xcb.disp_env, app_xcb.c, app_xcb.scr_idx, app_xcb.scr->root, app_xcb.win_id);
		exit(1);
	}
	LOG("0:MAIN:xcb:'%s':connection:%p:screen:%d:root window id:%#x:window id:%#x:window mapped\n", app_xcb.disp_env, app_xcb.c, app_xcb.scr_idx, app_xcb.scr->root, app_xcb.win_id);
}

/*
 * a disp is n scrs and 1 [set of] keyboard[s] and 1 [set of] mouse[s]
 * 1 scr could be n monitors
 * nowdays: usually 1 scr per display
 * 1 scr has 1 root win
 */
static void app_xcb_connect(void)
{
	int r;

	app_xcb.disp_env = getenv("DISPLAY");
	if (app_xcb.disp_env == 0 || app_xcb.disp_env[0] == 0) {
		LOG("0:MAIN:no x11 DISPLAY environment variable, exiting\n");
		exit(0);
	}

	app_xcb.scr_idx = 0;
	app_xcb.c = dl_xcb_connect(0, &app_xcb.scr_idx); /* should be 0 though */
	r = dl_xcb_connection_has_error(app_xcb.c);
	if (r > 0) {
		LOG("0:MAIN:FATAL:%d:%s:error while connecting to the x11 server\n", r, app_xcb.disp_env);
		exit(1);
	}
	LOG("0:MAIN:xcb:'%s':connection=%p, default screen index is %d (should be 0)\n", app_xcb.disp_env, app_xcb.c, app_xcb.scr_idx);
}

static void app_xcb_scr_get(void)
{
	xcb_screen_iterator_t iter;
	int scrs_n;
	int i;

	app_xcb.setup = dl_xcb_get_setup(app_xcb.c);

	scrs_n = dl_xcb_setup_roots_length(app_xcb.setup);
	LOG("0:MAIN:xcb:'%s':connection:%p:has %d screens (should be 1)\n", app_xcb.disp_env, app_xcb.c, scrs_n);

	iter = dl_xcb_setup_roots_iterator(app_xcb.setup);
	i = 0;
	app_xcb.scr = 0;
	loop {
		if (iter.rem == 0)
			break; /* no more scr to iterate on */

		if (i == app_xcb.scr_idx) {
			app_xcb.scr = iter.data;
			break;
		}
		dl_xcb_screen_next(&iter);
	}
	LOG("0:MAIN:xcb:'%s':connection:%p:screen:%d:root window id:%#x:width=%d pixels\n", app_xcb.disp_env, app_xcb.c, app_xcb.scr_idx, app_xcb.scr->root, app_xcb.scr->width_in_pixels);
	LOG("0:MAIN:xcb:'%s':connection:%p:screen:%d:root window id:%#x:height=%d pixels\n", app_xcb.disp_env, app_xcb.c, app_xcb.scr_idx, app_xcb.scr->root, app_xcb.scr->height_in_pixels);
	LOG("0:MAIN:xcb:'%s':connection:%p:screen:%d:root window id:%#x:white pixel=0x%08x\n", app_xcb.disp_env, app_xcb.c, app_xcb.scr_idx, app_xcb.scr->root, app_xcb.scr->white_pixel);
	LOG("0:MAIN:xcb:'%s':connection:%p:screen:%d:root window id:%#x:black pixel=0x%08x\n", app_xcb.disp_env, app_xcb.c, app_xcb.scr_idx, app_xcb.scr->root, app_xcb.scr->black_pixel);
}

#define MIN_SZ_BIT (1 << 4)
#define MAX_SZ_BIT (1 << 5)
#define FLAGS		0
/* 4 padding dwords */
#define MIN_WIDTH	5
#define MIN_HEIGHT	6
#define MAX_WIDTH	7
#define MAX_HEIGHT	8
#define DWORDS_N	18
static void app_xcb_wm_hints(void)
{
	u32 data[DWORDS_N];

	memset(data, 0, sizeof(data));
	data[FLAGS] = MIN_SZ_BIT | MAX_SZ_BIT;
	data[MIN_WIDTH] = APP_WIN_WIDTH;
	data[MIN_HEIGHT] = APP_WIN_HEIGHT;
	data[MAX_WIDTH] = APP_WIN_WIDTH;
	data[MAX_HEIGHT] = APP_WIN_HEIGHT;
	
	dl_xcb_change_property(app_xcb.c, XCB_PROP_MODE_REPLACE, app_xcb.win_id,
		XCB_ATOM_WM_NORMAL_HINTS, XCB_ATOM_WM_SIZE_HINTS, 32, DWORDS_N,
									data);
}
#undef MIN_SZ_BIT
#undef MAX_SZ_BIT
#undef FLAGS
#undef MIN_WIDTH
#undef MIN_HEIGHT
#undef MAX_WIDTH
#undef MAX_HEIGHT
#undef DWORDS_N

static void app_xcb_init(void)
{
	int r;

	app_xcb_client_lib_load();
	app_xcb_syms();

	app_xcb_connect();
	app_xcb_scr_get();
	app_xcb_win_create();
	app_xcb_wm_hints();

	r = dl_xcb_flush(app_xcb.c);
	if (r <= 0) {
		LOG("0:MAIN:FATAL:%d:xcb:'%s':connection:%p:screen:%d:root window id:%#x:window id:%#x:unable to flush the connection\n", r, app_xcb.disp_env, app_xcb.c, app_xcb.scr_idx, app_xcb.scr->root, app_xcb.win_id);
		exit(1);
	}
	LOG("0:MAIN:xcb:'%s':connection:%p:connection flushed\n", app_xcb.disp_env, app_xcb.c);
}

static void app_xcb_evt_key_release(xcb_generic_event_t *evt)
{
	xcb_key_release_event_t *key;

	key = (xcb_key_release_event_t*)evt;

	switch (key->detail) {
	case X11_KEYCODE_ESC: 
		app_state = app_state_quit;
		break;
	case X11_KEYCODE_SPACE:
		app_do_render = true;
		break;
	default:
		LOG("0:MAIN:xcb:'%s':connection:%p:event:key release:keycode:%#02x\n", app_xcb.disp_env, app_xcb.c, key->detail);
		break;
	}
}

static void app_xcb_evt_resz_request(xcb_generic_event_t *evt)
{
	xcb_resize_request_event_t *rre;

	/*
	 * the x11 server is not resizing the win, it is asking us to
	 * actually do it (and we won't)
	 */
	rre = (xcb_resize_request_event_t*)evt;
	LOG("0:MAIN:xcb:'%s':connection:%p:event:resize request:window=%u width=%u,height=%u\n", app_xcb.disp_env, app_xcb.c, rre->window, rre->width, rre->height);
}

static void app_xcb_evt_handle(xcb_generic_event_t *evt)
{
	u8 evt_code;

	/*
	 * do not discriminate evts generated by clients using sendevent
	 * requests (note: "client message" evts have always their most
	 * significant bit set)
	 */
	evt_code = evt->response_type & 0x7f;

	switch (evt_code) {
	case XCB_KEY_RELEASE:
		app_xcb_evt_key_release(evt);
		break;
	case XCB_RESIZE_REQUEST:
		app_xcb_evt_resz_request(evt);
		break;
	default:
		break;
	}
}
#endif
