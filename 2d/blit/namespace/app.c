#ifndef CLEANUP
#define check_vk_version			app_check_vk_version
#define cmdbuf_record				app_cmdbuf_record
#define cmdbufs_create				app_cmdbufs_create
#define cmdbufs_record				app_cmdbufs_record
#define cmdpools_create				app_cmdpools_create
#define cpu_img_create				app_cpu_img_create
#define cpu_img_dev_mem_alloc			app_cpu_img_dev_mem_alloc
#define cpu_img_draw				app_cpu_img_draw
#define cpu_img_layout_to_general		app_cpu_img_layout_to_general
#define cpu_img_subrsrc_layout_get		app_cpu_img_subrsrc_layout_get
#define cpu_img_to_pe				app_cpu_img_to_pe
#define cpu_imgs_create				app_cpu_imgs_create
#define cpu_imgs_dev_mem_alloc			app_cpu_imgs_dev_mem_alloc
#define cpu_imgs_dev_mem_bind			app_cpu_imgs_dev_mem_bind
#define cpu_imgs_dev_mem_map			app_cpu_imgs_dev_mem_map
#define cpu_imgs_subrsrc_layout_get		app_cpu_imgs_subrsrc_layout_get
#define dev_create				app_dev_create
#define dev_init				app_dev_init
#define dev_type_str				app_dev_type_str
#define img_mem_barrier_run_once		app_img_mem_barrier_run_once
#define init_vk					app_init_vk
#define instance_create				app_instance_create
#define instance_exts_dump			app_instance_exts_dump
#define instance_layers_dump			app_instance_layers_dump
#define match_mem_type				app_match_mem_type
#define phydev_exts_dump			app_phydev_exts_dup
#define phydev_init				app_phydev_init
#define phydevs_exts_dump			app_phydevs_exts_dup
#define phydevs_props_dump			app_phydevs_props_dump
#define present_mode_to_str			app_present_mode_to_str
#define q_get					app_q_get
#define render					app_render
#define run					app_run
#define sems_create				app_sems_create
#define surf_create				app_surf_create
#define surf_init				app_surf_init
#define swpchn_acquire_next_img			app_swpchn_acquire_next_img
#define swpchn_imgs_get				app_swpchn_imgs_get
#define swpchn_init				app_swpchn_init
#define texel_mem_blk_confs_dump		app_texel_mem_blk_confs_dump
#define tmp_cpu_img_mem_rqmts_get		app_tmp_cpu_img_mem_rqmts_get
#define tmp_cpu_imgs_mem_rqmts_get		app_cpu_imgs_mem_rqmts_get
#define tmp_phydev_and_q_fam_select		app_tmp_phydev_and_q_fam_select
#define tmp_phydev_mem_heap_dump		app_phydev_mem_heap_dump
#define tmp_phydev_mem_heaps_dump		app_phydev_mem_heaps_dump
#define tmp_phydev_mem_props_dump		app_tmp_phydev_mem_props_dump
#define tmp_phydev_mem_props_get		app_tmp_phydev_mem_props_get
#define tmp_phydev_mem_type_dump		app_tmp_phydev_mem_type_dump
#define tmp_phydev_mem_types_dump		app_tmp_phydev_mem_types_dump
#define tmp_phydev_q_fams_get			app_tmp_phydev_q_fams_get
#define tmp_phydev_q_fams_dump			app_tmp_phydev_q_fams_dump
#define tmp_phydevs_mem_props_dump		app_tmp_phydevs_mem_props_dump
#define tmp_phydevs_mem_props_get		app_tmp_phydevs_mem_props_get
#define tmp_phydevs_props_dump			app_tmp_phydevs_props_dump
#define tmp_phydevs_q_fams_dump			app_tmp_phydevs_q_fams_dump
#define tmp_phydevs_q_fams_get			app_tmp_phydevs_q_fams_get
#define tmp_phydevs_q_fams_surf_support_get	app_tmp_phydevs_q_fams_surf_support_get
#define tmp_phydevs_get				app_tmp_phydevs_get
#define tmp_present_modes_dump			app_present_modes_dump
#define tmp_present_modes_get			app_present_modes_get
#define tmp_selected_phydev_cherry_pick		app_tmp_selected_phydev_cherry_pick
#define tmp_surf_caps_dump			app_surf_caps_dump
#define tmp_surf_caps_get			app_surf_caps_get
#define tmp_texel_mem_blk_conf_select		app_tmp_texel_mem_blk_conf_select
#define try_alloc_cpu_img_dev_mem		app_try_alloc_cpu_img_dev_mem
#define uuid_str				app_uuid_str
/******************************************************************************/
#else
#undef check_vk_version
#undef cmdbuf_record
#undef cmdbufs_create
#undef cmdbufs_record
#undef cmdpools_create
#undef cpu_img_create
#undef cpu_img_dev_mem_alloc
#undef cpu_img_draw
#undef cpu_img_layout_to_general
#undef cpu_img_subrsrc_layout_get
#undef cpu_img_to_pe
#undef cpu_imgs_create
#undef cpu_imgs_dev_mem_alloc
#undef cpu_imgs_dev_mem_bind
#undef cpu_imgs_dev_mem_map
#undef cpu_imgs_layout_to_general
#undef cpu_imgs_subrsrc_layout_get
#undef dev_create
#undef dev_init
#undef dev_type_str
#undef img_mem_barrier_run_once
#undef init_vk
#undef instance_create
#undef instance_exts_dump
#undef instance_layers_dump
#undef match_mem_type
#undef phydev_exts_dump
#undef phydev_init
#undef phydevs_exts_dump
#undef phydevs_props_dump
#undef present_mode_to_str
#undef q_get
#undef render
#undef run
#undef sems_create
#undef surf_create
#undef surf_init
#undef swpchn_acquire_next_img
#undef swpchn_imgs_get
#undef swpchn_init
#undef texel_mem_blk_confs_dump
#undef tmp_cpu_img_mem_rqmts_get
#undef tmp_cpu_imgs_mem_rqmts_get
#undef tmp_phydev_and_q_fam_select
#undef tmp_phydev_mem_heap_dump
#undef tmp_phydev_mem_heaps_dump
#undef tmp_phydev_mem_props_dump
#undef tmp_phydev_mem_props_get
#undef tmp_phydev_mem_type_dump
#undef tmp_phydev_mem_types_dump
#undef tmp_phydev_q_fams_get
#undef tmp_phydev_q_fams_dump
#undef tmp_phydevs_mem_props_dump
#undef tmp_phydevs_mem_props_get
#undef tmp_phydevs_props_dump
#undef tmp_phydevs_q_fams_get
#undef tmp_phydevs_q_fams_dump
#undef tmp_phydevs_q_fams_surf_support_get
#undef tmp_phydevs_get
#undef tmp_present_modes_dump
#undef tmp_present_modes_get
#undef tmp_selected_phydev_cherry_pick
#undef tmp_surf_caps_dump
#undef tmp_surf_caps_get
#undef tmp_texel_mem_blk_conf_select
#undef try_alloc_cpu_img_dev_mem
#undef uuid_str
#endif
