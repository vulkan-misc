#ifndef CLEANUP
#define dev_syms		app_devs_syms
#define instance_static_syms	app_instance_static_syms
#define instance_syms		app_instance_syms
#define loader_g		app_loader
#define loader_syms		app_loader_syms
#define load_vk_loader		app_load_vk_loader
/******************************************************************************/
#else
#undef dev_syms
#undef instance_static_syms
#undef instance_syms
#undef loader_g
#undef loader_syms
#undef load_vk_loader
#endif
