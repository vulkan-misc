#ifndef CLEANUP
#define cpu_img_t		app_cpu_img_t
#define dev_t			app_dev_t
#define phydev_t		app_phydev_t
#define sems_n			app_sems_n 
#define sem_acquire_img_done	app_sem_acquire_img_done
#define sem_blit_done		app_sem_blit_done
#define state_quit		app_state_quit
#define state_run		app_state_run
#define surf_t			app_surf_t
#define swpchn_imgs_n_max	app_swpchn_imgs_n_max
#define swpchn_t 		app_swpchn_t
#define tmp_phydev_q_fams_n_max	app_tmp_phydev_q_fams_n_max
#define tmp_phydev_t		app_tmp_phydev_t
#define tmp_phydevs_n_max	app_tmp_phydevs_n_max 
#define tmp_present_modes_n_max	app_tmp_present_modes_n_max
/******************************************************************************/
#else
#undef cpu_img_t
#undef dev_t
#undef phydev_t	
#undef sems_n 
#undef sem_acquire_img_done
#undef sem_blit_done
#undef state_run
#undef surf_t
#undef state_quit
#undef swpchn_imgs_n_max
#undef swpchn_t
#undef tmp_phydev_q_fams_n_max
#undef tmp_phydev_t
#undef tmp_phydevs_n_max 
#undef tmp_present_modes_n_max
#endif
