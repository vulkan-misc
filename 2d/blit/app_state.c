#ifndef APP_STATE_C
#define APP_STATE_C
/*
 * this is public domain without any warranties of any kind
 * Sylvain BERTRAND
 */
/* XXX: KEEP AN EYE ON ABBREVIATIONS, ALWAYS */
#include <stddef.h>
#include <xcb.h>
#include "app_core_types.h"
#include "nyanvk/types.h"
#include "app_state_types.h"
/*----------------------------------------------------------------------------*/
#include "namespace/app_state_types.h"
#include "namespace/app_state.c"
/*----------------------------------------------------------------------------*/
static u8 state_g;
static bool do_render_g;
static u32 fill_texel_g;
/*----------------------------------------------------------------------------*/
static void *instance_g;
static struct surf_t surf_g;
/*============================================================================*/
/* tmp storage */
static struct tmp_phydev_t tmp_phydevs_g[tmp_phydevs_n_max];
static u32 tmp_phydevs_n_g;
static struct vk_surf_caps_t tmp_surf_caps_g;
static u32 tmp_present_modes_g[tmp_present_modes_n_max];
static u32 tmp_present_modes_n_g;
static struct vk_mem_rqmts_t tmp_mem_rqmts_g[swpchn_imgs_n_max];
#define CLEANUP
#include "namespace/app_state_types.h"
#include "namespace/app_state.c"
#undef CLEANUP
#endif /* APP_STATE_C */
