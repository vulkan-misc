		capability shader
//==============================================================================
		memory_model addressing_model=logical memory_model=glsl450

//==============================================================================
		entry_point execution_model=fragment entry_point=%13 name="main" \
			 /*interfaces[0]=*/%9
//==============================================================================
		decorate target=%9 decoration=location 0
//==============================================================================
/* %2 */	type_void %2
/* %3 */	type_function id=%3 return_type=%2
/* %6 */	type_float id=%6 width=32
/* %7 */	type_vector id=%7 component_type=%6 components_n=4
/* %8 */	type_pointer id=%8 storage_class=output type=%7
/* %9 */	variable pointer_id=%9 type=%8 storage_class=output
//==============================================================================
// section end: non function declarations, breaking opcode=function(54)
/* %10 */	constant type=%6 id=%10 /*values[0]=*/0.0
/* %11 */	constant type=%6 id=%11 /*values[0]=*/1.0
// {1.0f, 1.0f, 1.0f, 1.0f} 
/* %12 */	constant_composite type=%7 id=%12 \
			/*constituents[0]=*/%11 \
			/*constituents[1]=*/%11 \
			/*constituents[2]=*/%11 \
			/*constituents[3]=*/%11
//==============================================================================
/* %13 */	function id=%13 return_type=%2 control=none type=%3
/* %14 */		label id=%14
				store pointer=%9 object=%12
				return
		function_end
