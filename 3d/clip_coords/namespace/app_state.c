#ifndef CLEANUP
#define do_render_g		app_do_render
#define instance_g		app_instance
#define state_g 		app_state
#define surf_g			app_surf
#define tmp_phydevs_g		app_tmp_phydevs
#define tmp_phydevs_n_g		app_tmp_phydevs_n
#define tmp_present_modes_g	app_tmp_present_modes
#define tmp_present_modes_n_g	app_tmp_present_modes_n
#define tmp_surf_caps_g		app_tmp_surf_caps
/******************************************************************************/
#else
#undef do_render_g
#undef instance_g
#undef state_g
#undef surf_g
#undef tmp_phydevs_g
#undef tmp_phydevs_n_g
#undef tmp_present_modes_g
#undef tmp_present_modes_n_g
#undef tmp_surf_caps_g
#endif
