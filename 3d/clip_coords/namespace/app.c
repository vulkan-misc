#ifndef CLEANUP
#define check_vk_version			app_check_vk_version
#define cb_rec					app_cb_rec
#define cbs_create				app_cbs_create
#define cbs_rec					app_cbs_rec
#define cp_create				app_cp_create
#define dev_create				app_dev_create
#define dev_init				app_dev_init
#define dev_type_str				app_dev_type_str
#define draw					app_draw
#define file_mmap				app_file_mmap
#define init_vk					app_init_vk
#define instance_create				app_instance_create
#define instance_exts_dump			app_instance_exts_dump
#define instance_layers_dump			app_instance_layers_dump
#define phydev_exts_dump			app_phydev_exts_dup
#define phydev_init				app_phydev_init
#define phydev_mem_heap_dump			app_phydev_mem_heap_dump
#define phydev_mem_type_dump			app_phydev_mem_type_dump
#define pl_layout_empty_create			app_pl_layout_empty_create
#define pl_create				app_pl_create
#define present_mode_to_str			app_present_mode_to_str
#define q_get					app_q_get
#define render					app_render
#define rp_create				app_rp_create
#define run					app_run
#define sems_create				app_sems_create
#define shmods_create				app_shmods_create
#define shmods_destroy				app_shmods_destroy
#define surf_create				app_surf_create
#define surf_init				app_surf_init
#define swpchn_acquire_next_img			app_swpchn_acquire_next_img
#define swpchn_fb_create			app_swpchn_fb_create
#define swpchn_fbs_create			app_swpchn_fbs_create
#define swpchn_imgs_get				app_swpchn_imgs_get
#define swpchn_imgview_create			app_swpchn_imgview_create
#define swpchn_imgviews_create			app_swpchn_imgviews_create
#define swpchn_init				app_swpchn_init
#define texel_mem_blk_conf_select		app_texel_mem_blk_conf_select
#define texel_mem_blk_confs_dump		app_texel_mem_blk_confs_dump
#define tmp_phydev_and_q_fam_select		app_tmp_phydev_and_q_fam_select
#define tmp_phydev_mem_heaps_dump		app_phydev_mem_heaps_dump
#define tmp_phydev_mem_props_dump		app_tmp_phydev_mem_props_dump
#define tmp_phydev_mem_props_get		app_tmp_phydev_mem_props_get
#define tmp_phydev_mem_types_dump		app_tmp_phydev_mem_types_dump
#define tmp_phydev_q_fams_get			app_tmp_phydev_q_fams_get
#define tmp_phydev_q_fams_dump			app_tmp_phydev_q_fams_dump
#define tmp_phydevs_mem_props_dump		app_tmp_phydevs_mem_props_dump
#define tmp_phydevs_mem_props_get		app_tmp_phydevs_mem_props_get
#define tmp_phydevs_props_dump			app_tmp_phydevs_props_dump
#define tmp_phydevs_q_fams_dump			app_tmp_phydevs_q_fams_dump
#define tmp_phydevs_q_fams_get			app_tmp_phydevs_q_fams_get
#define tmp_phydevs_q_fams_surf_support_get	app_tmp_phydevs_q_fams_surf_support_get
#define tmp_phydevs_get				app_tmp_phydevs_get
#define tmp_present_modes_dump			app_present_modes_dump
#define tmp_present_modes_get			app_present_modes_get
#define tmp_selected_phydev_cherry_pick		app_tmp_selected_phydev_cherry_pick
#define tmp_surf_caps_dump			app_surf_caps_dump
#define tmp_surf_caps_get			app_surf_caps_get
#define uuid_str				app_uuid_str
/******************************************************************************/
#else
#undef check_vk_version
#undef cb_rec
#undef cbs_create
#undef cbs_rec
#undef cp_create
#undef dev_create
#undef dev_init
#undef dev_type_str
#undef draw
#undef file_mmap
#undef init_vk
#undef instance_create
#undef instance_exts_dump
#undef instance_layers_dump
#undef phydev_exts_dump
#undef phydev_init
#undef phydev_mem_heap_dump
#undef phydev_mem_type_dump
#undef pl_create
#undef pl_layout_empty_create
#undef present_mode_to_str
#undef q_get
#undef render
#undef rp_create
#undef run
#undef sems_create
#undef shmods_create
#undef shmods_destroy
#undef surf_create
#undef surf_init
#undef swpchn_acquire_next_img
#undef swpchn_fb_create
#undef swpchn_fbs_create
#undef swpchn_imgs_get
#undef swpchn_imgview_create
#undef swpchn_imgviews_create
#undef swpchn_init
#undef texel_mem_blk_conf_select
#undef texel_mem_blk_confs_dump
#undef tmp_phydev_and_q_fam_select
#undef tmp_phydev_mem_heap_dump
#undef tmp_phydev_mem_heaps_dump
#undef tmp_phydev_mem_props_dump
#undef tmp_phydev_mem_props_get
#undef tmp_phydev_mem_type_dump
#undef tmp_phydev_mem_types_dump
#undef tmp_phydev_q_fams_get
#undef tmp_phydev_q_fams_dump
#undef tmp_phydevs_mem_props_dump
#undef tmp_phydevs_mem_props_get
#undef tmp_phydevs_props_dump
#undef tmp_phydevs_q_fams_get
#undef tmp_phydevs_q_fams_dump
#undef tmp_phydevs_q_fams_surf_support_get
#undef tmp_phydevs_get
#undef tmp_present_modes_dump
#undef tmp_present_modes_get
#undef tmp_selected_phydev_cherry_pick
#undef tmp_surf_caps_dump
#undef tmp_surf_caps_get
#undef uuid_str
#endif
