#ifndef APP_C
#define APP_C
/*
 * this is public domain without any warranties of any kind
 * Sylvain BERTRAND
 */
/* XXX: KEEP AN EYE ON ABBREVIATIONS, ALWAYS */
/* C */
#include <stdlib.h>
#include <string.h>
/* posix, should move that to C */
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
/* x11 */
#include <xcb.h>
#include "app_core_types.h"
#include "nyanvk/consts.h"
#include "nyanvk/types.h"
#include "vk_app.h"
#include "app_state_types.h"
#include "log.h"
#include "vk_syms.c"
#include "app_state.c"
#include "xcb_syms.c"
#include "xcb.c"
/*---------------------------------------------------------------------------*/
#include "namespace/app.c"
#include "namespace/vk_syms.c"
#include "namespace/app_state_types.h"
#include "namespace/app_state.c"
/*---------------------------------------------------------------------------*/
/*
 * In "wc", 'w' is the "standard" 4th 3d coord, and 'c' stands for 'C'lipped.
 * the clip coords in vk is a 3d "cube":
 * -"wc" <= x <= "wc" (w is the "standard" 4th coord, n
 * -"wc" <= y <= "wc"
 * 0.0f <= z <= "wc"
 * y axis is pointing down
 * x axis is pointing to the right
 * z axis is pointing away from you
 * "wc" is from the "projection matrix" _WITHOUT_ the "wc" division (you must
 * understand the related 3d maths).
 * 
 *                     Z
 *                     ^
 *                    /
 *                   /
 *    center(0,0,0) +------->
 *                  |       X
 *                  |
 *                  | 
 *                  v Y
 *                  
 *
 * the vtx position which is output by the vtx sh must be in those
 * "clip coords", because the fixed-function GPU hardware will expect it.
 * You must understand the different 3d coords from a math point of view.
 *
 * In "3d" we have 4 coords, x,y, z, w due to the fact
 * that "perpective projection", with linear adjustment of z, is a linear
 * transformation only in 4D. Linear adjustment of z is VERY important
 * or you won't be able to write this projection as a 4x4 matrix.
 * This is a "math trick", to allow classic 4x4 linear algebra to just "work".
 *
 * "clip coords" means the final w division for a complete "perspective
 * projection" was not done. See the vk specs, "chapter 23.2 primitive
 * clipping" (at the time of writing).
 * 
 */
#define VK_FATAL(fmt, ...) \
if (r < 0) {\
	LOG(fmt, ##__VA_ARGS__);\
	exit(1);\
}

#define FATAL(fmt, ...) \
{\
	LOG(fmt, ##__VA_ARGS__);\
	exit(1);\
}
static void file_mmap(u8 *path, u8 **map, u64 *sz)
{
	struct stat file_stat;
	int r;
	int fd;

	r = stat(path, &file_stat);
	if (r != 0)
		FATAL("0:MAIN:FATAL:%d:file_mmap:%s:error stating the file\n", r, path)
	*sz = (u64)file_stat.st_size;
	fd = open(path, O_RDONLY);
	if (fd ==  -1)
		FATAL("0:MAIN:FATAL:%d:file_mmap:%s:error openning the file\n", r, path)
	*map = mmap(0, *sz, PROT_READ, MAP_PRIVATE, fd, 0);
	if (*map == 0)
		FATAL("0:MAIN:FATAL:%d:file_mmap:%s:fd=%d:error mmaping the file\n", r, path, fd)
}
/* the phydev q fam selected */
static void dev_create(void)
{
	struct vk_dev_create_info_t info;
	struct vk_dev_q_create_info_t q_info;
	float q_prio;
	/* XXX: most of the used exts are actually api consistency fixes */
	static u8 *exts[] = {
		/*------------------------------------------------------------*/
		"VK_KHR_multiview",
		"VK_KHR_maintenance2",	
		"VK_KHR_create_renderpass2",
		/*------------------------------------------------------------*/
		"VK_KHR_bind_memory2",
		/*------------------------------------------------------------*/
		"VK_KHR_get_memory_requirements2",
		"VK_KHR_swapchain"};
	s32 r;

	memset(&info, 0, sizeof(info));
	memset(&q_info, 0, sizeof(q_info));
	/*--------------------------------------------------------------------*/
	q_info.type = vk_struct_type_dev_q_create_info;
	q_info.q_fam = surf_g.dev.phydev.q_fam;
	q_info.qs_n = 1;
	q_info.q_prios = &q_prio;
	q_prio = 1.0f;
	/*--------------------------------------------------------------------*/
	info.type = vk_struct_type_dev_create_info;
	info.q_create_infos_n = 1;
	info.q_create_infos = &q_info;
	info.enabled_exts_n = ARRAY_N(exts);
	info.enabled_ext_names = exts;
	vk_create_dev(&info);
	VK_FATAL("0:MAIN:FATAL:%d:physical device:%p:unable to create a vulkan device\n", r, surf_g.dev.phydev.vk)
	LOG("0:MAIN:physical device:%p:vulkan device created with one proper queue:%p\n", surf_g.dev.phydev.vk, surf_g.dev.vk);
}

static void instance_create(void)
{
	s32 r;
	struct vk_instance_create_info_t info;
	/* XXX: most of the used exts are actually api consistency fixes */
	static u8 *exts[] = {
		"VK_KHR_get_surface_capabilities2",
		/*------------------------------------------------------------*/
		"VK_KHR_get_physical_device_properties2",
		"VK_KHR_xcb_surface",
		"VK_KHR_surface"};
	u32 i;

	i = 0;
	loop {
		if (i == ARRAY_N(exts))
			break;
		LOG("0:MAIN:will use vulkan instance_g extension %s\n", exts[i]);
		++i;
	}
	memset(&info, 0, sizeof(info));
	info.type = vk_struct_type_instance_create_info;
	info.enabled_exts_n = ARRAY_N(exts);
	info.enabled_ext_names = exts;
	vk_create_instance(&info);
	VK_FATAL("0:MAIN:FATAL:%d:unable to create a vulkan instance_g\n", r)
	LOG("0:MAIN:vulkan instance_g handle %p\n", instance_g);
}

/* in theory, this could change on the fly */
static void instance_exts_dump(void)
{
#define EXTS_N_MAX 512
	struct vk_ext_props_t exts[EXTS_N_MAX];
	u32 n;
	s32 r;

	memset(exts, 0, sizeof(exts));
	n = EXTS_N_MAX;
	vk_enumerate_instance_ext_props(&n, exts);
	if (r != vk_success && r != vk_incomplete) {
		LOG("0:MAIN:ERROR:%d:unable to enumerate instance_g extension(s)\n", r);
		return;
	} 
	if (r == vk_incomplete) {
		LOG("0:MAIN:ERROR:too many extensions (%u/%u), dumping disabled", n, EXTS_N_MAX);
		return;
	}
	/* vk_success */
	LOG("0:MAIN:have %u instance_g extension(s)\n", n);
	loop {
		if (n == 0)
			break;
		LOG("0:MAIN:instance_g extension:name=%s:specification version=%u\n", exts[n - 1].name, exts[n - 1].spec_version);
		n--;
	}
#undef EXTS_N_MAX
}

/* in theory, this could change on the fly */
static void instance_layers_dump(void)
{
#define LAYERS_N_MAX 32
	struct vk_layer_props_t layers[LAYERS_N_MAX];
	u32 n;
	s32 r;

	memset(layers, 0, sizeof(layers));
	n = LAYERS_N_MAX;
	vk_enumerate_instance_layer_props(&n, layers);
	if (r != vk_success && r != vk_incomplete) {
		LOG("0:MAIN:ERROR:%d:unable to enumerate instance_g layer(s)\n", r);
		return;
	}
	if (r == vk_incomplete) {
		LOG("0:MAIN:ERROR:too many layers (%u/%u), dumping disabled", n, LAYERS_N_MAX);
		return;
	}
	/* vk_success */
	LOG("0:MAIN:have %u instance_g layer(s)\n", n);
	loop {
		if (n == 0)
			break;
		LOG("0:MAIN:instance_g layer:%u:name=%s:specification version=%u:implementation version=%u:description=%s\n", n, layers[n].name, layers[n].spec_version, layers[n].implementation_version, layers[n].desc);
		n--;
	}
#undef LAYERS_N_MAX
}

static void tmp_phydevs_get(void)
{
	void *phydevs[tmp_phydevs_n_max];
	u32 n;
	s32 r;

	memset(phydevs, 0, sizeof(phydevs));
	n = tmp_phydevs_n_max;
	vk_enumerate_phydevs(&n, phydevs);
	if (r != vk_success && r != vk_incomplete)
		FATAL("0:MAIN:FATAL:%ld:unable to enumerate physical devices\n",r)
	if (r == vk_incomplete)
		FATAL("0:MAIN:FATAL:too many vulkan physical devices %u/%u for our temporary storage\n", n, tmp_phydevs_n_max)
	/* vk_success */
	LOG("0:MAIN:detected %u physical devices\n", n);
	if (n == 0)
		FATAL("0:MAIN:no vulkan physical devices, exiting\n")
	tmp_phydevs_n_g = n;
	memset(tmp_phydevs_g, 0, sizeof(tmp_phydevs_g));
	n = 0;	
	loop {
		if (n == tmp_phydevs_n_g)
			break;
		tmp_phydevs_g[n].vk = phydevs[n];
		++n;
	};
}

static void phydev_exts_dump(void *phydev)
{
#define EXTS_N_MAX 512
	struct vk_ext_props_t exts[EXTS_N_MAX];
	u32 n;
	s32 r;

	memset(exts, 0, sizeof(exts));
	n = EXTS_N_MAX;
	vk_enumerate_dev_ext_props(phydev, &n, exts);
	if (r != vk_success && r != vk_incomplete) {
		LOG("0:MAIN:ERROR:physical device:%p:%d:unable to enumerate device extension(s)\n", phydev, r);
		return;
	} 
	if (r == vk_incomplete) {
		LOG("0:MAIN:ERROR:physical device:%p:too many extensions (%u/%u), dumping disabled", phydev, n, EXTS_N_MAX);
		return;
	}
	/* vk_success */
	LOG("0:MAIN:physical device:%p:have %u device extension(s)\n", phydev, n);
	loop {
		if (n == 0)
			break;
		LOG("0:MAIN:physical device:%p:device extension:name=%s:specification version=%u\n", phydev, exts[n - 1].name, exts[n - 1].spec_version);
		n--;
	}
#undef EXTS_N_MAX
}

static void tmp_phydevs_exts_dump(void)
{
	u8 i;

	i = 0;
	loop {
		if (i == tmp_phydevs_n_g)
			break;
		phydev_exts_dump(tmp_phydevs_g[i].vk);
		++i;
	}
}

static u8 *dev_type_str(u32 type)
{
	switch (type) {
	case vk_phydev_type_other:
		return "other";
	case vk_phydev_type_integrated_gpu:
		return "integrated gpu";
	case vk_phydev_type_discrete_gpu:
		return "discrete gpu";
	case vk_phydev_type_virtual_gpu:
		return "virtual gpu";
	case vk_phydev_type_cpu:
		return "cpu";
	default:
		return "UNKNOWN";
	}
}

static u8 *uuid_str(u8 *uuid)
{
	static u8 uuid_str[VK_UUID_SZ * 2 + 1];
	u8 i;

	memset(uuid_str, 0, sizeof(uuid_str));
	i = 0;
	loop {
		if (i == VK_UUID_SZ)
			break;
		/* XXX: always write a terminating  0, truncated or not */
		snprintf(uuid_str + i * 2, 3, "%02x", uuid[i]);
		++i;
	}	
	return uuid_str;
}

static void tmp_phydevs_props_dump(void)
{
	u32 i;

	i = 0;
	loop {
		struct vk_phydev_props_t props;
		struct tmp_phydev_t *p;

		if (i == tmp_phydevs_n_g)
			break;
		p = &tmp_phydevs_g[i];
		memset(&props, 0, sizeof(props));
		props.type = vk_struct_type_phydev_props;
		vk_get_phydev_props(p->vk, &props);
		LOG("0:MAIN:physical device:%p:properties:api version=%#x=%u.%u.%u\n", p->vk, props.core.api_version, VK_VERSION_MAJOR(props.core.api_version), VK_VERSION_MINOR(props.core.api_version), VK_VERSION_PATCH(props.core.api_version));
		LOG("0:MAIN:physical device:%p:properties:driver version=%#x=%u.%u.%u\n", p->vk, props.core.driver_version, VK_VERSION_MAJOR(props.core.driver_version), VK_VERSION_MINOR(props.core.driver_version), VK_VERSION_PATCH(props.core.driver_version));
		LOG("0:MAIN:physical device:%p:properties:vendor id=%#x\n", p->vk, props.core.vendor_id);
		LOG("0:MAIN:physical device:%p:properties:device id=%#x\n", p->vk, props.core.dev_id);
		LOG("0:MAIN:physical device:%p:properties:type=%s\n", p->vk, dev_type_str(props.core.dev_type));
		if (props.core.dev_type == vk_phydev_type_discrete_gpu)
			p->is_discret_gpu = true;
		else
			p->is_discret_gpu = false;
		LOG("0:MAIN:physical device:%p:properties:name=%s\n", p->vk, props.core.name);
		LOG("0:MAIN:physical device:%p:properties:pipeline cache uuid=%s\n", p->vk, uuid_str(props.core.pl_cache_uuid));
		/* display the limits and sparse props at log level 1, if needed */
		++i;
	}
}

static void tmp_phydev_q_fams_get(struct tmp_phydev_t *p)
{
	u8 i;
	u32 n;

	n = 0;
	vk_get_phydev_q_fam_props(p->vk, &n, 0);
	if (n > tmp_phydev_q_fams_n_max)
		FATAL("0:MAIN:FATAL:physical device:%p:too many queue families %u/%u\n", p->vk, n, tmp_phydev_q_fams_n_max)
	memset(p->q_fams, 0, sizeof(p->q_fams));
	i = 0;
	loop {
		if (i == tmp_phydev_q_fams_n_max)
			break;
		p->q_fams[i].type = vk_struct_type_q_fam_props;
		++i;
	}
	vk_get_phydev_q_fam_props(p->vk, &n, p->q_fams);
	p->q_fams_n = n;
	LOG("0:MAIN:physical device:%p:have %u queue families\n", p->vk, p->q_fams_n);
}

static void tmp_phydevs_q_fams_get(void)
{
	u8 i;

	i = 0;
	loop {
		if (i == tmp_phydevs_n_g)
			break;
		tmp_phydev_q_fams_get(&tmp_phydevs_g[i]);
		++i;
	}
}

static void tmp_phydev_q_fams_dump(struct tmp_phydev_t *p)
{
	u8 i;

	i = 0;
	loop {
		if (i == p->q_fams_n)
			break;
		if ((p->q_fams[i].core.flags & vk_q_gfx_bit) != 0)
			LOG("0:MAIN:physical device:%p:queue family:%u:flags:graphics\n", p->vk, i);
		if ((p->q_fams[i].core.flags & vk_q_compute_bit) != 0)
			LOG("0:MAIN:physical device:%p:queue family:%u:flags:compute\n", p->vk, i);
		if ((p->q_fams[i].core.flags & vk_q_transfer_bit) != 0)
			LOG("0:MAIN:physical device:%p:queue family:%u:flags:transfer\n", p->vk, i);
		if ((p->q_fams[i].core.flags & vk_q_sparse_binding_bit) != 0)
			LOG("0:MAIN:physical device:%p:queue family:%u:flags:sparse binding\n", p->vk, i);
		if ((p->q_fams[i].core.flags & vk_q_protected_bit) != 0)
			LOG("0:MAIN:physical device:%p:queue family:%u:flags:protected\n", p->vk, i);
		LOG("0:MAIN:physical device:%p:queue family:%u:%u queues\n", p->vk, i, p->q_fams[i].core.qs_n);
		LOG("0:MAIN:physical device:%p:queue family:%u:%u bits timestamps\n", p->vk, i, p->q_fams[i].core.timestamp_valid_bits);
		LOG("0:MAIN:physical device:%p:queue family:%u:(width=%u,height=%u,depth=%u) minimum image transfer granularity\n", p->vk, i, p->q_fams[i].core.min_img_transfer_granularity.width, p->q_fams[i].core.min_img_transfer_granularity.height, p->q_fams[i].core.min_img_transfer_granularity.depth);
		++i;
	}
}

static void cp_create(void)
{
	s32 r;
	struct vk_cp_create_info_t info;

	memset(&info, 0, sizeof(info));
	info.type = vk_struct_type_cp_create_info;
	info.flags = vk_cp_create_reset_cb_bit;
	info.q_fam = surf_g.dev.phydev.q_fam;
	vk_create_cp(&info);
	VK_FATAL("0:MAIN:FATAL:%d:unable create the commmand pool\n", r)
	LOG("0:MAIN:device:%p:queue family:%u:created command pool %p\n", surf_g.dev.vk, surf_g.dev.phydev.q_fam, surf_g.dev.cp);
}

static void tmp_phydevs_q_fams_dump(void)
{
	u8 i;

	i = 0;
	loop {
		if (i == tmp_phydevs_n_g)
			break;
		tmp_phydev_q_fams_dump(&tmp_phydevs_g[i]);
		++i;
	}
}

static void q_get(void)
{
	LOG("0:MAIN:device:%p:getting queue:family=%u queue=0\n", surf_g.dev.vk, surf_g.dev.phydev.q_fam);
	vk_get_dev_q();
	LOG("0:MAIN:device:%p:got queue:%p\n", surf_g.dev.vk, surf_g.dev.q);
}

static void check_vk_version(void)
{
	u32 api_version;
	s32 r;

	vk_enumerate_instance_version(&api_version);
	if (r != vk_success)
		FATAL("0:MAIN:FATAL:%d:unable to enumerate instance_g version\n", r)
	LOG("0:MAIN:vulkan instance_g version %#x = %u.%u.%u\n", api_version, VK_VERSION_MAJOR(api_version), VK_VERSION_MINOR(api_version), VK_VERSION_PATCH(api_version));
	if (VK_VERSION_MAJOR(api_version) == 1
					&& VK_VERSION_MINOR(api_version) == 0)
		FATAL("0:MAIN:FATAL:vulkan instance_g version too old\n")
}
/*
 * the major obj to use in vk abstraction of gfx hardware is the q. In this
 * abstraction, many core objs like bufs/imgs are "own" by a specific q, and
 * transfer of such ownership to other qs can be expensive. we know it's not
 * really the case on AMD hardware, but if vk abstraction insists on this, it
 * probably means it is important on some hardware of other vendors.
 */
static void tmp_phydevs_q_fams_surf_support_get(void)
{
	u8 i;

	i = 0;
	loop {
		struct tmp_phydev_t *p;
		u8 j;

		if (i == tmp_phydevs_n_g)
			break;
		p = &tmp_phydevs_g[i];
		j = 0;
		loop {
			s32 r;
			u32 supported;

			if (j == p->q_fams_n)
				break;
			supported = vk_false;
			vk_get_phydev_surf_support(p->vk, j, &supported);
			VK_FATAL("0:MAIN:FATAL:%d:physical device:%p:queue family:%u:surface:%p:unable to query queue family wsi/(image presentation to our surface) support\n", r, p->vk, j, surf_g.vk);
			if (supported == vk_true) {
				LOG("0:MAIN:physical device:%p:queue family:%u:surface:%p:does support wsi/(image presentation to our surface) \n", p->vk, j, surf_g.vk);
				p->q_fams_surf_support[j] = true;
			} else {
				LOG("0:MAIN:physical device:%p:queue family:%u:surface:%p:does not support wsi/(image presentation to our surface)\n", p->vk, j, surf_g.vk);
				p->q_fams_surf_support[j] = false; 
			}
			++j;
		}
		++i;
	}
}

static void tmp_selected_phydev_cherry_pick(u8 i)
{
	struct tmp_phydev_t *p;

	p = &tmp_phydevs_g[i];
	surf_g.dev.phydev.vk = p->vk;
	surf_g.dev.phydev.is_discret_gpu = p->is_discret_gpu;
	surf_g.dev.phydev.mem_types_n = p->mem_props.core.mem_types_n;
	memcpy(surf_g.dev.phydev.mem_types, p->mem_props.core.mem_types,
					sizeof(surf_g.dev.phydev.mem_types));
}

/*
 * we ask qs of phydevs which one is able to present imgs to the
 * external pe surf_g. Additionally we require this q to support gfx. we
 * select basically the first q from the first phydev fitting what we are
 * looking for.
 */
static void tmp_phydev_and_q_fam_select(void)
{
	u8 i;
		
	i = 0;
	loop {
		u8 j;
		struct tmp_phydev_t *p;

		if (i == tmp_phydevs_n_g)
			break;
		p = &tmp_phydevs_g[i];
		j = 0;
		loop {
			if (j == p->q_fams_n)
				break;
			/*
			 * we are looking for a q fam with:
			 *  - img presentation to our surf_g
			 *  - gfx
			 *  - transfer (implicit with gfx)
			 */
			if (p->q_fams_surf_support[j]
				&& (p->q_fams[j].core.flags & vk_q_gfx_bit)
									!= 0) {
				surf_g.dev.phydev.q_fam = j;
				tmp_selected_phydev_cherry_pick(i);
				LOG("0:MAIN:physical device %p selected for (wsi/image presentation to our surface %p) using its queue family %u\n", surf_g.dev.phydev.vk, surf_g.vk, surf_g.dev.phydev.q_fam);
				return;
			}
			++j;
		}
		++i;
	}
}

/*
 * XXX: the surf_g is an obj at the instance_g lvl, NOT THE [PHYSICAL]
 * DEV LVL.
 */
static void surf_create(void)
{
	struct vk_xcb_surf_create_info_t xcb_info;
	s32 r;

	memset(&surf_g, 0, sizeof(surf_g));
	memset(&xcb_info, 0, sizeof(xcb_info));
	xcb_info.type = vk_struct_type_xcb_surf_create_info;
	xcb_info.c = app_xcb.c;
	xcb_info.win = app_xcb.win_id;
	vk_create_xcb_surf(&xcb_info);
	VK_FATAL("0:MAIN:FATAL:%d:xcb:%s:screen:%d:root window id:%#x:window id:%#x:unable to create a vulkan surface from this x11 window\n", r, app_xcb.disp_env, app_xcb.scr_idx, app_xcb.scr->root, app_xcb.win_id)
	LOG("0:MAIN:xcb:'%s':screen:%d:root window id:%#x:window id:%#x:created vk_surface=%p\n", app_xcb.disp_env, app_xcb.scr_idx, app_xcb.scr->root, app_xcb.win_id, surf_g.vk);
}

static void texel_mem_blk_confs_dump(u32 confs_n,
				struct vk_surf_texel_mem_blk_conf_t *confs)
{
	u32 i;

	i = 0;
	loop {
		if (i == confs_n)
			break;
		LOG("0:MAIN:physical device:%p:surface:%p:texel memory block configuration:format=%u color_space=%u\n", surf_g.dev.phydev.vk, surf_g.vk, confs[i].core.fmt, confs[i].core.color_space);
		++i;
	}
}

/*
 * we only know this phydev/q is "able to present imgs" to the external
 * pe surf_g. Here we choose the conf of textel blk
 */
#define CONFS_N_MAX 1024
static void texel_mem_blk_conf_select(void)
{
	struct vk_phydev_surf_info_t info;
	struct vk_surf_texel_mem_blk_conf_t confs[CONFS_N_MAX];
	struct vk_surf_texel_mem_blk_conf_core_t *cc;
	s32 r;
	u32 confs_n;
	u32 i;

	memset(&info, 0, sizeof(info));
	info.type = vk_struct_type_phydev_surf_info;
	info.surf = surf_g.vk;
	vk_get_phydev_surf_texel_mem_blk_confs(&info, &confs_n, 0);
	VK_FATAL("0:MAIN:FATAL:%d:physical device:%p:surface:%p:unable get the count of valid surface texel memory block configurations\n", r, surf_g.dev.phydev.vk, surf_g.vk)
	if (confs_n > CONFS_N_MAX)
		FATAL("0:MAIN:FATAL:physical device:%p:surface:%p:too many surface texel memory block configurations %u/%u\n", surf_g.dev.phydev.vk, surf_g.vk, confs_n, CONFS_N_MAX)
	memset(confs, 0, sizeof(confs[0]) * confs_n);
	i = 0;
	loop {
		if (i == confs_n)
			break;
		confs[i].type = vk_struct_type_surf_texel_mem_blk_conf;
		++i;
	}	
	vk_get_phydev_surf_texel_mem_blk_confs(&info, &confs_n, confs);
	VK_FATAL("0:MAIN:FATAL:%d:physical device:%p:surface:%p:unable get the valid surface texel memory block configurations\n", r, surf_g.dev.phydev.vk, surf_g.vk)
	if (confs_n == 0)
		FATAL("0:MAIN:FATAL:physical device:%p:surface:%p:no valid surface texel memory block configuration\n", surf_g.dev.phydev.vk, surf_g.vk)
	texel_mem_blk_confs_dump(confs_n, confs);
	cc = &surf_g.dev.phydev.selected_texel_mem_blk_conf_core;	
	if ((confs_n == 1) && (confs[0].core.fmt
					== vk_texel_mem_blk_fmt_undefined)) {
		/* this means the dev let us choose our the fmt */
		cc->fmt = vk_texel_mem_blk_fmt_b8g8r8a8_srgb;
		LOG("0:MAIN:physical device:%p:surface:%p:using our surface texel memory block format %u\n", surf_g.dev.phydev.vk, surf_g.vk, cc->fmt);
		cc->color_space = vk_color_space_srgb_nonlinear;
		LOG("0:MAIN:physical device:%p:surface:%p:using prefered surface texel memory block color space %u\n", surf_g.dev.phydev.vk, surf_g.vk, cc->color_space);
	} else {
		/* the first valid fmt is the prefered fmt */
		surf_g.dev.phydev.selected_texel_mem_blk_conf_core.fmt =
							confs[0].core.fmt;
		LOG("0:MAIN:physical device:%p:surface:%p:using prefered surface texel memory block format %u\n", surf_g.dev.phydev.vk, surf_g.vk, surf_g.dev.phydev.selected_texel_mem_blk_conf_core.fmt);
		cc->color_space = confs[0].core.color_space;
		LOG("0:MAIN:physical device:%p:surface:%p:using prefered surface texel memory block color space %u\n", surf_g.dev.phydev.vk, surf_g.vk, cc->color_space);
	}
}

static void tmp_phydev_mem_props_get(struct tmp_phydev_t *p)
{
	memset(&p->mem_props, 0, sizeof(p->mem_props));
	p->mem_props.type = vk_struct_type_phydev_mem_props;
	vk_get_phydev_mem_props(p->vk, &p->mem_props);
}

static void tmp_phydevs_mem_props_get(void)
{
	u8 i;

	i = 0;
	loop {
		if (i == tmp_phydevs_n_g)
			break;
		tmp_phydev_mem_props_get(&tmp_phydevs_g[i]);
		++i;
	}
}

static void phydev_mem_heap_dump(void *phydev, u8 i, struct vk_mem_heap_t *heap)
{
	LOG("0:MAIN:physical device:%p:memory heap:%u:size:%u bytes\n", phydev, i, heap->sz);
	LOG("0:MAIN:physical device:%p:memory heap:%u:flags:%#08x\n", phydev, i, heap->flags);
	if ((heap->flags & vk_mem_heap_dev_local_bit) != 0)
		LOG("0:MAIN:physical device:%p:memory heap:%u:device local\n", phydev, i);
	if ((heap->flags & vk_mem_heap_multi_instance_bit) != 0)
		LOG("0:MAIN:physical device:%p:memory type:%u:multi instance_g\n", phydev, i);
}

static void phydev_mem_type_dump(void *phydev, u8 i, struct vk_mem_type_t *type)
{
	LOG("0:MAIN:physical device:%p:memory type:%u:heap:%u\n", phydev, i, type->heap);
	LOG("0:MAIN:physical device:%p:memory type:%u:flags:%#08x\n", phydev, i, type->prop_flags);
	if ((type->prop_flags & vk_mem_prop_dev_local_bit) != 0)
		LOG("0:MAIN:physical device:%p:memory type:%u:device local\n", phydev, i);
	if ((type->prop_flags & vk_mem_prop_host_visible_bit) != 0)
		LOG("0:MAIN:physical device:%p:memory type:%u:host visible\n", phydev, i);
	if ((type->prop_flags & vk_mem_prop_host_cached_bit) != 0)
		LOG("0:MAIN:physical device:%p:memory type:%u:host cached\n", phydev, i);
}

static void tmp_phydev_mem_types_dump(struct tmp_phydev_t *p)
{
	u8 i;

	LOG("0:MAIN:physical device:%p:%u memory types\n", p->vk, p->mem_props.core.mem_types_n);
	i = 0;
	loop {
		if (i == p->mem_props.core.mem_types_n)
			break;
		phydev_mem_type_dump(p->vk, i,
					&p->mem_props.core.mem_types[i]);
		++i;
	}
}

static void tmp_phydev_mem_heaps_dump(struct tmp_phydev_t *p)
{
	u8 i;

	LOG("0:MAIN:physical device:%p:%u memory heaps\n", p->vk, p->mem_props.core.mem_heaps_n);
	i = 0;
	loop {
		if (i == p->mem_props.core.mem_heaps_n)
			break;
		phydev_mem_heap_dump(p->vk, i,
					&p->mem_props.core.mem_heaps[i]);
		++i;
	}

}

static void tmp_phydev_mem_props_dump(struct tmp_phydev_t *p)
{
	tmp_phydev_mem_types_dump(p);
	tmp_phydev_mem_heaps_dump(p);
}

static void tmp_phydevs_mem_props_dump(void)
{
	u8 i;

	i = 0;
	loop {
		if (i == tmp_phydevs_n_g)
			break;
		tmp_phydev_mem_props_dump(&tmp_phydevs_g[i]);
		++i;
	}
}

static void tmp_surf_caps_get(void)
{
	s32 r;
	struct vk_phydev_surf_info_t info;

	memset(&info, 0, sizeof(info));
	info.type = vk_struct_type_phydev_surf_info;
	info.surf = surf_g.vk;
	memset(&tmp_surf_caps_g, 0, sizeof(tmp_surf_caps_g));
	tmp_surf_caps_g.type = vk_struct_type_surf_caps;
	vk_get_phydev_surf_caps(&info, &tmp_surf_caps_g);
	VK_FATAL("0:MAIN:FATAL:%d:physical device:%p:surface:%p:unable to get our surface capabilities in the context of the selected physical device\n", r, surf_g.dev.phydev.vk, surf_g.vk)
	/* we have room for a maximum of 3 images per swapchain */
	if (tmp_surf_caps_g.core.imgs_n_min > swpchn_imgs_n_max)
		FATAL("0:MAIN:FATAL:physical device:%p:surface:%p:we have room for %u images per swapchain, but this swapchain requires a minimum of %u images\n", surf_g.dev.phydev.vk, surf_g.vk, swpchn_imgs_n_max, tmp_surf_caps_g.core.imgs_n_min)
}

static void tmp_surf_caps_dump(void)
{
	LOG("0:MAIN:physical device:%p:surface:%p:imgs_n_min=%u\n", surf_g.dev.phydev.vk, surf_g.vk, app_tmp_surf_caps.core.imgs_n_min);
	LOG("0:MAIN:physical device:%p:surface:%p:imgs_n_max=%u\n", surf_g.dev.phydev.vk, surf_g.vk, tmp_surf_caps_g.core.imgs_n_max);
	LOG("0:MAIN:physical device:%p:surface:%p:current extent=(width=%u, height=%u)\n", surf_g.dev.phydev.vk, surf_g.vk, tmp_surf_caps_g.core.current_extent.width, tmp_surf_caps_g.core.current_extent.height);
	LOG("0:MAIN:physical device:%p:surface:%p:minimal extent=(width=%u, height=%u)\n", surf_g.dev.phydev.vk, surf_g.vk, tmp_surf_caps_g.core.img_extent_min.width, tmp_surf_caps_g.core.img_extent_min.height);
	LOG("0:MAIN:physical device:%p:surface:%p:maximal extent=(width=%u, height=%u)\n", surf_g.dev.phydev.vk, surf_g.vk, tmp_surf_caps_g.core.img_extent_max.width, tmp_surf_caps_g.core.img_extent_max.height);
	LOG("0:MAIN:physical device:%p:surface:%p:img_array_layers_n_max=%u\n", surf_g.dev.phydev.vk, surf_g.vk, tmp_surf_caps_g.core.img_array_layers_n_max);
	LOG("0:MAIN:physical device:%p:surface:%p:supported_transforms=%#08x\n", surf_g.dev.phydev.vk, surf_g.vk, tmp_surf_caps_g.core.supported_transforms);
	LOG("0:MAIN:physical device:%p:surface:%p:current_transform=%#08x\n", surf_g.dev.phydev.vk, surf_g.vk, tmp_surf_caps_g.core.current_transform);
	LOG("0:MAIN:physical device:%p:surface:%p:supported_composite_alpha=%#08x\n", surf_g.dev.phydev.vk, surf_g.vk, tmp_surf_caps_g.core.supported_composite_alpha);
	LOG("0:MAIN:physical device:%p:surface:%p:supported_img_usage_flags=%#08x\n", surf_g.dev.phydev.vk, surf_g.vk, tmp_surf_caps_g.core.supported_img_usage_flags);
}

static void swpchn_imgs_get(void)
{
	s32 r;

	surf_g.dev.swpchn.imgs_n = swpchn_imgs_n_max;
	vk_get_swpchn_imgs();
	VK_FATAL("0:MAIN:FATAL:%d:device:%p:surface:%p:swapchain:%p:unable to get the swapchain images\n", r, surf_g.dev.vk, surf_g.vk, surf_g.dev.swpchn.vk)
	LOG("0:MAIN:device:%p:surface:%p:swapchain:%p:got %u swapchain images\n", surf_g.dev.vk, surf_g.vk, surf_g.dev.swpchn.vk, surf_g.dev.swpchn.imgs_n);
}

static void swpchn_imgview_create(u8 i)
{
	struct vk_imgview_create_info_t info;
	s32 r;

	memset(&info, 0, sizeof(info));
	info.type = vk_struct_type_imgview_create_info;
	info.img = surf_g.dev.swpchn.imgs[i];
	info.view_type = vk_imgview_type_2d;
	info.fmt = surf_g.dev.phydev.selected_texel_mem_blk_conf_core.fmt;
	info.subrsrc_range.aspect = vk_img_aspect_color_bit;
	info.subrsrc_range.lvls_n = 1;
	info.subrsrc_range.array_layers_n = 1;
	vk_create_imgview(&info, &surf_g.dev.swpchn.imgviews[i]);
	VK_FATAL("0:MAIN:FATAL:%d:device:%p:swapchain:%p:image:%u:unable to create image view\n", r, surf_g.dev.vk, surf_g.dev.swpchn.vk, i)
	LOG("0:MAIN:device:%p:swapchain:%p:image:%u:image view created %p\n", surf_g.dev.vk, surf_g.dev.swpchn.vk, i, surf_g.dev.swpchn.imgviews[i]);
}

static void swpchn_imgviews_create(void)
{
	u8 i;

	i = 0;
	loop {
		if (i == surf_g.dev.swpchn.imgs_n)
			break;
		swpchn_imgview_create(i);
		++i;
	}
}

static void swpchn_init(void)
{
	struct vk_swpchn_create_info_t info;
	struct phydev_t *p;
	s32 r;

	memset(&info, 0, sizeof(info));
	p = &surf_g.dev.phydev;
	info.type = vk_struct_type_swpchn_create_info;
	info.surf = surf_g.vk;
	info.imgs_n_min = tmp_surf_caps_g.core.imgs_n_min;
	info.img_texel_mem_blk_fmt = p->selected_texel_mem_blk_conf_core.fmt;
	info.img_color_space = p->selected_texel_mem_blk_conf_core.color_space;
	memcpy(&info.img_extent, &tmp_surf_caps_g.core.current_extent,
						sizeof(info.img_extent));
	info.img_layers_n = 1;
	info.img_usage = vk_img_usage_color_attachment_bit
					| vk_img_usage_transfer_dst_bit;
	info.img_sharing_mode = vk_sharing_mode_exclusive;
	info.pre_transform = vk_surf_transform_identity_bit;
	info.composite_alpha = vk_composite_alpha_opaque_bit;
	info.present_mode = vk_present_mode_fifo;
	info.clipped = vk_true;
	vk_create_swpchn(&info, &surf_g.dev.swpchn.vk);
	VK_FATAL("0:MAIN:FATAL:%d:device:%p:surface:%p:unable to create the initial swapchain\n", r, surf_g.dev.vk, surf_g.vk)
	LOG("0:MAIN:device:%p:surface:%p:swapchain created %p\n", surf_g.dev.vk, surf_g.vk, surf_g.dev.swpchn.vk);
}

static void tmp_present_modes_get(void)
{
	s32 r;

	tmp_present_modes_n_g = tmp_present_modes_n_max;
	vk_get_phydev_surf_present_modes();
	VK_FATAL("0:MAIN:FATAL:%d:physical device:%p:surface:%p:unable to get the physical device present mode for our surface\n", r, surf_g.dev.phydev.vk, surf_g.vk)
}

static u8 *present_mode_to_str(u32 mode)
{
	switch (mode) {
	case vk_present_mode_immediate:
		return "immediate";
	case vk_present_mode_mailbox:
		return "mailbox";
	case vk_present_mode_fifo:
		return "fifo";
	case vk_present_mode_fifo_relaxed:
		return "fifo relaxed";
	default:
		return "unknown";
	}
}

static void tmp_present_modes_dump(void)
{
	u8 i;

	i = 0;
	LOG("0:MAIN:physical device:%p:surface:%p:%u present modes\n", surf_g.dev.phydev.vk, surf_g.vk, tmp_present_modes_n_g);
	loop {
		if (i == (u8)tmp_present_modes_n_g)
			break;
		LOG("0:MAIN:physical device:%p:surface:%p:present mode=%s\n", surf_g.dev.phydev.vk, surf_g.vk, present_mode_to_str(tmp_present_modes_g[i]));
		++i;
	}
}

static void sems_create(void)
{
	s32 r;
	struct vk_sem_create_info_t info;
	u8 sem;

	sem = 0;
	loop {
		if (sem == sems_n)
			break;
		memset(&info, 0, sizeof(info));
		info.type = vk_struct_type_sem_create_info;
		vk_create_sem(&info, &surf_g.dev.sems[sem]);
		VK_FATAL("0:MAIN:FATAL:%d:device:%p:unable to create a semaphore %u for our swapchain\n", r, surf_g.dev.vk, sem)
		LOG("0:MAIN:device:%p:semaphore %u for our swapchain created %p\n", surf_g.dev.vk, sem, surf_g.dev.sems[sem]);

		++sem;
	}
}

static void cbs_create(void)
{
	s32 r;
	struct vk_cb_alloc_info_t alloc_info;

	memset(&alloc_info, 0, sizeof(alloc_info));
	alloc_info.type = vk_struct_type_cb_alloc_info;
	alloc_info.cp = surf_g.dev.cp;
	alloc_info.lvl = vk_cb_lvl_primary;
	alloc_info.cbs_n = surf_g.dev.swpchn.imgs_n;
	vk_alloc_cbs(&alloc_info);
	VK_FATAL("0:MAIN:FATAL:%d:device:%p:unable to allocate command buffers for our swapchain images from %p command pool\n", r, surf_g.dev.vk, surf_g.dev.cp)
	LOG("0:MAIN:device:%p:allocated %u command buffers for our swapchain images from %p command pool\n", surf_g.dev.vk, surf_g.dev.swpchn.imgs_n, surf_g.dev.cp);
}

static void cb_rec(u8 i)
{
	s32 r;
	struct vk_cb_begin_info_t cb_info;
	struct vk_rp_begin_info_t rp_info;
	struct vk_sp_begin_info_t sp_begin_info;
	struct vk_sp_end_info_t sp_end_info;
	union vk_clr_val_t clr_val;
	/*--------------------------------------------------------------------*/
	memset(&cb_info, 0, sizeof(cb_info));
	cb_info.type = vk_struct_type_cb_begin_info;
	vk_begin_cb(surf_g.dev.cbs[i], &cb_info);
	VK_FATAL("0:MAIN:FATAL:%d:swapchain img:%u:command buffer:%p:unable to begin recording\n", r, i, surf_g.dev.cbs[i])
	/*--------------------------------------------------------------------*/
	/*XXX this is where the link between a rp and a fb is created */
	memset(&rp_info, 0, sizeof(rp_info));
	rp_info.type = vk_struct_type_rp_begin_info;
	rp_info.rp = surf_g.dev.rp;
	rp_info.fb = surf_g.dev.swpchn.fbs[i];
	rp_info.render_area.offset.x = 0;
	rp_info.render_area.offset.y = 0;
	rp_info.render_area.extent.width = APP_WIN_WIDTH;
	rp_info.render_area.extent.height = APP_WIN_HEIGHT;
	clr_val.color.f32s[0] = 0.0f;
	clr_val.color.f32s[1] = 0.0f;
	clr_val.color.f32s[2] = 0.0f;
	clr_val.color.f32s[3] = 1.0f;
	rp_info.clr_vals_n = 1;
	rp_info.clr_vals = &clr_val;
	/*--------------------------------------------------------------------*/
	memset(&sp_begin_info, 0, sizeof(sp_begin_info));
	sp_begin_info.type = vk_struct_type_sp_begin_info;
	sp_begin_info.contents = vk_sp_contents_inline;
	/*--------------------------------------------------------------------*/
	memset(&sp_end_info, 0, sizeof(sp_end_info));
	sp_end_info.type = vk_struct_type_sp_end_info;
	/*--------------------------------------------------------------------*/
	vk_cmd_begin_rp(surf_g.dev.cbs[i], &rp_info, &sp_begin_info);
	vk_cmd_bind_pl(surf_g.dev.cbs[i]);
	vk_cmd_draw(surf_g.dev.cbs[i], 3, 1, 0, 0);
	vk_cmd_end_rp(surf_g.dev.cbs[i], &sp_end_info);
	/*--------------------------------------------------------------------*/
	vk_end_cb(surf_g.dev.cbs[i]);
	VK_FATAL("0:MAIN:FATAL:%d:swapchain img:%u:command buffer:%p:unable to end recording\n", r, i, surf_g.dev.cbs[i])
}

static void cbs_rec(void)
{
	u8 i;

	i = 0;
	loop {
		if (i == surf_g.dev.swpchn.imgs_n)
			break;
		cb_rec(i);
		++i;
	}
}

static void phydev_init(void)
{
	tmp_phydevs_get();
	/*--------------------------------------------------------------------*/
	tmp_phydevs_exts_dump();
	tmp_phydevs_props_dump();
	tmp_phydevs_mem_props_get();
	tmp_phydevs_mem_props_dump();
	/*--------------------------------------------------------------------*/
	tmp_phydevs_q_fams_get();
	tmp_phydevs_q_fams_dump();
	tmp_phydevs_q_fams_surf_support_get();
	/*--------------------------------------------------------------------*/
	tmp_phydev_and_q_fam_select();
	/*--------------------------------------------------------------------*/
	texel_mem_blk_conf_select();
	/*--------------------------------------------------------------------*/
	tmp_surf_caps_get();
	tmp_surf_caps_dump();
	/*--------------------------------------------------------------------*/
	tmp_present_modes_get();
	tmp_present_modes_dump();
}

static void rp_create(void)
{
	s32 r;
	struct vk_rp_create_info_t info;
	struct vk_at_desc_t color_at; /* only one color at for all sps */
	struct vk_sp_desc_t sp; /* our only sp */
	struct vk_at_ref_t color_at_ref; /* for our only sp */

	memset(&info, 0, sizeof(info));
	memset(&sp, 0, sizeof(sp));
	memset(&color_at, 0, sizeof(color_at));
	memset(&color_at_ref, 0, sizeof(color_at_ref));
	/* our rp, namely all its supasses will need only 1 color at */
	color_at.type = vk_struct_type_at_desc;
	color_at.fmt = surf_g.dev.phydev.selected_texel_mem_blk_conf_core.fmt;
	color_at.samples_n = vk_samples_n_1_bit;
	color_at.load_op = vk_at_load_op_clr;
	color_at.store_op = vk_at_store_op_store;
	color_at.stencil_load_op = vk_at_load_op_dont_care;
	color_at.stencil_store_op = vk_at_store_op_dont_care;
	color_at.initial_layout = vk_img_layout_undefined;
	color_at.final_layout = vk_img_layout_present;
	/*
	 * our rp will include 1 sp which will use the above
	 * color at, hence we declare its ref
	 */
	color_at_ref.type = vk_struct_type_at_ref;
	color_at_ref.at = 0; /* idx of the only at in our rp */
	color_at_ref.layout = vk_img_layout_color_at_optimal;
	/*
	 * our only sp using a ref to the only color at. In the
 	 * frag/pixel shader, outputting to this attachment will have to be done
 	 * using "location 0" spirv decoration
	 */
	sp.type = vk_struct_type_sp_desc;
	sp.pl_bind_point = vk_pl_bind_point_gfx;
	sp.color_ats_n = 1;
	sp.color_ats = &color_at_ref;
	/*
	 * we use a full gfx pl which we sync with the presentation
 	 * engine using semaphore. Then default/automatic sps sync is ok.
	 */
	info.type = vk_struct_type_rp_create_info;
	info.ats_n = 1;
	info.ats = &color_at;
	info.sps_n = 1;
	info.sps = &sp;
	vk_create_rp(&info);
	VK_FATAL("0:MAIN:FATAL:%d:device:%p:unable to create the render pass\n", r, surf_g.dev.vk)
	LOG("0:MAIN:device:%p:created render pass %p\n", surf_g.dev.vk, surf_g.dev.rp);
}

static void shmods_create(void)
{
	s32 r;
	struct vk_shmod_create_info_t info;

	/* vs */
	file_mmap("./vs.spirv", (u8**)&tmp_vs_g, &tmp_vs_sz_g);
	memset(&info, 0, sizeof(info));
	info.type = vk_struct_type_shmod_create_info;
	info.code_sz = tmp_vs_sz_g;
	info.code = tmp_vs_g;
	vk_create_shmod(&info, &tmp_vs_mod_g);
	VK_FATAL("0:MAIN:FATAL:%d:device:%p:unable to create the vertex shader module", r, surf_g.dev.vk)
	LOG("0:MAIN:device:%p:created vertex shader module %p\n", surf_g.dev.vk, tmp_vs_mod_g);
	/* fs/ps */
	file_mmap("./fs.spirv", (u8**)&tmp_fs_g, &tmp_fs_sz_g);
	memset(&info, 0, sizeof(info));
	info.type = vk_struct_type_shmod_create_info;
	info.code_sz = tmp_fs_sz_g;
	info.code = tmp_fs_g;
	vk_create_shmod(&info, &tmp_fs_mod_g);
	VK_FATAL("0:MAIN:FATAL:%d:device:%p:unable to create the fragment/pixel shader module", r, surf_g.dev.vk)
	LOG("0:MAIN:device:%p:created fragment/pixel shader module %p\n", surf_g.dev.vk, tmp_fs_mod_g);
}

static void shmods_destroy(void)
{
	vk_destroy_shmod(tmp_vs_mod_g);
	munmap(tmp_vs_g, tmp_vs_sz_g);
	vk_destroy_shmod(tmp_fs_mod_g);
	munmap(tmp_fs_g, tmp_fs_sz_g);
}

static void *pl_layout_empty_create(void)
{
	struct vk_pl_layout_create_info_t layout_info;
	s32 r;
	void *pl_layout;

	memset(&layout_info, 0, sizeof(layout_info));
	layout_info.type = vk_struct_type_pl_layout_create_info;
	vk_create_pl_layout(&layout_info, &pl_layout);
	VK_FATAL("0:MAIN:FATAL:%d:device:%p:unable to create the pipeline layout", r, surf_g.dev.vk)
	return pl_layout;
}

static void pl_create(void)
{
	struct vk_gfx_pl_create_info_t pl_info;
	struct vk_pl_sh_stage_create_info_t sh_stage_infos[2];
	struct vk_pl_vtx_input_state_create_info_t vtx_info;
	struct vk_pl_input_assembly_state_create_info_t assembly_info;
	struct vk_pl_viewport_state_create_info_t viewport_info;
	struct vk_viewport_t viewport;
	struct vk_rect_2d_t scissor;
	struct vk_pl_raster_state_create_info_t raster_info;
	struct vk_pl_multisample_state_create_info_t multisample_info;
	struct vk_pl_color_blend_at_state_t color_blend_at;
	struct vk_pl_color_blend_state_create_info_t color_blend_info;
	s32 r;
	/*--------------------------------------------------------------------*/
	memset(sh_stage_infos, 0, sizeof(sh_stage_infos));
	sh_stage_infos[0].type = vk_struct_type_pl_sh_stage_create_info;
	sh_stage_infos[0].stage = vk_sh_stage_vtx_bit;
	sh_stage_infos[0].shmod = tmp_vs_mod_g;
	sh_stage_infos[0].name = "main";
	sh_stage_infos[1].type = vk_struct_type_pl_sh_stage_create_info;
	sh_stage_infos[1].stage = vk_sh_stage_frag_bit;
	sh_stage_infos[1].shmod = tmp_fs_mod_g;
	sh_stage_infos[1].name = "main";
	/*--------------------------------------------------------------------*/
	memset(&vtx_info, 0, sizeof(vtx_info));
	vtx_info.type = vk_struct_type_pl_vtx_input_state_create_info;
	/* our triangle vtxs are hardcoded in the vtx shader */
	/*--------------------------------------------------------------------*/
	memset(&assembly_info, 0, sizeof(assembly_info));
	assembly_info.type = vk_struct_type_pl_input_assembly_state_create_info;
	assembly_info.topology = vk_prim_topology_triangle_list;
	/*--------------------------------------------------------------------*/
	/* "viewport" is actually a transformation from "normalized" coords */
	memset(&viewport_info, 0, sizeof(viewport_info));
	viewport_info.type = vk_struct_type_pl_viewport_state_create_info;
	viewport_info.viewports_n = 1;
	viewport.x = 0.0f;
	viewport.y = 0.0f;
	viewport.width = (f32)APP_WIN_WIDTH;
	viewport.height = (f32)APP_WIN_HEIGHT;
	viewport.depth_min = 0.0f;
	viewport.depth_max = 1.0f;
	viewport_info.viewports = &viewport;
	memset(&scissor, 0, sizeof(scissor));
	viewport_info.scissors_n = 1;
	scissor.offset.x = 0;
	scissor.offset.y = 0;
	scissor.extent.width = APP_WIN_WIDTH; /* same than our swpchn imgs */
	scissor.extent.height = APP_WIN_HEIGHT; /* same than our swpchn imgs */
	viewport_info.scissors = &scissor;
	/*--------------------------------------------------------------------*/
	memset(&raster_info, 0, sizeof(raster_info));
	raster_info.type = vk_struct_type_pl_raster_state_create_info;
	raster_info.polygon_mode = vk_polygon_mode_fill;
	raster_info.cull_mode = vk_cull_mode_back_bit;
	raster_info.front_face = vk_front_face_clockwise;
	/*--------------------------------------------------------------------*/
	memset(&multisample_info, 0, sizeof(multisample_info));
	multisample_info.type = vk_struct_type_pl_multisample_state_create_info;
	multisample_info.raster_samples_n = vk_samples_n_1_bit;
	/*--------------------------------------------------------------------*/
	memset(&color_blend_at, 0, sizeof(color_blend_at));
	color_blend_at.color_write_mask =	  vk_color_comp_r_bit
						| vk_color_comp_g_bit
						| vk_color_comp_b_bit
						| vk_color_comp_a_bit;
	memset(&color_blend_info, 0, sizeof(color_blend_info));
	color_blend_info.type = vk_struct_type_pl_color_blend_state_create_info;
	color_blend_info.ats_n = 1;
	color_blend_info.ats = &color_blend_at;
	/*--------------------------------------------------------------------*/
	memset(&pl_info, 0, sizeof(pl_info));
	pl_info.type = vk_struct_type_gfx_pl_create_info;
	pl_info.sh_stages_n = 2;
	pl_info.sh_stages = sh_stage_infos;
	pl_info.vtx_input_state = &vtx_info;
	pl_info.input_assembly_state = &assembly_info;
	pl_info.viewport_state = &viewport_info;
	pl_info.raster_state = &raster_info;
	pl_info.multisample_state = &multisample_info;
	pl_info.color_blend_state = &color_blend_info;
	pl_info.layout = pl_layout_empty_create();
	pl_info.rp = surf_g.dev.rp;
	pl_info.sp = 0; /* idx */
	/*--------------------------------------------------------------------*/
	vk_create_gfx_pls(&pl_info);
	VK_FATAL("0:MAIN:FATAL:%d:device:%p:unable to create the pipeline", r, surf_g.dev.vk)
}

static void dev_init(void)
{
	phydev_init();
	dev_create();
	dev_syms();
	q_get();
	cp_create();
	rp_create();
	shmods_create();
	pl_create();
	shmods_destroy(); /* we can destroy the shmods after pl creation */
}

static void swpchn_fb_create(u8 i)
{
	s32 r;
	struct vk_fb_create_info_t info;

	memset(&info, 0, sizeof(info));
	info.type = vk_struct_type_fb_create_info;
	/* all fbs use the same rp */
	info.rp = surf_g.dev.rp;
	info.ats_n = 1;
	info.ats = &surf_g.dev.swpchn.imgviews[i];
	info.width = APP_WIN_WIDTH;
	info.height = APP_WIN_HEIGHT;
	info.layers_n = 1;
	vk_create_fb(&info, &surf_g.dev.swpchn.fbs[i]);
	VK_FATAL("0:MAIN:FATAL:%d:device:%p:swapchain:%p:image:%u:unable to create the framebuffer\n", r, surf_g.dev.vk, surf_g.dev.swpchn.vk, i)
	LOG("0:MAIN:FATAL:%d:device:%p:swapchain:%p:image:%u:framebuffer created %p\n", r, surf_g.dev.vk, surf_g.dev.swpchn.vk, i, surf_g.dev.swpchn.fbs[i]);
}

static void swpchn_fbs_create(void)
{
	u8 i;

	i = 0;
	loop {
		if (i == surf_g.dev.swpchn.imgs_n)
			break;
		swpchn_fb_create(i);
		++i;
	}
}

static void surf_init(void)
{
	surf_create();
	dev_init();
	swpchn_init();
	swpchn_imgs_get();
	swpchn_imgviews_create();
	/*
	 * the link between one of the fbs and our only rp/pl is
	 * done while recording cmds, namely it is per cb.
	 */
	swpchn_fbs_create();
	sems_create();
	cbs_create();
	cbs_rec();
}

static void init_vk(void)
{
	load_vk_loader();
	loader_syms();
	instance_static_syms();
	check_vk_version();
	instance_exts_dump();
	instance_layers_dump();
	instance_create();
	instance_syms();
	surf_init();
}

static void swpchn_acquire_next_img(u32 *i)
{
	struct vk_acquire_next_img_info_t info;
	s32 r;

	memset(&info, 0, sizeof(info));
	info.type = vk_struct_type_acquire_next_img_info;
	info.swpchn = surf_g.dev.swpchn.vk;
	info.timeout = u64_max; /* infinite */
	info.devs = 0x00000001; /* no device group then 1 */
	info.sem = surf_g.dev.sems[sem_acquire_img_done];
	vk_acquire_next_img(&info, i);
	VK_FATAL("0:MAIN:FATAL:%d:device:%p:unable to acquire next image from swapchain %p\n", r, surf_g.dev.vk, surf_g.dev.swpchn.vk)
	LOG("0:MAIN:device:%p:swapchain:%p:acquired image %u\n", surf_g.dev.vk, surf_g.dev.swpchn.vk, *i);
}

static void draw(u8 i)
{
	s32 r;
	struct vk_submit_info_t submit_info;
	struct vk_present_info_t present_info;
	u32 wait_dst_stage;
	u32 idxs[1];

	memset(&submit_info, 0, sizeof(submit_info));
	submit_info.type = vk_struct_type_submit_info;
	submit_info.wait_sems_n = 1;
	submit_info.wait_sems = &surf_g.dev.sems[sem_acquire_img_done];
	wait_dst_stage = vk_pl_stage_bottom_of_pipe_bit;
	submit_info.wait_dst_stages = &wait_dst_stage;
	submit_info.cbs_n = 1;
	submit_info.cbs = &surf_g.dev.cbs[i];
	submit_info.signal_sems_n = 1;
	submit_info.signal_sems = &surf_g.dev.sems[app_sem_draw_done];
	vk_q_submit(&submit_info);
	VK_FATAL("0:MAIN:FATAL:%d:queue:%p:unable to submit the image/framebuffer pre-recorded drawing command buffer\n", r, surf_g.dev.q)
	/*--------------------------------------------------------------------*/
	idxs[0] = i;
	memset(&present_info, 0, sizeof(present_info));
	present_info.type = vk_struct_type_present_info;
	present_info.wait_sems_n = 1;
	present_info.wait_sems = &surf_g.dev.sems[sem_draw_done];
	present_info.swpchns_n = 1;
	present_info.swpchns = &surf_g.dev.swpchn.vk;
	present_info.idxs = idxs;
	present_info.results = 0;
	vk_q_present(&present_info);
	VK_FATAL("0:MAIN:FATAL:%d:queue:%p:unable to submit the image/framebuffer %u to the presentation engine\n", r, surf_g.dev.q, i)
}

static void render(void)
{
	u32 i;

	swpchn_acquire_next_img(&i);
	draw(i);
	do_render_g = false;
}

/* "main" loop */
static void run(void)
{
	state_g = state_run;

	render();

	loop {
		xcb_generic_event_t *e;	

		do_render_g = false;
		/* "evts which could lead to change what we display" */
		e = dl_xcb_wait_for_event(app_xcb.c);
		if (e == 0) { /* i/o err */
			LOG("0:MAIN:xcb:'%s':connection:%p:event:input/output error | x11 server connection lost\n", app_xcb.disp_env, app_xcb.c);
			break;
		}
		loop { /* drain evts */
			app_xcb_evt_handle(e);
			free(e);
			if (state_g == state_quit)
				return;
			e = dl_xcb_poll_for_event(app_xcb.c);
			if (e == 0)
				break;
		}
		/* synchronous rendering */
		if (do_render_g)
			render();
	}
}

int main(void)
{
	LOG("0:starting app\n");
	app_xcb_init();
	init_vk();
	run();
	LOG("0:exiting app\n");
	exit(0);
}
#undef VK_FATAL
#undef FATAL
/*---------------------------------------------------------------------------*/
#define CLEANUP
#include "namespace/app.c"
#include "namespace/vk_syms.c"
#include "namespace/app_state_types.h"
#include "namespace/app_state.c"
#undef CLEANUP
/*---------------------------------------------------------------------------*/
#endif
