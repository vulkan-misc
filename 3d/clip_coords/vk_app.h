#ifndef VK_APP_H
#define VK_APP_H
/*
 * this is public domain without any warranties of any kind
 * Sylvain BERTRAND
 */
/* XXX: KEEP AN EYE ON ABBREVIATIONS, ALWAYS */
/*
 * this is the simplification and taylorization of vk api for the specific
 * uses of the app.
 */
#define vk_get_dev_q() \
app_surf.dev.dl_vk_get_dev_q(app_surf.dev.vk, app_surf.dev.phydev.q_fam, 0, &app_surf.dev.q)

#define vk_create_cp(info) \
r = app_surf.dev.dl_vk_create_cp(app_surf.dev.vk, info, 0, &app_surf.dev.cp)

/* a swpchn can become "out of date" */
#define vk_create_swpchn(info, swpchn) \
r = app_surf.dev.dl_vk_create_swpchn(app_surf.dev.vk, info, 0, swpchn)

#define vk_get_swpchn_imgs() \
r = app_surf.dev.dl_vk_get_swpchn_imgs(app_surf.dev.vk, app_surf.dev.swpchn.vk, &app_surf.dev.swpchn.imgs_n, app_surf.dev.swpchn.imgs)

#define vk_alloc_cbs(info) \
r = app_surf.dev.dl_vk_alloc_cbs(app_surf.dev.vk, info, app_surf.dev.cbs)

#define vk_begin_cb(...) \
r = app_surf.dev.dl_vk_begin_cb(__VA_ARGS__)

#define vk_end_cb(...) \
r = app_surf.dev.dl_vk_end_cb(__VA_ARGS__)

#define vk_q_submit(info) \
r = app_surf.dev.dl_vk_q_submit(app_surf.dev.q, 1, info, 0)

#define vk_acquire_next_img(...) \
r = app_surf.dev.dl_vk_acquire_next_img(app_surf.dev.vk,##__VA_ARGS__)

#define vk_q_present(...) \
r = app_surf.dev.dl_vk_q_present(app_surf.dev.q,##__VA_ARGS__)

#define vk_create_sem(info, sem) \
r = app_surf.dev.dl_vk_create_sem(app_surf.dev.vk, info, 0, sem)

#define vk_create_imgview(info, imgview) \
r = app_surf.dev.dl_vk_create_imgview(app_surf.dev.vk, info, 0, imgview)

#define vk_create_rp(info) \
r = app_surf.dev.dl_vk_create_rp(app_surf.dev.vk, info, 0, &app_surf.dev.rp)

#define vk_create_shmod(info, shmod) \
r = app_surf.dev.dl_vk_create_shmod(app_surf.dev.vk, info, 0, shmod)

#define vk_destroy_shmod(shmod) \
app_surf.dev.dl_vk_destroy_shmod(app_surf.dev.vk, shmod, 0)

#define vk_create_pl_layout(info, layout) \
r = app_surf.dev.dl_vk_create_pl_layout(app_surf.dev.vk, info, 0, layout)

#define vk_create_gfx_pls(info) \
r = app_surf.dev.dl_vk_create_gfx_pls(app_surf.dev.vk, 0, 1, info, 0, &app_surf.dev.pl)

#define vk_create_fb(info, fb) \
r = app_surf.dev.dl_vk_create_fb(app_surf.dev.vk, info, 0, fb)

#define vk_cmd_begin_rp app_surf.dev.dl_vk_cmd_begin_rp

#define vk_cmd_bind_pl(cb) \
app_surf.dev.dl_vk_cmd_bind_pl(cb, vk_pl_bind_point_gfx, app_surf.dev.pl)

#define vk_cmd_end_rp app_surf.dev.dl_vk_cmd_end_rp

#define vk_cmd_draw app_surf.dev.dl_vk_cmd_draw
/******************************************************************************/
/* from nyanvk/syms_global.h */
#define VK_GLOBAL_SYMS \
	static void *(*dl_vk_get_instance_proc_addr)(void *instance, u8 *name);\
	static void *(*dl_vk_get_dev_proc_addr)(void *dev, u8 *name);\
\
	static s32 (*dl_vk_enumerate_instance_version)(u32 *version);\
	static s32 (*dl_vk_enumerate_instance_layer_props)(\
				u32 *props_n,\
				struct vk_layer_props_t *props);\
	static s32 (*dl_vk_enumerate_instance_ext_props)(\
				u8 *layer_name,\
				u32 *props_n,\
				struct vk_ext_props_t *props);\
	static s32 (*dl_vk_create_instance)(\
				struct vk_instance_create_info_t *info,\
				void *allocator,\
				void **instance);\
	static s32 (*dl_vk_enumerate_phydevs)(\
				void *instance,\
				u32 *phydevs_n,\
				void **phydevs);\
	static s32 (*dl_vk_enumerate_dev_ext_props)(\
				void *phydev,\
				u8 *layer_name,\
				u32 *props_n,\
				struct vk_ext_props_t *props);\
	static void (*dl_vk_get_phydev_props)(\
				void *phydev,\
				struct vk_phydev_props_t *props);\
	static s32 (*dl_vk_create_dev)(\
			void *phydev,\
			struct vk_dev_create_info_t *create_info,\
			void *allocator,\
			void **dev);\
	static void (*dl_vk_get_phydev_q_fam_props)(\
			void *phydev,\
			u32 *q_fam_props_n,\
			struct vk_q_fam_props_t *props);\
	static s32 (*dl_vk_create_xcb_surf)(\
				void *instance,\
				struct vk_xcb_surf_create_info_t *info,\
				void *allocator,\
				void **surf);\
	static s32 (*dl_vk_get_phydev_surf_support)(\
					void *phydev,\
					u32 q_fam,\
					void *surf,\
					u32 *supported);\
	static s32  (*dl_vk_get_phydev_surf_texel_mem_blk_confs)(\
				void *phydev,\
				struct vk_phydev_surf_info_t *info,\
				u32 *confs_n,\
				struct vk_surf_texel_mem_blk_conf_t *confs);\
	static void (*dl_vk_get_phydev_mem_props)(\
			void *phydev,\
			struct vk_phydev_mem_props_t *props);\
	static s32 (*dl_vk_get_phydev_surf_caps)(\
					void *phydev,\
					struct vk_phydev_surf_info_t *info,\
					struct vk_surf_caps_t *caps);\
	static s32 (*dl_vk_get_phydev_surf_present_modes)(\
					void *phydev,\
					void *surf,\
					u32 *modes_n,\
					u32 *modes);
/******************************************************************************/
#define vk_get_instance_proc_addr dl_vk_get_instance_proc_addr

#define vk_get_dev_proc_addr dl_vk_get_dev_proc_addr

#define vk_enumerate_instance_version(...) \
r = dl_vk_enumerate_instance_version(__VA_ARGS__)

#define vk_enumerate_instance_layer_props(...) \
r = dl_vk_enumerate_instance_layer_props(__VA_ARGS__)

#define vk_enumerate_instance_ext_props(props_n, props) \
r = dl_vk_enumerate_instance_ext_props(0, props_n, props)

#define vk_create_instance(info) \
r = dl_vk_create_instance(info, 0, &app_instance)

#define vk_enumerate_phydevs(...) \
r = dl_vk_enumerate_phydevs(app_instance,##__VA_ARGS__)

#define vk_enumerate_dev_ext_props(phydev, props_n, props) \
r = dl_vk_enumerate_dev_ext_props(phydev, 0, props_n, props)

#define vk_get_phydev_props dl_vk_get_phydev_props

#define vk_create_dev(info) \
r = dl_vk_create_dev(app_surf.dev.phydev.vk, info, 0, &app_surf.dev.vk)

#define vk_get_phydev_q_fam_props dl_vk_get_phydev_q_fam_props

#define vk_create_xcb_surf(info) \
r = dl_vk_create_xcb_surf(app_instance, info, 0, &app_surf.vk)

#define vk_get_phydev_surf_support(phydev, q_fam, supported) \
r = dl_vk_get_phydev_surf_support(phydev, q_fam, app_surf.vk, supported)

#define vk_get_phydev_surf_texel_mem_blk_confs(...) \
r = dl_vk_get_phydev_surf_texel_mem_blk_confs(app_surf.dev.phydev.vk,##__VA_ARGS__)

#define vk_get_phydev_mem_props dl_vk_get_phydev_mem_props

#define vk_get_phydev_surf_caps(info, caps) \
r = dl_vk_get_phydev_surf_caps(app_surf.dev.phydev.vk, info, caps)

#define vk_get_phydev_surf_present_modes() \
r = dl_vk_get_phydev_surf_present_modes(app_surf.dev.phydev.vk,app_surf.vk, &app_tmp_present_modes_n, app_tmp_present_modes)
#endif
