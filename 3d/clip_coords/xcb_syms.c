#ifndef XCB_SYMS_C
#define XCB_SYMS_C
/*
 * this is public domain without any warranties of any kind
 * Sylvain BERTRAND
 */
/* XXX: KEEP AN EYE ON ABBREVIATIONS, ALWAYS */
#include <dlfcn.h>
#include <stdlib.h>
#include <xcb.h>
#include "app_core_types.h"
#include "log.h"

static void *xcb_client_lib;

static xcb_connection_t *(*dl_xcb_connect)(u8 *displayname, int *scr);
static u32 (*dl_xcb_generate_id)(xcb_connection_t *c);
static int (*dl_xcb_connection_has_error)(xcb_connection_t *c);
static xcb_setup_t *(*dl_xcb_get_setup)(xcb_connection_t *c);
static int (*dl_xcb_setup_roots_length)(xcb_setup_t *s);
static xcb_screen_iterator_t (*dl_xcb_setup_roots_iterator)(xcb_setup_t *s);
static void (*dl_xcb_screen_next)(xcb_screen_iterator_t *iter);
static void (*dl_xcb_create_window)(xcb_connection_t *c, u8 depth,
	xcb_window_t wid, xcb_window_t parent, s16 x, s16 y, u16 width,
	u16 heigth, u16 border_width, u16 class, xcb_visualid_t visual,
	u32 value_mask, u32 *value_list);
static xcb_void_cookie_t (*dl_xcb_map_window)(xcb_connection_t *c,
							xcb_window_t win);
static xcb_void_cookie_t (*dl_xcb_map_window_checked)(xcb_connection_t *c,
							xcb_window_t win);
static xcb_generic_error_t *(*dl_xcb_request_check)(xcb_connection_t *c,
						xcb_void_cookie_t cookie);
static int (*dl_xcb_flush)(xcb_connection_t *c);
static xcb_generic_event_t *(*dl_xcb_wait_for_event)(xcb_connection_t *c);
static xcb_generic_event_t *(*dl_xcb_poll_for_event)(xcb_connection_t *c);
static void (*dl_xcb_disconnect)(xcb_connection_t *c);
static xcb_void_cookie_t (*dl_xcb_change_property)(xcb_connection_t *c, u8 mode,
	xcb_window_t window, xcb_atom_t property, xcb_atom_t type, u8 format,
						u32 data_len, void *data);

static void app_xcb_client_lib_load(void)
{
	xcb_client_lib = dlopen("libxcb.so.1", RTLD_LAZY);
	if (xcb_client_lib == 0) {
		LOG("0:MAIN:FATAL:%s:unable to load the xcb dynamic shared library\n", dlerror());
		exit(1);
	}
}

static void app_xcb_client_lib_close(void)
{
	int r;

	r = dlclose(xcb_client_lib);
	if (r != 0) {
		LOG("0:MAIN:FATAL:%d:%s:unable to close the xcb dynamic shared library\n", r, dlerror());
		exit(1);
	}
	xcb_client_lib = 0;
}
/*----------------------------------------------------------------------------*/
#define XCB_DLSYM(x)								\
	dl_##x = dlsym(xcb_client_lib, #x);					\
	if (dl_##x == 0) {							\
		LOG("0:MAIN:FATAL:%s:unable to find " #x "\n", dlerror());	\
		exit(1);							\
	}
static void app_xcb_syms(void)
{
	XCB_DLSYM(xcb_connect);
	XCB_DLSYM(xcb_generate_id);
	XCB_DLSYM(xcb_connection_has_error);
	XCB_DLSYM(xcb_get_setup);
	XCB_DLSYM(xcb_setup_roots_length);
	XCB_DLSYM(xcb_setup_roots_iterator);
	XCB_DLSYM(xcb_screen_next);
	XCB_DLSYM(xcb_create_window);
	XCB_DLSYM(xcb_map_window);
	XCB_DLSYM(xcb_map_window_checked);
	XCB_DLSYM(xcb_request_check);
	XCB_DLSYM(xcb_flush);
	XCB_DLSYM(xcb_wait_for_event);
	XCB_DLSYM(xcb_poll_for_event);
	XCB_DLSYM(xcb_change_property);
	XCB_DLSYM(xcb_disconnect);
}
#undef XCB_DLSYM
#endif /* X_SYMS_C */
