#ifndef VK_SYMS_C
#define VK_SYMS_C
/*
 * this is public domain without any warranties of any kind
 * Sylvain BERTRAND
 */
/* XXX: KEEP AN EYE ON ABBREVIATIONS, ALWAYS */
#include <dlfcn.h>
#include <stdlib.h>
#include <xcb.h>
#include "app_core_types.h"
#include "nyanvk/types.h"
#include "vk_app.h"
#include "log.h"
#include "app_state.c"
/*----------------------------------------------------------------------------*/
#include "namespace/vk_syms.c"
/*----------------------------------------------------------------------------*/
VK_GLOBAL_SYMS
static void *loader_g;
#define INSTANCE_STATIC_SYM(x,y)						\
	dl_##y = vk_get_instance_proc_addr(0, #x);				\
	if (dl_##y == 0) {							\
		LOG("0:MAIN:FATAL:unable to find vulkan " #x "\n");		\
		exit(1);							\
	}
static void instance_static_syms(void)
{
	INSTANCE_STATIC_SYM(vkEnumerateInstanceVersion,
						vk_enumerate_instance_version);
	INSTANCE_STATIC_SYM(vkEnumerateInstanceExtensionProperties,
					vk_enumerate_instance_ext_props);
	INSTANCE_STATIC_SYM(vkEnumerateInstanceLayerProperties,
					vk_enumerate_instance_layer_props);
	INSTANCE_STATIC_SYM(vkCreateInstance, vk_create_instance);
}
#undef INSTANCE_STATIC_SYM
/*----------------------------------------------------------------------------*/
#define INSTANCE_SYM(x,y)							\
	dl_##y = vk_get_instance_proc_addr(app_instance, #x);			\
	if (dl_##y == 0) {								\
		LOG("0:MAIN:FATAL:unable to find vulkan " #x "\n");		\
		exit(1);							\
	}
static void instance_syms(void)
{
	INSTANCE_SYM(vkEnumeratePhysicalDevices, vk_enumerate_phydevs);
	INSTANCE_SYM(vkEnumerateDeviceExtensionProperties,
						vk_enumerate_dev_ext_props);
	INSTANCE_SYM(vkGetPhysicalDeviceProperties2, vk_get_phydev_props);
	INSTANCE_SYM(vkGetPhysicalDeviceQueueFamilyProperties2,
						vk_get_phydev_q_fam_props);
	INSTANCE_SYM(vkCreateDevice, vk_create_dev);
	/* wsi related -------------------------------------------------------*/
	INSTANCE_SYM(vkGetPhysicalDeviceSurfaceSupportKHR,
					vk_get_phydev_surf_support);
	INSTANCE_SYM(vkGetPhysicalDeviceSurfaceFormats2KHR,
				vk_get_phydev_surf_texel_mem_blk_confs);
	INSTANCE_SYM(vkCreateXcbSurfaceKHR, vk_create_xcb_surf);
	INSTANCE_SYM(vkGetPhysicalDeviceMemoryProperties2,
						vk_get_phydev_mem_props);
	INSTANCE_SYM(vkGetPhysicalDeviceSurfaceCapabilities2KHR,
						vk_get_phydev_surf_caps);
	INSTANCE_SYM(vkGetPhysicalDeviceSurfacePresentModesKHR,
					vk_get_phydev_surf_present_modes);
	/*--------------------------------------------------------------------*/
}
#undef INSTANCE_SYM
/*----------------------------------------------------------------------------*/
#define DEV_SYM(x,y)								\
	app_surf.dev.dl_##y = vk_get_dev_proc_addr(app_surf.dev.vk, #x);	\
	if (app_surf.dev.dl_##y == 0) {						\
		LOG("0:MAIN:FATAL:unable to find vulkan device " #x "\n");	\
		exit(1);							\
	}
static void dev_syms(void)
{
	DEV_SYM(vkGetDeviceQueue, vk_get_dev_q);
	DEV_SYM(vkCreateCommandPool, vk_create_cp);
	DEV_SYM(vkCreateSwapchainKHR, vk_create_swpchn);
	DEV_SYM(vkGetSwapchainImagesKHR, vk_get_swpchn_imgs);
	DEV_SYM(vkAllocateCommandBuffers, vk_alloc_cbs);
	DEV_SYM(vkBeginCommandBuffer, vk_begin_cb);
	DEV_SYM(vkEndCommandBuffer, vk_end_cb);
	DEV_SYM(vkQueueSubmit, vk_q_submit);
	DEV_SYM(vkAcquireNextImage2KHR, vk_acquire_next_img);
	DEV_SYM(vkQueuePresentKHR, vk_q_present);
	DEV_SYM(vkCreateSemaphore, vk_create_sem);
	DEV_SYM(vkCreateImageView, vk_create_imgview);
	DEV_SYM(vkCreateRenderPass2KHR, vk_create_rp);
	DEV_SYM(vkCreateFramebuffer, vk_create_fb);
	DEV_SYM(vkCreateShaderModule, vk_create_shmod);
	DEV_SYM(vkDestroyShaderModule, vk_destroy_shmod);
	DEV_SYM(vkCreatePipelineLayout, vk_create_pl_layout);
	DEV_SYM(vkCreateGraphicsPipelines, vk_create_gfx_pls);
	DEV_SYM(vkCmdBeginRenderPass2KHR, vk_cmd_begin_rp);
	DEV_SYM(vkCmdBindPipeline, vk_cmd_bind_pl);
	DEV_SYM(vkCmdDraw, vk_cmd_draw);
	DEV_SYM(vkCmdEndRenderPass2KHR, vk_cmd_end_rp);
}
#undef DEVICE_SYM
/*----------------------------------------------------------------------------*/
#define DLSYM(x, y)								\
	dl_##y = dlsym(loader_g, #x);							\
	if (dl_##y == 0) {								\
		LOG("0:MAIN:FATAL:%s:unable to find " #x "\n", dlerror());	\
		exit(1);							\
	}
static void loader_syms(void)
{
	DLSYM(vkGetInstanceProcAddr, vk_get_instance_proc_addr);
	DLSYM(vkGetDeviceProcAddr, vk_get_dev_proc_addr);
}
#undef DLSYM
/*----------------------------------------------------------------------------*/
static void load_vk_loader(void)
{
	/* no '/' in the shared dynamic lib path, then standard lookup */
	loader_g = dlopen("libvulkan.so.1", RTLD_LAZY);
	if (loader_g == 0) {
		LOG("0:MAIN:FATAL:%s:unable to load the vulkan loader dynamic shared library\n", dlerror());
		exit(1);
	}
}
/*----------------------------------------------------------------------------*/
#define CLEANUP
#include "namespace/vk_syms.c"
#undef CLEANUP
/*----------------------------------------------------------------------------*/
#endif /* VK_SYMS_C */
