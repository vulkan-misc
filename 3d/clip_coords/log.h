#ifndef APP_LOG_H
#define APP_LOG_H
/*
 * this is public domain without any warranties of any kind
 * Sylvain BERTRAND
 */
/* XXX: KEEP AN EYE ON ABBREVIATIONS, ALWAYS */
#include <stdio.h>
#define LOG(fmt,...) dprintf(2,fmt,##__VA_ARGS__)
#endif /* APP_LOG_H */
